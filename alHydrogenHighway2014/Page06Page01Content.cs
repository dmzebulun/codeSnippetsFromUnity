﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Page06Page01Content : MonoBehaviour 
{

	private Animator mainAnim;

	// Use this for initialization
	void Start () 
	{
		mainAnim = GetComponent<Animator>();
	}
	
	public void AnimateIn()
	{
		mainAnim.SetTrigger("AnimateIn");
	}

	public void AnimateReset()
	{
		mainAnim.SetTrigger("AnimateReset");
	}
}
