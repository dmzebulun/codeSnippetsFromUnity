﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BackgroundImage : MonoBehaviour 
{
	private Animator bgAnim;


	// Use this for initialization
	void Start () 
	{
		bgAnim = GetComponent<Animator>();
	}


	public void HomeBackground()
	{
		bgAnim.SetTrigger("Home");
	}

	public void ContentBackground()
	{
		bgAnim.SetTrigger ("Content");
	}
}
