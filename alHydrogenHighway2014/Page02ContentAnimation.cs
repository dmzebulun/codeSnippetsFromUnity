﻿using UnityEngine;
using System.Collections;

public class Page02ContentAnimation : MonoBehaviour 
{

	private Animator menuAnim;
	
	public void Awake()
	{
		menuAnim = GetComponent<Animator>();
	}

	public void AnimateStart()
	{
		menuAnim.SetTrigger("AnimateStart");
	}

	public void AnimateIn()
	{
		menuAnim.SetTrigger("AnimateIn");
	}

}
