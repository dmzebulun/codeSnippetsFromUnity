﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Page06MovieAnimControl : MonoBehaviour 
{
	private Animator mainAnim;
	private bool visible;

	public AVProQuickTimeMovie page06movie;

	// Use this for initialization
	void Start () 
	{
		mainAnim = GetComponent<Animator>();
		visible = false;
	}


	public void FadeIn()
	{
		mainAnim.SetTrigger("FadeIn");
		visible = true;
	}

	public void FadeOut()
	{

		mainAnim.SetTrigger("FadeOut");
		visible = false;
		page06movie.MovieInstance.Pause();
		StartCoroutine(PositionReset());
	}

	public void FadeControl()
	{
		if(!visible)
		{
			mainAnim.SetTrigger("FadeIn");
			visible = true;
			page06movie.MovieInstance.Play();
			AnimationControl.moviePage06IsPlaying = true;

		}
		else
		{

			mainAnim.SetTrigger("FadeOut");
			visible = false;
			page06movie.MovieInstance.Pause();
			StartCoroutine(PositionReset());

		}

	}

	public IEnumerator PositionReset()
	{

		yield return new WaitForSeconds(1f);
		page06movie.MovieInstance.PositionSeconds = 0.0f;
		AnimationControl.moviePage06IsPlaying = false;


	}
}
