﻿using UnityEngine;
using System.Collections;

public class Page00Content : MonoBehaviour 
{

	private Animator mapAnim;
	public Animator mapTitleAnim;

	void Awake()
	{
		mapAnim = GetComponent<Animator>();
	}

	public void AnimateIn()
	{
		mapAnim.SetTrigger ("AnimateIn");
		mapTitleAnim.SetTrigger("AnimateIn");

	}

	public void AnimateReset()
	{
		mapAnim.SetTrigger ("AnimateReset");
		mapTitleAnim.SetTrigger("AnimateReset");
	}
}
