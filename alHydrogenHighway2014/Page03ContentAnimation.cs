﻿using UnityEngine;
using System.Collections;

public class Page03ContentAnimation : MonoBehaviour {

	private Animator menuAnim;
	
	public void Awake()
	{
		menuAnim = GetComponent<Animator>();
	}
	
	public void AnimateRestart()
	{
		menuAnim.SetTrigger("AnimateReset");
	}
	
	public void AnimateIn()
	{
		menuAnim.SetTrigger("AnimateIn");
	}

}
