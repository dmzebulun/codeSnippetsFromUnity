﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Runtime.InteropServices;

public class AnimationControl : MonoBehaviour 
{
	//Test Text 
	public Text debugText;

	//Static Variables
	public static CanvasGroup currentCanvasGroup;
	public static Animator currentAnimator01;
	public static Animator currentAnimator02;
	public static Button currentButton;
	public static Image currentBackground;
	public static bool button06reset;
	public static bool topHomeButton;
	public static bool bottomHomeButton;
	public static bool movieIsPlaying;
	public static bool moviePage06IsPlaying;
	
	//Button01 Variables
	public Image Button01inactive;
	public Image Button01active;
	public Button button01;

	//Button02 Variables
	public Image Button02inactive;
	public Image Button02active;
	public Button button02;

	//Button03 Variables
	public Image Button03inactive;
	public Image Button03active;
	public Button button03;

	//Button04 Variables
	public Image Button04inactive;
	public Image Button04active;
	public Button button04;
	public Animator button04Map;
	public Animator button04MapTitles;

	//Button05 Variables
	public Image Button05inactive;
	public Image Button05active;
	public Button button05;

	//Button06 Variables
	public Image Button06inactive;
	public Image Button06active;
	public Button button06;
	public Animator button06bar;
	public Animator button06Arrow;
	public Animator button06ContentMover;
	public Animator button06Content;
	public Animator button06Page01;
	public AVProQuickTimeMovie moviePage06;
	public Animator page06movie;
	public Animator page06SubHeading01;
	public Animator page06SubHeading02;

	//Button07 Variables
	public Image Button07inactive;
	public Image Button07active;
	public Button button07;

	//Home Page Map Animators
	public Animator mapAnim;
	public Animator mapTitlesAnim;
	public AVProQuickTimeMovie movieHomePage;

	//Top Home Button
	public Button homeButtonTop;
	public Image button08HomeActive;
	public Animator homeButtonTopAnim;
	
	//Bottom Home Button
	public Button homeButtonBottom;
	public Image button10HomeActive;
	public Animator homeButtonBottomAnim;

	//Start Button
	public Button button09TouchToStart;
	public GameObject button09StartGO;
	public Animator buttonTouchToStartAnim;
	
	//All the canvas groups in the project
	public CanvasGroup canvasGroup01;
	public CanvasGroup canvasGroup02;
	public CanvasGroup canvasGroup03;
	public CanvasGroup canvasGroup04;
	public CanvasGroup canvasGroup05;
	public CanvasGroup canvasGroup06;
	public CanvasGroup canvasGroup07;
	public CanvasGroup canvasGroup08;
	public CanvasGroup dummyGroup;
	
	//All the Canvas Animators 
	public Animator homePage00;
	public Animator page01anim;
	public Animator page02anim;
	public Animator page03anim;
	public Animator page04anim;
	public Animator page05anim;
	public Animator page06anim;
	public Animator page07anim;
	public Animator dummyAnimator;


	//Both Background Images
	private bool backgroundContentVeiwable;
	public Image backgroundHome;
	public Image backgroundContent;
	





	
	public void Start()
	{
		LeanTween.scale (button08HomeActive.rectTransform, new Vector3(.4f, .4f, .4f), .2f).setEase(LeanTweenType.easeInOutExpo);
		homePage00.GetComponent<Animator>().SetTrigger("FadeIn");
		//mapAnim.GetComponent<Animator>().SetTrigger("AnimateIn");
		//mapTitlesAnim.GetComponent<Animator>().SetTrigger("AnimateIn");
		currentCanvasGroup = canvasGroup08;
		currentAnimator01 = mapAnim;
		currentAnimator02 = mapTitlesAnim;
		currentButton = homeButtonTop;
		currentBackground = backgroundHome;
		backgroundContentVeiwable = false;
		button06reset = true;
		topHomeButton = true;
		bottomHomeButton = false;
		homeButtonBottom.enabled = false;
		movieIsPlaying = true;
		moviePage06IsPlaying = false;
		//page06SubHeading.text = "INSIDE A STATION";
	}



	/*public void Update()
	{
		Debug.Log (backgroundContentVeiwable);
		Debug.Log(currentBackground);	
	}*/
	



	public void ContentFadeOut()
	{
		currentCanvasGroup.GetComponent<Animator>().SetTrigger("FadeOut");
		currentAnimator01.GetComponent<Animator>().SetTrigger("AnimateReset");
		currentAnimator02.GetComponent<Animator>().SetTrigger("AnimateReset");
	}



	//Button Controllers
	public void Button01()
	{
		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = true;
		currentButton = button01;
		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = false;

		Debug.Log ("How did we get here?");
		debugText.text = "Debug Text: Button 01";
			
		//Scale up current button
		LeanTween.scale (Button01active.rectTransform, new Vector3(.4f, .4f, .4f), .2f).setEase(LeanTweenType.easeInOutExpo);

		//Scale down any and all of the other buttons
		LeanTween.scale (Button02active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button03active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button04active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button05active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button06active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button07active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (button08HomeActive.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
			

		BackgroundCheck();


		currentCanvasGroup = canvasGroup01;
		currentAnimator01 = page01anim;
		currentAnimator02 = dummyAnimator;
		currentBackground = backgroundContent;
		backgroundContentVeiwable = true;

		Button06Reset();
		StartCoroutine(MoviePage06Check());

		HomeButtonManager();

		StartCoroutine(MovieCheck());


	}

	
	public void Button02()
	{
		Debug.Log ("Where are we now");
		debugText.text = "Debug Text: Button 02";

		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = true;
		currentButton = button02;
		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = false;

		//Scale up bullet next to question
		LeanTween.scale (Button02active.rectTransform, new Vector3(.4f, .4f, .4f), .2f).setEase(LeanTweenType.easeInOutExpo);

		//Scale down any and all of the other buttons
		LeanTween.scale (Button01active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button03active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button04active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button05active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button06active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button07active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (button08HomeActive.rectTransform, new Vector3(.0f, .0f, .0f), .3f);

		BackgroundCheck();

		currentCanvasGroup = canvasGroup02;
		currentAnimator01 = page02anim;
		currentAnimator02 = dummyAnimator;
		currentBackground = backgroundContent;
		backgroundContentVeiwable = true;

		Button06Reset();
		StartCoroutine(MoviePage06Check());

		HomeButtonManager();

		StartCoroutine(MovieCheck());

	}


	public void Button03()
	{
		Debug.Log ("Where have we been?");
		debugText.text = "Debug Text: Button 03";

		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = true;
		currentButton = button03;
		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = false;
		
		//Scale up bullet next to question
		LeanTween.scale (Button03active.rectTransform, new Vector3(.4f, .4f, .4f), .2f).setEase(LeanTweenType.easeInOutExpo);

		//Scale down any and all of the other buttons
		LeanTween.scale (Button01active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button02active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button04active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button05active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button06active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button07active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (button08HomeActive.rectTransform, new Vector3(.0f, .0f, .0f), .3f);

		BackgroundCheck();

		currentCanvasGroup = canvasGroup03;
		currentAnimator01 = page03anim;
		currentAnimator02 = dummyAnimator;
		currentBackground = backgroundContent;
		backgroundContentVeiwable = true;

		Button06Reset();
		StartCoroutine(MoviePage06Check());

		HomeButtonManager();

		StartCoroutine(MovieCheck());

	}


	public void Button04()
	{
		Debug.Log ("Where do we go from here?");
		debugText.text = "Debug Text: Button 04";

		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = true;
		currentButton = button04;
		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = false;

		//Scale up bullet next to question
		LeanTween.scale (Button04active.rectTransform, new Vector3(.4f, .4f, .4f), .2f).setEase(LeanTweenType.easeInOutExpo);

		//Scale down any and all of the other buttons
		LeanTween.scale (Button01active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button02active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button03active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button05active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button06active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button07active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (button08HomeActive.rectTransform, new Vector3(.0f, .0f, .0f), .3f);

		BackgroundCheck();

		currentCanvasGroup = canvasGroup04;
		currentAnimator01 = button04Map;
		currentAnimator02 = button04MapTitles;
		currentBackground = backgroundContent;
		backgroundContentVeiwable = true;

		Button06Reset();
		StartCoroutine(MoviePage06Check());

		HomeButtonManager();

		StartCoroutine(MovieCheck());

	}


	public void Button05()
	{
		Debug.Log ("What are the economic opportunities?");
		debugText.text = "Debug Text: Button 05";

		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = true;
		currentButton = button05;
		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = false;

		//Scale up bullet next to question
		LeanTween.scale (Button05active.rectTransform, new Vector3(.4f, .4f, .4f), .2f).setEase(LeanTweenType.easeInOutExpo);

		//Scale down any and all of the other buttons
		LeanTween.scale (Button01active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button02active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button03active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button04active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button06active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button07active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (button08HomeActive.rectTransform, new Vector3(.0f, .0f, .0f), .3f);

		BackgroundCheck();

		currentCanvasGroup = canvasGroup05;
		currentAnimator01 = page05anim;
		currentAnimator02 = dummyAnimator;
		currentBackground = backgroundContent;
		backgroundContentVeiwable = true;

		Button06Reset();
		StartCoroutine(MoviePage06Check());

		HomeButtonManager();

		StartCoroutine(MovieCheck());


	}


	public void Button06()
	{
		Debug.Log ("How does it work?");
		debugText.text = "Debug Text: Button 06";


		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = true;
		currentButton = button06;
		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = false;


		//Scale up bullet next to question
		LeanTween.scale (Button06active.rectTransform, new Vector3(.4f, .4f, .4f), .2f).setEase(LeanTweenType.easeInOutExpo);
	
		//Scale down any and all of the other buttons
		LeanTween.scale (Button01active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button02active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button03active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button04active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button05active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button07active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (button08HomeActive.rectTransform, new Vector3(.0f, .0f, .0f), .3f);

		BackgroundCheck();

		currentCanvasGroup = canvasGroup06;
		currentAnimator01 = page06anim;
		currentAnimator02 = dummyAnimator;
		currentBackground = backgroundContent;
		backgroundContentVeiwable = true;

		moviePage06.MovieInstance.Play();
		moviePage06IsPlaying = true;

		button06reset = false;

		HomeButtonManager();

		StartCoroutine(MovieCheck());


	}


	public void Button07()
	{
		Debug.Log ("What does it mean for the sustainable city?");
		debugText.text = "Debug Text: Button 07";

		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = true;
		currentButton = button07;
		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = false;

		//Scale up bullet next to question
		LeanTween.scale (Button07active.rectTransform, new Vector3(.4f, .4f, .4f), .2f).setEase(LeanTweenType.easeInOutExpo);

		//Scale down any and all of the other buttons
		LeanTween.scale (Button01active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button02active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button03active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button04active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button05active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button06active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (button08HomeActive.rectTransform, new Vector3(.0f, .0f, .0f), .3f);


		BackgroundCheck();

		currentCanvasGroup = canvasGroup07;
		currentAnimator01 = page07anim;
		currentAnimator02 = dummyAnimator;
		currentBackground = backgroundContent;
		backgroundContentVeiwable = true;

		Button06Reset();
		StartCoroutine(MoviePage06Check());

		HomeButtonManager();

		StartCoroutine(MovieCheck());

	}


	public void Button08()
	{
		Debug.Log ("Home");
		debugText.text = "Debug Text: Home";

		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = true;
		currentButton = homeButtonTop;
		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = false;



		//Scale up upper home button
		LeanTween.scale (button08HomeActive.rectTransform, new Vector3(.4f, .4f, .4f), .2f).setEase(LeanTweenType.easeInOutExpo);

		//Scale down any active buttons
		LeanTween.scale (Button01active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button02active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button03active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button04active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button05active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button06active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button07active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);


	
		currentCanvasGroup = canvasGroup08;
		currentAnimator01 = mapAnim;
		currentAnimator02 = mapTitlesAnim;
		currentBackground = backgroundHome;

		BackgroundCheck();

		backgroundContentVeiwable = false;

		Button06Reset();
		StartCoroutine(MoviePage06Check());

		movieHomePage.MovieInstance.Play();
		movieIsPlaying = true;


	}


	public void HomeButtonBottom()
	{
		Debug.Log ("Home");
		debugText.text = "Debug Text: Home";

		homeButtonTopAnim.SetTrigger("FadeIn");
		buttonTouchToStartAnim.SetTrigger("AnimateIn");
		homeButtonBottomAnim.SetTrigger("AnimateOut");
		topHomeButton = true;

		button09TouchToStart.enabled = true;
		homeButtonBottom.enabled = false;

		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = true;
		currentButton = homeButtonBottom;
		currentButton.GetComponent<CanvasGroup>().blocksRaycasts = false;
		
		//Scale up upper home button
		LeanTween.scale (button08HomeActive.rectTransform, new Vector3(.4f, .4f, .4f), .2f).setEase(LeanTweenType.easeInOutExpo);
		
		//Scale down any active buttons
		LeanTween.scale (Button01active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button02active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button03active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button04active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button05active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button06active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);
		LeanTween.scale (Button07active.rectTransform, new Vector3(.0f, .0f, .0f), .3f);

		currentCanvasGroup = canvasGroup08;
		currentAnimator01 = mapAnim;
		currentAnimator02 = mapTitlesAnim;
		currentBackground = backgroundHome;

		BackgroundCheck();

		backgroundContentVeiwable = false;

		Button06Reset();

		StartCoroutine(MoviePage06Check());

		movieHomePage.MovieInstance.Play();
		movieIsPlaying = true;
	}


	public void TouchToStart()
	{
		homeButtonTopAnim.SetTrigger("FadeOut");
		buttonTouchToStartAnim.SetTrigger("AnimateOut");
		homeButtonBottomAnim.SetTrigger("AnimateIn");
		topHomeButton = false;

		homeButtonBottom.enabled = true;
		homeButtonTop.enabled = false;
		button09TouchToStart.enabled = false;
	
	}



	public void HomeButtonManager()
	{
		if(topHomeButton == true)
		{
			homeButtonTopAnim.SetTrigger("FadeOut");
			buttonTouchToStartAnim.SetTrigger("AnimateOut");
			homeButtonBottomAnim.SetTrigger("AnimateIn");
			topHomeButton = false;

			homeButtonBottom.enabled = true;
			homeButtonTop.enabled = false;
			button09TouchToStart.enabled = false;

		}
		else
		{
			/*homeButtonTopAnim.SetTrigger("FadeIn");
			homeButtonBottomAnim.SetTrigger("FadeOut");
			topHomeButton = true;*/
		}

	}

	//Controls the background image. Each button references this in their script.
	public void BackgroundCheck()
	{
		if(currentCanvasGroup == canvasGroup08)
		{
			if(backgroundContentVeiwable == false)
			{
				backgroundContent.GetComponent<Animator>().SetTrigger("FadeIn");
				//Debug.Log("Current Group is Home");
			}
			else
			{
				backgroundContent.GetComponent<Animator>().SetTrigger("FadeOut");
			}
		}
	}



	//Resets the animation on Button 06 for the content to move back to its original spot.
	public void Button06Reset()
	{
		if(button06reset == false)
		{
			button06bar.GetComponent<Animator>().SetTrigger("AnimateReset");
			
			if(Page06ContentAnimation.page01 == false)
			{
				button06ContentMover.GetComponent<Animator>().SetTrigger("AnimateReset");
				button06Arrow.GetComponent<Animator>().SetTrigger("AnimateOut");
				button06Content.GetComponent<Animator>().SetTrigger("AnimateReset");
				page06SubHeading01.GetComponent<Animator>().SetTrigger("AnimateIn");
				page06SubHeading02.GetComponent<Animator>().SetTrigger("AnimateReset");

				
				Page06ContentAnimation.page01 = true;
				button06Page01.GetComponent<Animator>().SetTrigger("AnimateReset");
				//page06SubHeading.text = "INSIDE A STATION";
			}
			else
			{
				button06Page01.GetComponent<Animator>().SetTrigger("AnimateReset");
			}
		}
		//button06Page01.GetComponent<Animator>().SetTrigger("AnimateReset");
		button06reset = true;
	}



	//Controls movie on the home page
	public IEnumerator MovieCheck()
	{
		if(movieIsPlaying == true)
		{
			//Debug.Log("Movie is playing");

			yield return new WaitForSeconds(1f);

			movieHomePage.MovieInstance.Pause();
			movieHomePage.MovieInstance.PositionSeconds = 0.0f;

			movieIsPlaying = false;
		}

		yield return null;
	}

	//Controls movie on the home page
	public IEnumerator MoviePage06Check()
	{
		if(moviePage06IsPlaying == true)
		{
			//Debug.Log("Movie is playing");
			page06movie.SetTrigger("FadeOut");

			yield return new WaitForSeconds(1f);
			
			moviePage06.MovieInstance.Pause();
			moviePage06.MovieInstance.PositionSeconds = 0.0f;
			
			moviePage06IsPlaying = false;
		}
		
		yield return null;
	}
	

}
