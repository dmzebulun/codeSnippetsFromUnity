﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Page06ContentAnimation : MonoBehaviour 
{

	private Animator mainAnim;
	public static bool page01;

	public Animator arrowAnim;
	public Animator page01Anim;
	public Animator subHeading01;
	public Animator subHeading02;

	// Use this for initialization
	void Start () 
	{
		mainAnim = GetComponent<Animator>();
		//arrowAnim = GetComponent<Animator>();
		page01 = true;
	}
	
	public void AnimateIn()
	{
		if(page01 == true)
		{
			page01Anim.SetTrigger("AnimateIn");
			mainAnim.SetTrigger("AnimateIn");
			subHeading02.SetTrigger("AnimateIn");
			subHeading01.SetTrigger("AnimateReset");
			Debug.Log ("Page01");
			page01 = false;
		}
		else
		{
			page01Anim.SetTrigger("AnimateReset");
			mainAnim.SetTrigger("AnimateReset");
			subHeading01.SetTrigger("AnimateIn");
			subHeading02.SetTrigger("AnimateReset");
			Debug.Log ("Page02");
			page01 = true;
		}

	}

	public void AnimatePage01()
	{
		page01Anim.SetTrigger("AnimateIn");
	}

	public void AnimateReset()
	{
		mainAnim.SetTrigger("AnimateReset");
		page01Anim.SetTrigger("AnimateReset");

	}
}
