﻿using UnityEngine;
using System.Collections;

public class ContentFade : MonoBehaviour 
{

	private Animator menuAnim;

	public void Awake()
	{
		menuAnim = GetComponent<Animator>();
	}



	public void FadeIn()
	{
		menuAnim.SetTrigger("FadeIn");
	}

	public void FadeOut()
	{
		//AnimationControl.currentCanvasGroup.GetComponent<Animator>().SetTrigger("Fadeout");
		menuAnim.SetTrigger("FadeOut");
	}



	/*public void FadeIn()
	{
		menuAnim.SetTrigger("FadeIn");
	}
	
	public void FadeOut()
	{
		//AnimationControl.currentCanvasGroup.GetComponent<Animator>().SetTrigger("Fadeout");
		menuAnim.SetTrigger("FadeOut");
	}*/

}
