﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Page06Button06Arrow : MonoBehaviour 
{
	private Animator mainAnim;
	public Text page06SubHeading;
	//private bool page01;

	// Use this for initialization
	void Start () 
	{
		mainAnim = GetComponent<Animator>();
	}
	
	public void AnimateIn()
	{
		if(Page06ContentAnimation.page01 == false)
		{
			mainAnim.SetTrigger("AnimateIn");

		}
		else
		{
			mainAnim.SetTrigger("AnimateOut");

		

		}
	}

	public void AnimateOut()
	{
		mainAnim.SetTrigger("AnimateOut");
	}
}
