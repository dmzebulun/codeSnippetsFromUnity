﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;
using System;

public class ApplicationReset : MonoBehaviour 
{
	private string loadedLevel;

	// Use this for initialization
	void Start () 
	{
		loadedLevel = Application.loadedLevelName;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	
	private void OnEnable()
	{
		// subscribe to gesture's Tapped event
		GetComponent<TapGesture>().Tapped += tappedHandler;
	}
	
	private void OnDisable()
	{
		// don't forget to unsubscribe
		GetComponent<TapGesture>().Tapped -= tappedHandler;
	}
	
	private void tappedHandler(object sender, EventArgs e)
	{
		Debug.Log("Tapped");
		Application.LoadLevel(loadedLevel);
	}
}
