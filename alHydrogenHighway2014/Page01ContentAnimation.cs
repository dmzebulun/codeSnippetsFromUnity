﻿using UnityEngine;
using System.Collections;

public class Page01ContentAnimation: MonoBehaviour 
{

	private Animator menuAnim;
	private float waitForMultiplier;

	public void Awake()
	{
		menuAnim = GetComponent<Animator>();
	}

	public void ScaleUp()
	{
		menuAnim.SetTrigger("ScaleUp");
	}
	
	public void ScaleDown()
	{
		menuAnim.SetTrigger("ScaleDown");
	}
}
