﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class page06Page02Content : MonoBehaviour 
{

	private Animator mainAnim;

	// Use this for initialization
	void Start () 
	{
		mainAnim = GetComponent<Animator>();
	}
	
	public void AnimateIn()
	{
			if(Page06ContentAnimation.page01 == false)
			{
				mainAnim.SetTrigger("AnimateIn");
				
				
			}
			else
			{
				mainAnim.SetTrigger("AnimateReset");
				
				
			}
	}

	public void AnimateReset()
	{
		mainAnim.SetTrigger("AnimateReset");
	}
}
