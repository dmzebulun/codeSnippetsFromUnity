﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ContentPage01AnimControl : MonoBehaviour 
{
	public Image circle01;
	public Image circle02;
	public Image circle03;
	public Image circle04;
	public Image circle05;
	public Image circle06;
	public Image circle07;



	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	public void ScaleUp()
	{
		LeanTween.scale (circle01.rectTransform, new Vector3(0,0,0), .001f);
		LeanTween.scale (circle02.rectTransform, new Vector3(0,0,0), .001f);
		LeanTween.scale (circle03.rectTransform, new Vector3(0,0,0), .001f);
		LeanTween.scale (circle04.rectTransform, new Vector3(0,0,0), .001f);
		LeanTween.scale (circle05.rectTransform, new Vector3(0,0,0), .001f);
		LeanTween.scale (circle06.rectTransform, new Vector3(0,0,0), .001f);
		LeanTween.scale (circle07.rectTransform, new Vector3(0,0,0), .001f);


		LeanTween.scale (circle01.rectTransform, new Vector3(1,1,1), .5f).setDelay(.05f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.scale (circle02.rectTransform, new Vector3(1,1,1), .5f).setDelay(.1f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.scale (circle03.rectTransform, new Vector3(1,1,1), .5f).setDelay(.2f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.scale (circle04.rectTransform, new Vector3(1,1,1), .5f).setDelay(.3f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.scale (circle05.rectTransform, new Vector3(1,1,1), .5f).setDelay(.4f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.scale (circle06.rectTransform, new Vector3(1,1,1), .5f).setDelay(.5f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.scale (circle07.rectTransform, new Vector3(1,1,1), .5f).setDelay(.5f).setEase(LeanTweenType.easeInOutSine);
	}


}
