﻿using UnityEngine;
using System.Collections;

public class CemTab : MonoBehaviour {

	public Transform videoVec; 
	public GameObject videoClone04;
    public Vector3 growValue;
    public Vector3 scaleValue;



	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

	}


	public void TabGrow()
	{
		if(DeclareOnLoad._videoPlaying == false)
		{
			LeanTween.moveLocal(this.gameObject, growValue, 2f).setEase(LeanTweenType.easeInOutSine).setOnComplete(LaunchPtVideo);
            LeanTween.scale(this.gameObject, scaleValue, 2f).setEase(LeanTweenType.easeInOutSine);
            DeclareOnLoad._videoPlaying = true;
            Timer._timer = 0;
        }
	}
	
	
	public void LaunchPtVideo()
	{

		Instantiate(videoClone04);
	}
}

