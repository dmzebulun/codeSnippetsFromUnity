﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MovieControls : MonoBehaviour 
{

	public Slider _durationSlider;
	public AVProWindowsMediaMovie _movieInstance;
	private float _realTimePosition;

	// Use this for initialization
	void Start () 
	{

	}

	void Update()
	{
		//Syncs up the Slider with the Movie's Current Run Time
		_realTimePosition = _movieInstance.MovieInstance.PositionSeconds;
		_durationSlider.value = _realTimePosition;


	}
	
	//Controls the run time of the video with the slider.
	public void UpdateDuration() 
	{
        
		_movieInstance.MovieInstance.PositionSeconds = _durationSlider.value;
	}
    
	public void PlayVideo()
	{
		Debug.Log ("This is pressed");
		_movieInstance.MovieInstance.Play();
	}

	public void PauseVideo()
	{
        _movieInstance.MovieInstance.Pause ();
	}
}
