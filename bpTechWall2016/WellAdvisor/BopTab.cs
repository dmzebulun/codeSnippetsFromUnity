﻿using UnityEngine;
using System.Collections;

public class BopTab : MonoBehaviour {

	public Transform videoVec; 
	public GameObject videoClone01;
    public Vector3 growValue;
    public Vector3 scaleValue;
    
	//public static bool _videoPlaying;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

	}



	public void TabGrow()
	{
		if(DeclareOnLoad._videoPlaying == false)
		{
			LeanTween.moveLocal(this.gameObject, growValue, 2f).setEase(LeanTweenType.easeInOutSine).setOnComplete(LaunchBopVideo);
            LeanTween.scale(this.gameObject, scaleValue, 2f).setEase(LeanTweenType.easeInOutSine);
            DeclareOnLoad._videoPlaying = true;
            Timer._timer = 0;
        }
	}
	

	public void LaunchBopVideo()
	{
		//Debug.Log ("This method is Launch");
		//Instantiate(videoClone01Prefab1,new Vector3(168.8f,73.6f,904.1f), Quaternion.Euler(0,0,0));
		Instantiate(videoClone01);
	}
}

