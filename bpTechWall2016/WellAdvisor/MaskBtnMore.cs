﻿using UnityEngine;
using System.Collections;

public class MaskBtnMore : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	
	public void MoreBtnOn()
	{
		LeanTween.moveLocal(this.gameObject, new Vector3 (0,0,0),.01f).setEase(LeanTweenType.easeInOutSine);
		
	}


	public void MoreBtnOff()
	{
		LeanTween.moveLocal(this.gameObject, new Vector3 (-1000,0,0),.01f).setEase(LeanTweenType.easeInOutSine);

	}


}
