﻿using UnityEngine;
using System.Collections;

public class Destroy : MonoBehaviour 
{
	public static bool _altVideos;
    

	public void Update()
	{

	}


	public void TabReturn()
	{
		GameObject _currentTab = GameObject.Find ("ComTab");

		LeanTween.moveLocal (_currentTab, new Vector3 (-109.4f, -87.39f, 475), 2f).setEase (LeanTweenType.easeInOutSine);
        LeanTween.scale(_currentTab, new Vector3(1, 1, 1), 2f).setEase(LeanTweenType.easeInOutSine);
		
		
	}

	public void TabReturnCEM()
	{
		GameObject _currentTab = GameObject.Find ("CemTab");
		
		LeanTween.moveLocal (_currentTab, new Vector3 (1790.3f, -375.25f, 11141.4f), 2f).setEase (LeanTweenType.easeInOutSine);
        LeanTween.scale(_currentTab, new Vector3(1, 1, 1), 2f).setEase(LeanTweenType.easeInOutSine);



    }

    public void TabReturnRSFM()
	{
		GameObject _currentTab = GameObject.Find ("RsfmTab");
		
		LeanTween.moveLocal (_currentTab, new Vector3 (2856, -2485, 11712), 2f).setEase (LeanTweenType.easeInOutSine);
        LeanTween.scale(_currentTab, new Vector3(1, 1, 1), 2f).setEase(LeanTweenType.easeInOutSine);



    }

    public void TabReturnBOP()
	{
		GameObject _currentTab = GameObject.Find ("BopTab");
		
		LeanTween.moveLocal (_currentTab, new Vector3 (-543.3f, -336.3f, 1033), 2f).setEase (LeanTweenType.easeInOutSine);
        LeanTween.scale(_currentTab, new Vector3(1, 1, 1), 2f).setEase(LeanTweenType.easeInOutSine);



    }

    public void TabReturnPT()
	{
		GameObject _currentTab = GameObject.Find ("PtTab");
		
		LeanTween.moveLocal (_currentTab, new Vector3 (1809.55f, -404.39f, 11141.4f), 2f).setEase (LeanTweenType.easeInOutSine);
        LeanTween.scale(_currentTab, new Vector3(1, 1, 1), 2f).setEase(LeanTweenType.easeInOutSine);



    }

    public void TabReturnCR()
	{
		GameObject _currentTab = GameObject.Find ("CrTab");
		
		LeanTween.moveLocal (_currentTab, new Vector3 (1809.8f, -346.1f, 11711.7f), 2f).setEase (LeanTweenType.easeInOutSine);
        LeanTween.scale(_currentTab, new Vector3(1, 1, 1), 2f).setEase(LeanTweenType.easeInOutSine);



    }
    public void TabReturnNDS()
	{
		GameObject _currentTab = GameObject.Find ("NdsTab");
		
		LeanTween.moveLocal (_currentTab, new Vector3 (2817, -2490, 11712), 2f).setEase (LeanTweenType.easeInOutSine);
        LeanTween.scale(_currentTab, new Vector3(1, 1, 1), 2f).setEase(LeanTweenType.easeInOutSine);



    }
    public void TabReturnDO()
	{
		GameObject _currentTab = GameObject.Find ("DoTab");
		
		LeanTween.moveLocal (_currentTab, new Vector3 (2816, -2490, 11712), 2f).setEase (LeanTweenType.easeInOutSine);
        LeanTween.scale(_currentTab, new Vector3(1, 1, 1), 2f).setEase(LeanTweenType.easeInOutSine);


        //_altVideos = true;


    }
    public void TabReturnRP()
	{
		GameObject _currentTab = GameObject.Find ("RpTab");
		
		LeanTween.moveLocal (_currentTab, new Vector3 (2815, -2490, 11712), 2f).setEase (LeanTweenType.easeInOutSine);
        LeanTween.scale(_currentTab, new Vector3(1, 1, 1), 2f).setEase(LeanTweenType.easeInOutSine);



    }

    public void ReturnAll()
    {
        
        TabReturnBOP();
        TabReturn();
        TabReturnCEM();
        TabReturnCR();
        TabReturnDO();
        TabReturnNDS();
        TabReturnPT();
        TabReturnRP();
        TabReturnRSFM();
        DeclareOnLoad._videoPlaying = false;

    }

    public void DestroyMe()
	{
		DeclareOnLoad._videoPlaying = false;
		//DeclareOnLoad._altVideos = false;
		Destroy(this.gameObject);

	}

	
}
