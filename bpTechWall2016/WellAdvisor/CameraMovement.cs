﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour 
{
	public GameObject _movementDriver;

	void Update () 
	{
        if(MainMenuAnimation._wellAdvisorActive == true)
        {
            this.transform.position = new Vector3(0f, (-1 * _movementDriver.transform.position.y), -5f);
        }
        

    }

    public void ResetCam()
    {
        _movementDriver.transform.position  = new Vector3(0,0,0);
        //LeanTween.move(_movementDriver, new Vector3(0, 0, 0f), 1f).setDelay(1f).setEase(LeanTweenType.easeInOutSine);
        
    }
}
