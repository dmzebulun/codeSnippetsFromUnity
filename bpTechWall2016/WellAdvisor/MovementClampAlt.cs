﻿using UnityEngine;
using System.Collections;

public class MovementClampAlt : MonoBehaviour 
{

	public bool _outOfBounds;
	public float _upperLimit;
	public float _lowerLimit;
	public Vector3 UpperLimitVector;
	public Vector3 LowerLimitVector;
	
	void Awake () 
	{
		//print (this.GetComponent<SimplePanGesture>());
	}


	// Use this for initialization
	void Start () 
	{
		_outOfBounds = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(_outOfBounds == false)
		{
			this.transform.position = new Vector3(0f, (Mathf.Clamp(this.transform.position.y, _lowerLimit, _upperLimit)), 0f);
		}

		if(this.transform.position.y >= _upperLimit)
		{
			UpperLimit();
		}

		if(this.transform.position.y <= _lowerLimit)
		{
			LowerLimit();
		}

	}

	void UpperLimit()
	{
		_outOfBounds = true;
		LeanTween.moveLocal(this.gameObject, UpperLimitVector, .2f).setEase(LeanTweenType.easeInOutSine);
		StartCoroutine(ResetOutOfBounds());
        
    }

	void LowerLimit()
	{
		_outOfBounds = true;
		LeanTween.moveLocal(this.gameObject, LowerLimitVector, .2f).setEase(LeanTweenType.easeInOutSine);
		StartCoroutine(ResetOutOfBounds());
        
    }

	public void CamSetvec()
	{
		LeanTween.moveLocal(this.gameObject, new Vector3 (0,0,0), 1.5f).setEase(LeanTweenType.easeInOutSine);
	
	}

	public void CamSetMid()
	{
		LeanTween.moveLocal(this.gameObject, new Vector3 (0,225.317f,0), 1.5f).setEase(LeanTweenType.easeInOutSine);
		
	}

	public void CamSetLow()
	{
		LeanTween.moveLocal(this.gameObject, new Vector3 (0,420,0), 1.5f).setEase(LeanTweenType.easeInOutSine);
		
	}

	public IEnumerator ResetOutOfBounds()
	{
		yield return new WaitForSeconds(.4f);
		_outOfBounds = false;
	}

    public void ResetCam()
    {
        LeanTween.moveLocal(this.gameObject, new Vector3(0, 0, 0), 1.5f).setEase(LeanTweenType.easeInOutSine);
    }
}
