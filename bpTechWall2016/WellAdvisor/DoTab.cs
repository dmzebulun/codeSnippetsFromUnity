﻿using UnityEngine;
using System.Collections;

public class DoTab : MonoBehaviour {

	public Transform videoVec; 
	public GameObject videoClone07;
	public GameObject videoClone08;
    public Vector3 growValue;
    public Vector3 scaleValue;
	
	
	// Use this for initialization
	void Start () {
		DeclareOnLoad._videoPlaying = false;
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void LaunchWBSVideo()
	{
		if (DeclareOnLoad._altVideos == true) {
			Instantiate (videoClone07);
		}
	}

	public void TabGrow()
	{
		 if(DeclareOnLoad._videoPlaying == false)
		{
			LeanTween.moveLocal(this.gameObject, growValue,2f).setEase(LeanTweenType.easeInOutSine);
            LeanTween.scale(this.gameObject, scaleValue, 2f).setEase(LeanTweenType.easeInOutSine);
            DeclareOnLoad._videoPlaying = true;
			DeclareOnLoad._altVideos = true;
            Timer._timer = 0;
        }

	}


	public void TabSetBack()
	{
		if (DeclareOnLoad._altVideos == true) {
            LeanTween.moveLocal(this.gameObject, new Vector3(2816, -2490, 11712), 2f).setEase(LeanTweenType.easeInOutSine);
            LeanTween.scale(this.gameObject, new Vector3(1,1,1), 2f).setEase(LeanTweenType.easeInOutSine);
            DeclareOnLoad._altVideos = false;
			DeclareOnLoad._videoPlaying = false;
		}
	}


	public void LaunchHCVideo()
	{
		//Debug.Log ("This method is Launch");
		//Instantiate(videoClone01Prefab1,new Vector3(168.8f,73.6f,904.1f), Quaternion.Euler(0,0,0));
		if (DeclareOnLoad._altVideos == true) {
			Instantiate (videoClone08);
		}
	}




}

