﻿using UnityEngine;
using System.Collections;

public class MaskReveal : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void MaskShow()
	{
        //LeanTween.moveLocal(this.gameObject, new Vector3 (0,-95,0),1f).setEase(LeanTweenType.easeInOutSine);
        LeanTween.scaleZ(this.gameObject, 0f, 1f).setEase(LeanTweenType.easeInOutSine);
	}

	public void MaskHide()
	{
        //LeanTween.moveLocal(this.gameObject, new Vector3 (0,-70,0),1f).setEase(LeanTweenType.easeInOutSine);
        LeanTween.scaleZ(this.gameObject, 4.6f, 1f).setEase(LeanTweenType.easeInOutSine);
    }
}
