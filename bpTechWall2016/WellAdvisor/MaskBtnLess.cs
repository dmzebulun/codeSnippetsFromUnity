﻿using UnityEngine;
using System.Collections;

public class MaskBtnLess : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void LessBtnOn()
	{
		LeanTween.moveLocal(this.gameObject, new Vector3 (0,0,0),.01f).setEase(LeanTweenType.easeInOutSine);

	}

	public void LessBtnOff()
	{
		LeanTween.moveLocal(this.gameObject, new Vector3 (-1000,0,0),.01f).setEase(LeanTweenType.easeInOutSine);
		
	}


}
