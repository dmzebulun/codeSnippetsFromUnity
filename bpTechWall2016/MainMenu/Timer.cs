﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class Timer : MonoBehaviour
{
	public static float _timer;
	public float _timeOut = 600f;
	public GameObject _prefabScreenSaver;
	public static GameObject _activeScreenSaver;
	public bool _timerDone;
	public static bool _screenSaverActive;
	public GameObject _menuAnimationScript;
    public GameObject mainCamera;
    public static bool _isLooping;
    


	void Start()
	{
		_timerDone = false;
		_screenSaverActive = false;
		StartCoroutine(EventTimer());

	}

	void Update()
	{
		if(_timer == (_timeOut-1) )
		{
			_timer =0;

			if(_screenSaverActive == false)
			{
				_activeScreenSaver = Instantiate(_prefabScreenSaver, new Vector3(0,0,0), Quaternion.Euler(0,0,0)) as GameObject;
                //_activeScreenSaver.transform.parent = mainCamera.transform;
				_isLooping = true;
				_menuAnimationScript.GetComponent<MainMenuAnimation>().ResetMainMenu();
				//mainCamera.GetComponent<CameraMovement>().ResetCam();
                KillWellVideos();
                _screenSaverActive = true;
                

				if(ContentCloner._currentClonedContent != null)
				{
					Destroy(ContentCloner._currentClonedContent);
				}
			}


		}
	}


	public IEnumerator EventTimer()
	{
		for(_timer=0; _timer<=_timeOut; _timer++)
		{
			yield return new WaitForSeconds(1f);
			Debug.Log (_timer);


		}
		yield return null;
	}

    public void ResetTimer()
    {
       _timer = 0;
    }

    public void KillWellVideos()
    {
        if(GameObject.FindGameObjectWithTag("video") == true)
        {
            
            GameObject video = GameObject.FindGameObjectWithTag("video");
            video.GetComponent<Destroy>().ReturnAll();
            DestroyObject(video);
        }
    }


	
}
