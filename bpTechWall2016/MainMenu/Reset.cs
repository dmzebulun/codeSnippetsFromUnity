﻿using UnityEngine;
using System.Collections;
using System;
using TouchScript.Gestures;

public class Reset : MonoBehaviour 
{

	private void OnEnable()
	{
		// subscribe to gesture's tapped event
		GetComponent<TapGesture>().Tapped += tappedHandler;
	}
	
	private void OnDisable()
	{
		// subscribe to gesture's tapped event
		GetComponent<TapGesture>().Tapped += tappedHandler;
	}
	
	private void tappedHandler(object sender, EventArgs e)
	{
		Debug.Log ("Quitting");
		Application.LoadLevel(0);
	}
}

