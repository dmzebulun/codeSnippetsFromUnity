﻿using UnityEngine;
using System.Collections;

public class DestroyThis : MonoBehaviour 
{
	public GameObject _timerObject;

	public void MoveAndDestroy()
	{
		if(ContentCloner._currentClonedContent != null)
		{
		LeanTween.value(ContentCloner._currentClonedContent.gameObject, 1f, 0f, 1f).setOnUpdate((float alpha)=>{ContentCloner._currentAlpha = alpha;}).setOnComplete(SelfDestruct);
	
		}
	}

	public void SelfDestruct()
	{

		Destroy(ContentCloner._currentClonedContent);
		CloneVideo._videoPlaying = false;
	}

	public void DestoryVideo()
	{
		//Timer._timer = 0;
		Destroy(this.gameObject);
		CloneVideo._videoPlaying = false;
		Timer._screenSaverActive = false;
		VideoLoop._isLooping = false;


	}


}
