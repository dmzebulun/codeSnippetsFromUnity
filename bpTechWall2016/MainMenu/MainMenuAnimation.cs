﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuAnimation : MonoBehaviour 
{
    public static bool _wellAdvisorActive;

	public GameObject _whiteLeft;
	public GameObject _deepWaterHorizon;
	public GameObject _saferDrilling;
	public GameObject _capabilityToRespond;
	public GameObject _capabilityToRespondSub01;
	public GameObject _capabilityToRespondSub02;
	public GameObject _processSafety;
	public GameObject _whiteRight;
	public GameObject _bottomNav;

	public GameObject _deepWaterHorizonCatButton;
	public GameObject _saferDrillingCatButton;
	public GameObject _capabilityToRespondCatButton;
	public GameObject _processSafetyCatButton;

	public GameObject _homeIcon;
	public GameObject _homeButtonText;

    public GameObject _mainMenuBG;
    public float _currentAlpha;

	private bool _atHome;
    public static bool _dontMove;

    public GameObject _movementDriver;


	void Start()
	{
		_atHome = true;
        _currentAlpha = 1;
        _wellAdvisorActive = false;
	}

    // Update is called once per frame
    void Update()
    {
        
            _mainMenuBG.GetComponentInChildren<CanvasGroup>().alpha = _currentAlpha;

        
    }


    public void ResetMainMenu()
	{	
		Debug.Log ("Reseting Main Menu");
		Timer._timer =0;

		LeanTween.moveLocal(_whiteLeft,           new Vector3(-280f, 16f, -235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.moveLocal(_saferDrilling,       new Vector3(-149f, 16f, -235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.moveLocal(_capabilityToRespond, new Vector3(0f, 16f, -235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.moveLocal(_processSafety,       new Vector3(149f, 16f, -235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.moveLocal(_whiteRight,          new Vector3(280f, 16f, -235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);

		LeanTween.moveLocal(_bottomNav,           new Vector3(0,0,0), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);

		LeanTween.moveLocal(_deepWaterHorizonCatButton,    new Vector3(-267f, -110f, -290f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.moveLocal(_saferDrillingCatButton,       new Vector3(-267f, -110f, -290f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.moveLocal(_capabilityToRespondCatButton, new Vector3(-267f, -110f, -290f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
        LeanTween.moveLocal(_processSafetyCatButton, new Vector3(-267f, -110f, -290f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);

        // Fade out BG
        if (_currentAlpha == 0)
        {
            LeanTween.value(_mainMenuBG.gameObject, 0f, 1f, 1f).setDelay(.5f).setOnUpdate((float alpha) => { _currentAlpha = alpha; });
        }
        //_movementDriver.transform.position = new Vector3(0, 0, 0);
        LeanTween.move(_movementDriver, new Vector3(0, 0, 0f), 1f).setEase(LeanTweenType.easeInOutSine);

        _atHome = true;
        //_wellAdvisorActive = false;
        StartCoroutine(boolWait());

    }


	public void Menu01()
	{
		Debug.Log ("Menu 01");
		Timer._timer =0;

		if(_atHome == true)
		{
            _wellAdvisorActive = true;
            // Move Panels
			LeanTween.moveLocal(_whiteLeft,           new Vector3(-542f,16f,-235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.moveLocal(_saferDrilling,       new Vector3(-411f,16f,-235f), 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.moveLocal(_capabilityToRespond, new Vector3(410f,16f,-235f), 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.moveLocal(_processSafety,       new Vector3(559f,16f,-235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.moveLocal(_whiteRight,          new Vector3(690f,16f,-235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);


            // Fade out BG
            LeanTween.value(_mainMenuBG.gameObject, 1f, 0f, 1f).setDelay(.5f).setOnUpdate((float alpha) => { _currentAlpha = alpha; });



            // Move Bottom Nav if present
            LeanTween.moveLocal(_saferDrillingCatButton, new Vector3(-161f, -110f, -290f), 1f).setEase(LeanTweenType.easeInOutQuad);
			
            // Turn off other main menu button check
			_atHome = false;

			//MenuBottomNav();
		}


	}

	public void Menu02()
	{
        Debug.Log("Menu 02");
        Timer._timer = 0;
        if (_atHome == true)
        {

                        
            LeanTween.moveLocal(_whiteLeft,             new Vector3(-687f, 16f, -235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
            LeanTween.moveLocal(_saferDrilling,         new Vector3(-556f, 16f, -235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);
            LeanTween.moveLocal(_capabilityToRespond,   new Vector3(-407f, 16f, -235f), 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuad);
            LeanTween.moveLocal(_processSafety,         new Vector3(408f, 16f, -235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);
            LeanTween.moveLocal(_whiteRight,            new Vector3(540f, 16f, -235f), 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuad);
                          
            LeanTween.moveLocal(_capabilityToRespondCatButton, new Vector3(-161f, -110f, -290f), 1f).setEase(LeanTweenType.easeInOutQuad);

            // Fade out BG
            //LeanTween.value(_mainMenuBG.gameObject, 1f, 0f, 1f).setDelay(.5f).setOnUpdate((float alpha) => { _currentAlpha = alpha; });


            _atHome = false;

        }


    }

	public void Menu03()
	{
		Debug.Log ("Menu 03");
        Timer._timer = 0;
        if (_atHome == true)
		{

			LeanTween.moveLocal(_whiteLeft,           new Vector3(-687f,16f,-235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.moveLocal(_saferDrilling,       new Vector3(-556f,16f,-235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.moveLocal(_capabilityToRespond, new Vector3(-407f,16f,-235f), 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.moveLocal(_processSafety,       new Vector3(408f,16f,-235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.moveLocal(_whiteRight,          new Vector3(540f, 16f,-235f), 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuad);
			              
			LeanTween.moveLocal(_capabilityToRespondCatButton, new Vector3(-161f, -110f, -290f), 1f).setEase(LeanTweenType.easeInOutQuad);
			
			
			_atHome = false;

		}
		
		
	}

	

	public void MenuBottomNav()
	{
		LeanTween.move (_bottomNav, new Vector3(370f, 0, 0), 1f).setEase(LeanTweenType.easeInOutSine);
		//LeanTween.scale(_homeButtonText, new Vector3(.5f, .5f, .5f), 1f).setEase(LeanTweenType.easeInOutExpo);
		//LeanTween.scale(_homeIcon, new Vector3(2,2,2), 1f).setEase(LeanTweenType.easeInOutExpo);
	}

    public IEnumerator boolWait()
    {
        yield return new WaitForSeconds(2f);
        _wellAdvisorActive = false;
    }

   
}
