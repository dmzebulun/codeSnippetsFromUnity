﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ContentCloner : MonoBehaviour 
{
	public static GameObject _currentClonedContent;
	public static float _currentAlpha;
	public Object _prefab01;
	public Object _prefab02;
	public Object _prefab03;
	public Object _prefab04;


	// Update is called once per frame
	void Update () 
	{
		if(_currentClonedContent != null)
		{
			_currentClonedContent.GetComponentInChildren<CanvasGroup>().alpha = _currentAlpha;

		}
		Debug.Log (_currentClonedContent);
	}


	public void Clone01()
	{
		_currentClonedContent = Instantiate(_prefab01, new Vector3(0,8,0), Quaternion.Euler(0,0,0)) as GameObject;
		LeanTween.value(_currentClonedContent.gameObject, 0f, 1f, 1f).setOnUpdate((float alpha)=>{_currentAlpha = alpha;});

	}

	public void Clone02()
	{
		if(_currentClonedContent == null)
		{
			_currentClonedContent = Instantiate(_prefab02) as GameObject;
			LeanTween.value(_currentClonedContent.gameObject, 0f, 1f, 1f).setOnUpdate((float alpha)=>{_currentAlpha = alpha;});
		}


	}

	public void Clone03()
	{
		if(_currentClonedContent == null)

        {

                _currentClonedContent = Instantiate(_prefab03) as GameObject;
                //LeanTween.value(_currentClonedContent.gameObject, 0f, 1f, 1f).setOnUpdate((float alpha) => { _currentAlpha = alpha; });
            
        }
	}

	public void Clone04()
	{
		if(_currentClonedContent == null)
		{

		_currentClonedContent = Instantiate(_prefab04, new Vector3(0,8,0), Quaternion.Euler(0,0,0)) as GameObject;
		LeanTween.value(_currentClonedContent.gameObject, 0f, 1f, 1f).setOnUpdate((float alpha)=>{_currentAlpha = alpha;});
		}
	}
}
