﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ContentMover : MonoBehaviour
{

    public GameObject contentMover;

    public GameObject _currentContent;

    public GameObject predictContent;
    public GameObject pinpointContent;
    public GameObject preventContent;

    public Button predictButton;
    public Sprite predictButtonInactive;
    public Sprite predictButtonActive;
    public Button pinpointButton;
    public Sprite pinpointButtonInactive;
    public Sprite pinpointButtonActive;
    public Button preventButton;
    public Sprite preventButtonInactive;
    public Sprite preventButtonActive;

    public Slider contentSlider;
    public float sliderResetValue;

    public float predictLeftLimit;
    public float predictRightLimit;

    public float pinpointLeftLimit;
    public float pinpointRightLimit;

    public float preventLeftLimit;
    public float preventRightLimit;

   

    public void Start()
    {
        InitialAnimation();
        
        
    }

    public void Update()
    {
        _currentContent.transform.position = new Vector3((contentSlider.value * -1), _currentContent.transform.position.y, _currentContent.transform.position.z);
    }

    public void InitialAnimation()
    {
        //Fade In

        // Set the min/max for the slider so it fits the content
        contentSlider.value = sliderResetValue;
        contentSlider.minValue = predictLeftLimit;
        contentSlider.maxValue = predictRightLimit;

        // Set this object to current so the slider can control it. 
        _currentContent = predictContent;


        predictButton.GetComponent<Image>().sprite = predictButtonActive;
        
    }


    public void PredictButton()
    {
        Timer._timer = 0;
        // Set the min/max for the slider so it fits the content
        contentSlider.minValue = predictLeftLimit;
        contentSlider.maxValue = predictRightLimit;
        
        // Set this object to current so the slider can control it. 
        _currentContent = predictContent;

        // Move this object into view vertically
        LeanTween.moveLocal(contentMover, new Vector3(0, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuad);

        // Reset the other object back to zero
        LeanTween.moveLocalX(pinpointContent, 44.5f, 1f).setDelay(1f);
        LeanTween.moveLocalX(preventContent, 38.5f, 1f).setDelay(1f);
        contentSlider.value = sliderResetValue;

        // Button Sprite Management
        predictButton.GetComponent<Image>().sprite = predictButtonActive;
        pinpointButton.GetComponent<Image>().sprite = predictButtonInactive;
        preventButton.GetComponent<Image>().sprite = predictButtonInactive;
    }

    public void PinpointButton()
    {
        Timer._timer = 0;
        // Set the min/max for the slider so it fits the content
        contentSlider.minValue = pinpointLeftLimit;
        contentSlider.maxValue = pinpointRightLimit;

        // Set this object to current so the slider can control it. 
        _currentContent = pinpointContent;

        // Move this object into view  vertically
        LeanTween.moveLocal(contentMover, new Vector3(0, 500f, 0), 1f).setEase(LeanTweenType.easeInOutQuad);


        // Reset the other object back to zero
        LeanTween.moveLocalX(predictContent, 44.5f, 1f).setDelay(1f);
        LeanTween.moveLocalX(preventContent, 38.4f, 1f).setDelay(1f);
        contentSlider.value = sliderResetValue;

        // Button Sprite Management
        predictButton.GetComponent<Image>().sprite = predictButtonInactive;
        pinpointButton.GetComponent<Image>().sprite = pinpointButtonActive;
        preventButton.GetComponent<Image>().sprite = preventButtonInactive;
    }

    public void PreventButton()
    {
        Timer._timer = 0;
        // Set the min/max for the slider so it fits the content
        contentSlider.minValue = preventLeftLimit;
        contentSlider.maxValue = preventRightLimit;

        // Set this object to current so the slider can control it. 
        _currentContent = preventContent;

        // Move this object into view  vertically
        LeanTween.moveLocal(contentMover, new Vector3(0, 1000f, 0), 1f).setEase(LeanTweenType.easeInOutQuad);

        // Reset the other object back to zero
        LeanTween.moveLocalX(pinpointContent, 44.5f, 1f).setDelay(1f);
        LeanTween.moveLocalX(predictContent, 44.5f, 1f).setDelay(1f);

        contentSlider.value = sliderResetValue;

        // Button Sprite Management
        predictButton.GetComponent<Image>().sprite = predictButtonInactive;
        pinpointButton.GetComponent<Image>().sprite = pinpointButtonInactive;
        preventButton.GetComponent<Image>().sprite = preventButtonActive;

    }
}
