﻿using UnityEngine;
using System.Collections;

public class MenuControls : MonoBehaviour 
{
	public Animator _controlPanelAnim;
	public GameObject _controlPanel;
	public bool _menuOut;

	// Use this for initialization
	void Start()
	{
		//StartControlPanel();
		_menuOut = true;
	}


	void Update()
	{
		if(_controlPanel.transform.position.x >= .77f)
		{
			_menuOut = false;
		}
		else
		{
			_menuOut = true;
		}
	}

	public void ResetControlPanel()
	{
		if(_menuOut == false)
		{
			Debug.Log ("Resetting Control Panel");
			_controlPanelAnim.Play("VideoControlsAnim", -1, 0f);

		}


	}
	

}
