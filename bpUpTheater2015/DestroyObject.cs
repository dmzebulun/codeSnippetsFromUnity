﻿using UnityEngine;
using System.Collections;

public class DestroyObject : MonoBehaviour 
{

	private GameObject _shardPanel;
	public GameObject _videoClone;


	public void ReturnShard()
	{
		//Find Shard Panel to return to menu position
		_shardPanel = GameObject.Find("ShardPanel");

		//Move video panel to the left of the menu
		LeanTween.moveLocal(_videoClone, new Vector3(-19,0,0), 1f).setEase(LeanTweenType.easeInOutSine);

		//move Shard panel to menu position and destroy video on completeion.
		LeanTween.moveLocal(_shardPanel, new Vector3(-17.61f, -0.85f, -0.9f), 1f).setEase(LeanTweenType.easeInOutSine).setOnComplete(DestroyThisObject);
	}



	public void DestroyThisObject()
	{
		Destroy(this.gameObject);
		Debug.Log("Object Destroyed");
		VideoClone._isThereAVideo = false;
	}



}
