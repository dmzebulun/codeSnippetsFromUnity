﻿using UnityEngine;
using System.Collections;

public class VideoClone : MonoBehaviour 
{
	public static bool _videoLooping;
	public static bool _isThereAVideo;



	public GameObject _prefabToClone01;
	public GameObject _prefabToClone02;
	public GameObject _prefabToClone03;
	public GameObject _prefabToClone04;
	public GameObject _prefabToClone05;

	public GameObject _shardPanel;

	public GameObject _currentVideo;

	// Use this for initialization
	void Start () 
	{
		_videoLooping = false;
		_isThereAVideo = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}


	public void VideoPlayback(int videoNumber)
	{
		if(videoNumber == 1 && _isThereAVideo == false)
		{
			Debug.Log ("Video 01 Cloned");

			//Turn off menu buttons while there is a video up. This prevents multiple videos as once. 
			_isThereAVideo = true;

			//Clone video and start playing
			_currentVideo = Instantiate(_prefabToClone01, new Vector3(-19,0,0), Quaternion.Euler(0,0,0)) as GameObject;

			//Move the Shard Panel Into cameera view
			LeanTween.moveLocal(_shardPanel, new Vector3(0.83f,-0.95f,-0.9f), 1f).setEase(LeanTweenType.easeInOutSine);

			//Move the videoPane into view
			LeanTween.moveLocal(_currentVideo, new Vector3(0,0,0), 1f).setEase(LeanTweenType.easeInOutSine);

		}

		if(videoNumber == 2 && _isThereAVideo == false)
		{
			Debug.Log ("Video 02 Cloned");

			//Turn off menu buttons while there is a video up. This prevents multiple videos as once. 
			_isThereAVideo = true;
			
			//Clone video and start playing
			_currentVideo = Instantiate(_prefabToClone02, new Vector3(-19,0,0), Quaternion.Euler(0,0,0)) as GameObject;
			
			//Move the Shard Panel Into cameera view
			LeanTween.moveLocal(_shardPanel, new Vector3(0.83f,-0.95f,-0.9f), 1f).setEase(LeanTweenType.easeInOutSine);
			
			//Move the videoPane into view
			LeanTween.moveLocal(_currentVideo, new Vector3(0,0,0), 1f).setEase(LeanTweenType.easeInOutSine);
			
		}

		if(videoNumber == 3 && _isThereAVideo == false)
		{
			Debug.Log ("Video 03 Cloned");

			//Turn off menu buttons while there is a video up. This prevents multiple videos as once. 
			_isThereAVideo = true;
			
			//Clone video and start playing
			_currentVideo = Instantiate(_prefabToClone03, new Vector3(-19,0,0), Quaternion.Euler(0,0,0)) as GameObject;
			
			//Move the Shard Panel Into cameera view
			LeanTween.moveLocal(_shardPanel, new Vector3(0.83f,-0.95f,-0.9f), 1f).setEase(LeanTweenType.easeInOutSine);
			
			//Move the videoPane into view
			LeanTween.moveLocal(_currentVideo, new Vector3(0,0,0), 1f).setEase(LeanTweenType.easeInOutSine);
			
		}

		if(videoNumber == 4 && _isThereAVideo == false)
		{
			Debug.Log ("Video 04 Cloned");

			//Turn off menu buttons while there is a video up. This prevents multiple videos as once. 
			_isThereAVideo = true;
			
			//Clone video and start playing
			_currentVideo = Instantiate(_prefabToClone04, new Vector3(-19,0,0), Quaternion.Euler(0,0,0)) as GameObject;
			
			//Move the Shard Panel Into cameera view
			LeanTween.moveLocal(_shardPanel, new Vector3(0.83f,-0.95f,-0.9f), 1f).setEase(LeanTweenType.easeInOutSine);
			
			//Move the videoPane into view
			LeanTween.moveLocal(_currentVideo, new Vector3(0,0,0), 1f).setEase(LeanTweenType.easeInOutSine);
			
		}

		if(videoNumber == 5 && _isThereAVideo == false)
		{
			Debug.Log ("Video 05 Cloned");

			//Turn off menu buttons while there is a video up. This prevents multiple videos as once. 
			_isThereAVideo = true;
			
			//Clone video and start playing
			_currentVideo = Instantiate(_prefabToClone05, new Vector3(-19,0,0), Quaternion.Euler(0,0,0)) as GameObject;
			
			//Move the Shard Panel Into cameera view
			LeanTween.moveLocal(_shardPanel, new Vector3(0.83f,-0.95f,-0.9f), 1f).setEase(LeanTweenType.easeInOutSine);
			
			//Move the videoPane into view
			LeanTween.moveLocal(_currentVideo, new Vector3(0,0,0), 1f).setEase(LeanTweenType.easeInOutSine);
			
		}

	}
}
