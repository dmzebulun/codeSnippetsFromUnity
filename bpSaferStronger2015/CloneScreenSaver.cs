﻿using UnityEngine;
using System.Collections;
using System;
using TouchScript.Gestures;

public class CloneScreenSaver : MonoBehaviour 
{

	public GameObject _screenSaverVideo;
	
	private void OnEnable()
	{
		// subscribe to gesture's tapped event
		GetComponent<TapGesture>().Tapped += tappedHandler;
	}
	
	private void OnDisable()
	{
		// subscribe to gesture's tapped event
		GetComponent<TapGesture>().Tapped -= tappedHandler;
	}
	
	private void tappedHandler(object sender, EventArgs e)
	{	
		Debug.Log ("Tapped is working");
		
		GameObject _ss = Instantiate(_screenSaverVideo, new Vector3(0,8,0), Quaternion.Euler(0,0,0)) as GameObject;
		VideoLoop._isLooping = true;
		Timer._screenSaverActive = true;
		if(ContentCloner._currentClonedContent != null)
		{
			Destroy(ContentCloner._currentClonedContent);
		}

		
	}
}
