﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;
using System;

public class SecretPresentation : MonoBehaviour 
{

	public GameObject _whiteLeft;
	public GameObject _saferDrilling;
	public GameObject _capabilityToRespond;
	public GameObject _processSafety;
	public GameObject _whiteRight;
	public GameObject _deepWaterHorizonCatButton;

	public GameObject _prefabDWH;

	private void OnEnable()
	{
		// subscribe to gesture's tapped event
		GetComponent<TapGesture>().Tapped += tappedHandler;
	}

	private void OnDisable()
	{
		// subscribe to gesture's tapped event
		GetComponent<TapGesture>().Tapped -= tappedHandler;
	}

	private void tappedHandler(object sender, EventArgs e)
	{	
		Debug.Log ("Tapped is working");

		ContentCloner._currentClonedContent = Instantiate(_prefabDWH, new Vector3(0,8,0), Quaternion.Euler(0,0,0)) as GameObject;

		LeanTween.move(_whiteLeft,           new Vector3(-497f,16f,-235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
		//LeanTween.move(_deepWaterHorizon,    new Vector3(-342f,16f,-235f), 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.move(_saferDrilling,       new Vector3(-366f,16f,-235f), 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.move(_capabilityToRespond, new Vector3(377f,16f,-235f), 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.move(_processSafety,       new Vector3(526f,16f,-235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.move(_whiteRight,          new Vector3(657f,16f,-235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);
		
		LeanTween.move(_deepWaterHorizonCatButton, new Vector3(-161f, -110f, -290f), 1f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.value(ContentCloner._currentClonedContent.gameObject, 0f, 1f, 1f).setOnUpdate((float alpha)=>{ContentCloner._currentAlpha = alpha;});

	}
}
