﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VolumeControl : MonoBehaviour 
{
	public Slider volumeSlider;
	public AVProQuickTimeMovie video;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
 	
		video.GetComponent<AVProQuickTimeMovie>()._volume = volumeSlider.value;
	}
}
