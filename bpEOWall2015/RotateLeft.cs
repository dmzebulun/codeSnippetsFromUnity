﻿using UnityEngine;
using System.Collections;

public class RotateLeft : MonoBehaviour 
{

	public float Speed;
	

	// Update is called once per frame
	void Update () 
	{
		//Debug.Log (transform.rotation);
		transform.Rotate(Vector3.back, Time.deltaTime*Speed);

	}

}
