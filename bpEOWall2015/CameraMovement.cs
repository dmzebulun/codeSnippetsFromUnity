﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour 
{


	public Animator FactSheetsButtonAnim;
	public Animator VideosMenuButtonAnim;
	public Vector3 CamLocationVideos;
	public Vector3 CamLocationFactSheets;
	public GameObject mainCamera;

	public GameObject VideosMenu;
	public GameObject FactSheetsMenu;

	public GameObject FactSheetsButton; 
	public GameObject VideosButton; 
	private GameObject destroyClone;

	public GameObject _videoControllerClone;

	public void Start()
	{
	}
	void Update()
	{
		//Debug.Log (transform.position);
	}

	//Moves the Camera to the Videos Menu
	public void MoveToCamLocationVideos()
	{
		LeanTween.moveLocal(mainCamera, CamLocationVideos, 1f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.rotateLocal(VideosMenu, new Vector3(0,195,0), 1f).setDelay(1f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.rotateLocal(FactSheetsMenu, new Vector3(0,125,0), 1f).setEase(LeanTweenType.easeInOutSine);

		LeanTween.moveLocal(FactSheetsButton, new Vector3(-450,-235,-800), 1f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.rotateLocal(FactSheetsButton, new Vector3(0,175,0), 1f).setEase(LeanTweenType.easeInOutSine);

		LeanTween.moveLocal(VideosButton, new Vector3(560,-235,-800), 1f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.rotateLocal(VideosButton, new Vector3(0,290,0), 1f).setEase(LeanTweenType.easeInOutSine);

	}

	//Moves the Camera to the FactSheets Menu
	public void MoveToCamLocationFactSheets()
	{
		LeanTween.moveLocal(mainCamera, CamLocationFactSheets, 1f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.rotateLocal(VideosMenu, new Vector3(0,290,0), 1f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.rotateLocal(FactSheetsMenu, new Vector3(0,180,0), 1f).setDelay(1f).setEase(LeanTweenType.easeInOutSine);

		LeanTween.moveLocal(FactSheetsButton, new Vector3(-560,-235,-800), 1f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.rotateLocal(FactSheetsButton, new Vector3(0,70,0), 1f).setEase(LeanTweenType.easeInOutSine);

		LeanTween.moveLocal(VideosButton, new Vector3(450,-235,-800), 1f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.rotateLocal(VideosButton, new Vector3(0,190,0), 1f).setEase(LeanTweenType.easeInOutSine);
	}
}
