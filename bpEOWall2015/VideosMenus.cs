﻿using UnityEngine;
using System.Collections;

public class VideosMenus : MonoBehaviour 
{
	public GameObject VideoMenuObject;
	public GameObject FirstButtonMenu;
	public GameObject SecondButtonMenu;


	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void MenuSpin(int choice)
	{	
		if(choice == 1)
		{
			LeanTween.rotateLocal(VideoMenuObject, new Vector3(0,195,0), 1f).setEase(LeanTweenType.easeInOutSine);
		}
		else
		{
			LeanTween.rotateLocal(VideoMenuObject, new Vector3(0,15,0), 1f).setEase(LeanTweenType.easeInOutSine);
		}
	}
}
