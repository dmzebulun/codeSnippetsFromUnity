﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FactSheetClone : MonoBehaviour 
{
	public GameObject prefabToClone01;
	public GameObject prefabToClone02;
	public GameObject prefabToClone03;
	public GameObject prefabToClone04;
	public GameObject prefabToClone05;
	public GameObject prefabToClone06;
	public GameObject prefabToClone07;
	public GameObject prefabToClone08;
	public GameObject prefabToClone09;
	public GameObject prefabToClone10;

	public static bool CloneAlive;
	public static Object currentClone;
	public Sprite sourcePanelSprite;
	public Sprite panelSprite;
	public Sprite tile01;
	public Sprite tile02;
	public Sprite tile03;
	public Sprite tile04;
	public Sprite tile05;
	public Sprite tile06;
	public Sprite tile07;
	public Sprite tile08;
	public Sprite tile09;
	public Sprite tile10;


	// Use this for initialization
	void Start () 
	{
		CloneAlive = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		Debug.Log (CloneAlive);
		Debug.Log (currentClone);
		Debug.Log (tile01);

	}

	public void FactSheetCreate(int tile)
	{
		if(CloneAlive == false && tile == 1)
		{
			currentClone = Instantiate(prefabToClone01, new Vector3(-97f, 84f, 168.5f), Quaternion.Euler(0f,0f,0f));
			CloneAlive = true;
		
		}

		if(CloneAlive == false && tile == 2)
		{
			currentClone = Instantiate(prefabToClone02, new Vector3(-97f, 84f, 168.5f), Quaternion.Euler(0f,0f,0f));
			CloneAlive = true;
	
		}

		if(CloneAlive == false && tile == 3)
		{
			currentClone = Instantiate(prefabToClone03, new Vector3(-97f, 84f, 168.5f), Quaternion.Euler(0f,0f,0f));
			CloneAlive = true;
		}

		if(CloneAlive == false && tile == 4)
		{
			currentClone = Instantiate(prefabToClone04, new Vector3(-97f, 84f, 168.5f), Quaternion.Euler(0f,0f,0f));
			CloneAlive = true;
		}

		if(CloneAlive == false && tile == 5)
		{
			currentClone = Instantiate(prefabToClone05, new Vector3(-97f, 84f, 168.5f), Quaternion.Euler(0f,0f,0f));
			CloneAlive = true;
		}

		if(CloneAlive == false && tile == 6)
		{
			currentClone = Instantiate(prefabToClone06, new Vector3(-97f, 84f, 168.5f), Quaternion.Euler(0f,0f,0f));
			CloneAlive = true;
		}

		if(CloneAlive == false && tile == 7)
		{
			currentClone = Instantiate(prefabToClone07, new Vector3(-97f, 84f, 168.5f), Quaternion.Euler(0f,0f,0f));
			CloneAlive = true;
		}

		if(CloneAlive == false && tile == 8)
		{
			currentClone = Instantiate(prefabToClone08, new Vector3(-97f, 84f, 168.5f), Quaternion.Euler(0f,0f,0f));
			CloneAlive = true;
		}

		if(CloneAlive == false && tile == 9)
		{
			currentClone = Instantiate(prefabToClone09, new Vector3(-97f, 84f, 168.5f), Quaternion.Euler(0f,0f,0f));
			CloneAlive = true;
		}

		if(CloneAlive == false && tile == 10)
		{
			currentClone = Instantiate(prefabToClone10, new Vector3(-97f, 84f, 168.5f), Quaternion.Euler(0f,0f,0f));
			CloneAlive = true;
		}
	}

	public void FactSheetScale()
	{
		LeanTween.scale(this.gameObject, new Vector3(0,0,0), .5f).setEase(LeanTweenType.easeInOutSine);

	}

	/*public void FactSheetDestroy()
	{
		DestroyImmediate(prefabToClone);
	}*/
}
