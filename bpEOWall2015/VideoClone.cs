﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class VideoClone : MonoBehaviour 
{
	public static AVProQuickTimeMovie currentVideoPlaying;
	public static bool _videoLooping;
	public static bool _isThereAVideo;

	public GameObject videoToClone01;
	public GameObject videoToClone02;

	public AVProQuickTimeMovie video01;
	public AVProQuickTimeMovie video02;

	public GameObject currentVideo;
	public bool _loopingBool01;
	public bool _loopingBool02;

	private bool _toggleSet;

	private bool _videoPlaying;

	//public GameObject _controlPanel;

	public void Start()
	{
		_isThereAVideo = false;
		_videoLooping = false;
		currentVideo = null;
		_videoPlaying = true;
		_toggleSet = false;
	}

	public void Update()
	{

	}


	public void CloneVideo01()
	{
		if(_isThereAVideo == false)
		{
			currentVideo = Instantiate(videoToClone01, new Vector3(-776.76f,79.04f,291f), Quaternion.Euler(0,0,0)) as GameObject;

			_isThereAVideo = true;

	
		}
	}

	public void CloneVideo02()
	{
		if(_isThereAVideo == false)
		{
			currentVideo = Instantiate(videoToClone02, new Vector3(-776.76f,79.04f,291f), Quaternion.Euler(0,0,0)) as GameObject;
			_isThereAVideo = true;
		}
	}

	/*public void LoopVideo01()
	{

		Debug.Log ("True Toggle is " + GameObject.Find("Toggle").GetComponent<Toggle>().isOn);
		Debug.Log("Toggle is " +_loopingBool01);
		Debug.Log("Looping Video 01");
		Debug.Log ("Loop is " + video01.GetComponent<AVProQuickTimeMovie>()._loop);

		_loopingBool01 = GameObject.Find("Toggle").GetComponent<Toggle>().isOn;
		if(_loopingBool01 == true)
		{
			video01.GetComponent<AVProQuickTimeMovie>()._loop = true;

		}
		else
		{
			video01.GetComponent<AVProQuickTimeMovie>()._loop = false;
			Debug.Log ("False false false");
		}

	}



	public void LoopVideo02()
	{

	}*/






}
