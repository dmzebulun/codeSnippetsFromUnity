﻿using UnityEngine;
using System.Collections;

public class FactSheetDestroy : MonoBehaviour 
{

	public void Update()
	{
		if(Camera.main.transform.position.x <= -500f)
		{
			Shrink();
		}
	}
	public void Shrink()
	{
		LeanTween.scale(this.gameObject, new Vector3(0,0,0), .5f).setEase(LeanTweenType.easeInOutExpo).setOnComplete(DestoryObject);
	}

	public void DestoryObject()
	{
		Destroy(this.gameObject);
		FactSheetClone.CloneAlive = false;

	}
}
