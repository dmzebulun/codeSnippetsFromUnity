﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ContentSlider : MonoBehaviour 
{
	public Slider _globalSlider;
	public GameObject _content;

	// Use this for initialization
	void Start () 
	{


	
	}
	
	// Update is called once per frame
	void Update () 
	{
		_content.transform.position = new Vector3(_globalSlider.value, _content.transform.position.y, _content.transform.position.z);
	}
}
