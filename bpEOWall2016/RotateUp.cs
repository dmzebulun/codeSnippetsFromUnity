﻿using UnityEngine;
using System.Collections;

public class RotateUp : MonoBehaviour 
{

	public float Speed;
	

	// Update is called once per frame
	void Update () 
	{
		//Debug.Log (transform.rotation);
		transform.Rotate(Vector3.up, Time.deltaTime*Speed);

	}

}
