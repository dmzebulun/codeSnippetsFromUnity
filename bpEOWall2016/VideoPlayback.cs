﻿using UnityEngine;
using System.Collections;

public class VideoPlayback : MonoBehaviour 
{
	private Animator mainAnim;
	public AVProWindowsMediaMovie movieFile;
	public static bool videoIsPlaying;

	public void Start()
	{
		videoIsPlaying = false;
	}
	public void VideoPlaybackManager()
	{
		if(videoIsPlaying == false)
		{
			movieFile.MovieInstance.Play();
			videoIsPlaying = true;
		}
		else
		{
			movieFile.MovieInstance.Pause();
			videoIsPlaying = false;
		}

	}

}
