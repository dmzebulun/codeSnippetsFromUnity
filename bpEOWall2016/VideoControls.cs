﻿using UnityEngine;
using System.Collections;

public class VideoControls : MonoBehaviour 
{
	public Animator _controlPanelAnim;
	public GameObject _controlPanel;
	public bool _menuOut;

	// Use this for initialization
	void Start()
	{
		StartControlPanel();
		_menuOut = true;
	}


	public void StartControlPanel() 
	{
		Debug.Log ("Starting Control Panel");
		_controlPanelAnim.SetTrigger("MoveOut");
		_menuOut = false;
		//LeanTween.move(_controlPanel, new Vector3(70f,54f,.5f), 5f).setDelay(8f).setEase(LeanTweenType.easeInOutSine);

	}

	public void ResetControlPanel()
	{
		Debug.Log ("Resetting Control Panel");
		_controlPanelAnim.Play("VideoControlMoveOut", -1, 0f);
		//LeanTween.move(_controlPanel, new Vector3(80f,54f,.5f), 5f).setDelay(8f).setEase(LeanTweenType.easeInOutSine);
		//StartControlPanel();

	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
