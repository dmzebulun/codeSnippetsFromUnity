﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class VideoClone : MonoBehaviour 
{
	public static AVProWindowsMediaMovie currentVideoPlaying;
	public static bool _videoLooping;
	public static bool _isThereAVideo;

	public GameObject videoToClone01;
	public GameObject videoToClone02;
    public GameObject videoToClone03;
    public GameObject videoToClone04;
    public GameObject videoToClone05;
    public GameObject videoToClone06;
    public GameObject videoToClone07;
    public GameObject videoToClone08;
    public GameObject videoToClone09;
    public GameObject videoToClone10;
    public GameObject videoToClone11;
    public GameObject videoToClone12;
    public GameObject videoToClone13;

    public AVProWindowsMediaMovie video01; // Only Need these for looping.
	//public AVProWindowsMediaMovie video02;

	public GameObject currentVideo;
	public bool _loopingBool01;
	public bool _loopingBool02;

	private bool _toggleSet;

	private bool _videoPlaying;

	//public GameObject _controlPanel;

	public void Start()
	{
		_isThereAVideo = false;
		_videoLooping = false;
		currentVideo = null;
		_videoPlaying = true;
		_toggleSet = false;
	}

	public void Update()
	{

	}


	public void CloneVideo01()
	{
		if(_isThereAVideo == false)
		{
			currentVideo = Instantiate(videoToClone01, new Vector3(-776.76f,79.04f,291f), Quaternion.Euler(0,0,0)) as GameObject;

			_isThereAVideo = true;

	
		}
	}

	public void CloneVideo02()
	{
		if(_isThereAVideo == false)
		{
			currentVideo = Instantiate(videoToClone02, new Vector3(-776.76f,79.04f,291f), Quaternion.Euler(0,0,0)) as GameObject;
			_isThereAVideo = true;
		}
	}

    public void CloneVideo03()
    {
        if (_isThereAVideo == false)
        {
            currentVideo = Instantiate(videoToClone03, new Vector3(-776.76f, 79.04f, 291f), Quaternion.Euler(0, 0, 0)) as GameObject;
            _isThereAVideo = true;
        }
    }

    public void CloneVideo04()
    {
        if (_isThereAVideo == false)
        {
            currentVideo = Instantiate(videoToClone04, new Vector3(-776.76f, 79.04f, 291f), Quaternion.Euler(0, 0, 0)) as GameObject;
            _isThereAVideo = true;
        }
    }

    public void CloneVideo05()
    {
        if (_isThereAVideo == false)
        {
            currentVideo = Instantiate(videoToClone05, new Vector3(-776.76f, 79.04f, 291f), Quaternion.Euler(0, 0, 0)) as GameObject;
            _isThereAVideo = true;
        }
    }

    public void CloneVideo06()
    {
        if (_isThereAVideo == false)
        {
            currentVideo = Instantiate(videoToClone06, new Vector3(-776.76f, 79.04f, 291f), Quaternion.Euler(0, 0, 0)) as GameObject;
            _isThereAVideo = true;
        }
    }

    public void CloneVideo07()
    {
        if (_isThereAVideo == false)
        {
            currentVideo = Instantiate(videoToClone07, new Vector3(-776.76f, 79.04f, 291f), Quaternion.Euler(0, 0, 0)) as GameObject;
            _isThereAVideo = true;
        }
    }

    public void CloneVideo08()
    {
        if (_isThereAVideo == false)
        {
            currentVideo = Instantiate(videoToClone08, new Vector3(-776.76f, 79.04f, 291f), Quaternion.Euler(0, 0, 0)) as GameObject;
            _isThereAVideo = true;
        }
    }

    public void CloneVideo09()
    {
        if (_isThereAVideo == false)
        {
            currentVideo = Instantiate(videoToClone09, new Vector3(-776.76f, 79.04f, 291f), Quaternion.Euler(0, 0, 0)) as GameObject;
            _isThereAVideo = true;
        }
    }

    public void CloneVideo10()
    {
        if (_isThereAVideo == false)
        {
            currentVideo = Instantiate(videoToClone10, new Vector3(-776.76f, 79.04f, 291f), Quaternion.Euler(0, 0, 0)) as GameObject;
            _isThereAVideo = true;
        }
    }

    public void CloneVideo11()
    {
        if (_isThereAVideo == false)
        {
            currentVideo = Instantiate(videoToClone11, new Vector3(-776.76f, 79.04f, 291f), Quaternion.Euler(0, 0, 0)) as GameObject;
            _isThereAVideo = true;
        }
    }

    public void CloneVideo12()
    {
        if (_isThereAVideo == false)
        {
            currentVideo = Instantiate(videoToClone12, new Vector3(-776.76f, 79.04f, 291f), Quaternion.Euler(0, 0, 0)) as GameObject;
            _isThereAVideo = true;
        }
    }

    public void CloneVideo13()
    {
        if (_isThereAVideo == false)
        {
            currentVideo = Instantiate(videoToClone13, new Vector3(-776.76f, 79.04f, 291f), Quaternion.Euler(0, 0, 0)) as GameObject;
            _isThereAVideo = true;
        }
    }

 

    public void LoopVideo01()
	{

		Debug.Log ("True Toggle is " + GameObject.Find("Toggle").GetComponent<Toggle>().isOn);
		Debug.Log("Toggle is " +_loopingBool01);
		Debug.Log("Looping Video 01");
		Debug.Log ("Loop is " + video01.GetComponent<AVProWindowsMediaMovie>()._loop);

		_loopingBool01 = GameObject.Find("Toggle").GetComponent<Toggle>().isOn;
		if(_loopingBool01 == true)
		{
			video01.GetComponent<AVProWindowsMediaMovie>()._loop = true;

		}
		else
		{
			video01.GetComponent<AVProWindowsMediaMovie>()._loop = false;
			Debug.Log ("False false false");
		}

	}



	public void LoopVideo02()
	{

	}






}
