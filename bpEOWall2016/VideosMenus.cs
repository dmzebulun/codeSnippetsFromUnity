﻿using UnityEngine;
using System.Collections;

public class VideosMenus : MonoBehaviour 
{
	public GameObject videoMenuObject;
	
    public GameObject videoMenuButton01;
    public GameObject videoMenuButton02;
    public GameObject videoMenuButton03;
    public GameObject videoMenuButton04;

    public float videoMenuRotation01;
    public float videoMenuRotation02;
    public float videoMenuRotation03;
    public float videoMenuRotation04;

    // Use this for initialization
    void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void MenuSpin(int choice)
	{	
		if(choice == 1)
		{
			LeanTween.rotateLocal(videoMenuObject, new Vector3(0,videoMenuRotation01,0), 1f).setEase(LeanTweenType.easeInOutSine);
		}
        else if (choice == 2)
        {
            LeanTween.rotateLocal(videoMenuObject, new Vector3(0, videoMenuRotation02, 0), 1f).setEase(LeanTweenType.easeInOutSine);
        }
        else if (choice == 3)
        {
            LeanTween.rotateLocal(videoMenuObject, new Vector3(0, videoMenuRotation03, 0), 1f).setEase(LeanTweenType.easeInOutSine);
        }
        else if (choice == 4)
		{
			LeanTween.rotateLocal(videoMenuObject, new Vector3(0, videoMenuRotation04,0), 1f).setEase(LeanTweenType.easeInOutSine);
		}
	}
}
