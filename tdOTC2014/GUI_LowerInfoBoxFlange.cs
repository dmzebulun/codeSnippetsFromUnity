﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class GUI_LowerInfoBoxFlange : TouchObject 
{
	public static bool boxIsSized;
	public static TextAsset currentText;

	private LTRect infoBox;
	private static LTRect infoBoxText;
	//private TextAsset currentText;

	public TextAsset textContent01;
	/*public TextAsset textContent02;
	public TextAsset textContent03;
	public TextAsset textContent04;
	public TextAsset textContent05;
	public TextAsset textContent06;
	*/


	public TextAsset emptyText;

	public GUISkin customSkin;

	private float w;
	private float h;

	void Awake()
	{

		boxIsSized = false;
		currentText = emptyText;
		w = Screen.width;
		h = Screen.height;


		infoBox = new LTRect(0.40f*w, 0.75f*h, 810, 120);
		infoBoxText = new LTRect (0.40f*w, 0.75f*h, 810, 120);

		GuiStartState();

	}

	void Update()
	{
		transform.LookAt(Camera.main.transform);
		GuiFadeCheck();
	}


	void OnGUI()
	{

		GUI.skin = customSkin;

		GUI.Box (infoBox.rect, emptyText.text);
		GUI.Box(infoBoxText.rect, currentText.text, "transbox");



	}

	public IEnumerator Tap(GestureEvent gEvent)
	{

		LeanTween.alpha (infoBoxText, 0.0f, .5f);
		yield return new WaitForSeconds(.5f);
		currentText = emptyText;
		if(boxIsSized == false)
		{
		LeanTween.scale(infoBox, new Vector2(infoBox.rect.width, infoBox.rect.height)* 10000f, 0.5f).setEase(LeanTweenType.easeInOutQuad);
		}	

		boxIsSized = true;
		currentText = textContent01;

		yield return new WaitForSeconds(.2f);
		LeanTween.alpha (infoBoxText, 1.0f, .5f);

	}

	void GuiStartState()
	{

		LeanTween.alpha (infoBoxText, 0f, 0.01f);
		LeanTween.scale(infoBox, new Vector2(infoBox.rect.width, infoBox.rect.height)* 0.0001f, 0.01f).setEase(LeanTweenType.easeInOutQuad);
	}

	void GuiFadeCheck()
	{
		if(GUI_BottomNav.sceneIsSwitchingGuiFade == true)
		{
			GuiFadeout();
		}
	}
	
	void GuiFadeout()
	{
		LeanTween.alpha (infoBoxText, 0f, 0.5f);
		LeanTween.alpha (infoBox, 0f, 0.5f);
	}



}
