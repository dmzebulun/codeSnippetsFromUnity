﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class GestureAnimationScriptConditional : TouchObject
{
	public static bool hasGesture;
	public float smooth = 2F;

	public Vector3 startPosition;
	public Vector3 endPosition;
	public Vector3 destination;

	//destination = new Vector3(-529.84f, -60.5f, -75.4f);
	//destination = new Vector3(-529.84f, -40.5f, -75.4f);
	
	// Use this for initialization
	// Use this for initialization

	void Start () 
	{
		hasGesture = true;
		
		
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (hasGesture == true)
		{
			destination = endPosition;
		}
		else
		{
			destination = startPosition;	
		}
		
		positionChangingGesture();
	}
	
	void positionChangingGesture()
	{
		transform.position = Vector3.Lerp(transform.position, destination, Time.deltaTime * smooth);
	}
	
}
