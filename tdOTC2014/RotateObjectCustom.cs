using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class RotateObjectCustom : TouchObject 
{
	private float resetTimer = 20.0f;
	private bool _isSpinning;
	private int rotateSpeed = 5;

	private float currentRotation;
	private float alteredRotation;

	private float tRotation;

	private float dragX;

	public float spinCenter = 1200;

	void Awake()
	{
		_isSpinning = true;
	}



	void Update()
	{
		if(_isSpinning == true)
		{
			transform.Rotate(Vector3.up * (Time.deltaTime * rotateSpeed));
		}

		if(GestureWorksUnity.Instance.TimeSinceLastEvent >= resetTimer)
		{
			_isSpinning = true;
		}

		currentRotation = transform.rotation.y;

		alteredRotation = currentRotation + dragX;

	

	 }

	




	public void NDrag(GestureEvent gEvent)
	{
		_isSpinning = false;

		if (gEvent.X <= spinCenter)
		{
			dragX = gEvent.X * 0.003f;
		}

		if (gEvent.X >= (spinCenter + 1))
		{
			dragX = gEvent.X * -0.003f;
		}



		transform.Rotate (0, alteredRotation, 0);
	}

}
