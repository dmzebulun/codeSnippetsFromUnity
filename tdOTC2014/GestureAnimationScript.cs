﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class GestureAnimationScript : TouchObject
{

	public float smooth = 2F;
	private Vector3 destination;

	// Use this for initialization
	void Start () 
	{
		destination = new Vector3(-529.84f, -60.5f, -75.4f);
	
	}
	
	// Update is called once per frame
	void Update () 
	{

		positionChangingGesture();
	
	}

	void positionChangingGesture()
	{
		transform.position = Vector3.Lerp(transform.position, destination, Time.deltaTime * smooth);
	}
}
