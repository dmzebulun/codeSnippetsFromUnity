﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class SubseaVehicleModelMove : TouchObject
{
	
	public static Vector3 positionHybridModel;
	//public static Quaternion rotationCam;

	public float smooth = 2F;
	public float speed = .01F;
	
	void Start ()
	{
		//positionCam = new Vector3 (-524, -62, -77);
		//rotationCam = Quaternion.Euler(358,249,0);

		positionHybridModel = new Vector3(-521.7901f, -63.15669f, -75.21426f);
		//rotationCam = GUI_DefaultMenuSelection.defaultCamRotation;
	}

	void Update (){
		PositionChanging();
		//RotationChange ();
	}

	public void PositionChanging (){
		transform.position = Vector3.Lerp (transform.position, positionHybridModel, Time.deltaTime * smooth);
		//Debug.Log (positionCam);

		}
	/*public void RotationChange (){
		Camera.main.transform.rotation= Quaternion.Lerp(Camera.main.transform.rotation, rotationCam, Time.time * speed); 
		//Debug.Log (rotationCam);
		}*/

}
