﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class ScreenFadeIn_Clean : TouchObject
{
	//public static bool sceneStarting = true;
	//private bool sceneEnding = false;
	
	public float fadeSpeed = 1.5f;
	
	
	public void Start()
	{
		/*Camera.main.transform.position = GUI_DefaultMenuSelection.defaultCamPosition;
		Camera.main.transform.rotation = GUI_DefaultMenuSelection.defaultCamRotation;*/

		
		GUI_DefaultMenuSelection.currentScene = Application.loadedLevelName;

		guiTexture.pixelInset = new Rect(0f,0f, Screen.width, Screen.height);
		guiTexture.color = Color.black;
		
		ScreenFadeIn.sceneStarting = true;
		ScreenFadeOut.sceneEnding = false;
	}
	public void Update()
	{
		if(ScreenFadeIn.sceneStarting == true)
		{
			StartCoroutine(FadeToClear());
		}
		
		/*		if(sceneEnding == true)
		{
			StartCoroutine(FadeToBlack());
		}
		*/
		//Debug.Log ("Alpha :" + guiTexture.color.a);
	}
	//Screen Fade scripting
	
	
	
	public IEnumerator FadeToClear()
	{
		
		guiTexture.color = Color.Lerp(guiTexture.color, Color.clear, fadeSpeed * Time.deltaTime);
		
		if(guiTexture.color.a <= 0.05f)
		{
			guiTexture.color = Color.clear;
			guiTexture.enabled = false;
			ScreenFadeIn.sceneStarting = false;
		}
		
		yield return null;
	}
	
	/*public IEnumerator FadeToBlack()
	{
		yield return new WaitForSeconds(1f);
		Debug.Log ("FadeToBlack");
		guiTexture.enabled = true;
		guiTexture.color = Color.Lerp(guiTexture.color, new Color(0,0,0,1), fadeSpeed * Time.deltaTime);
		
		if(guiTexture.color.a >=.9f)
		{
			guiTexture.color = Color.black;
		}
		
		
	}*/
}
