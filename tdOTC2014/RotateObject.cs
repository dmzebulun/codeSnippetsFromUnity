using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class RotateObject : TouchObject 
{
	private float resetTimer = 20.0f;
	private bool _isSpinning;
	private int rotateSpeed = 5;

	private float currentRotation;
	private float alteredRotation;

	private float tRotation;

	private float dragX;



	void Awake()
	{
		_isSpinning = true;
	}



	void Update()
	{
		if(_isSpinning == true)
		{
			transform.Rotate(Vector3.up * (Time.deltaTime * rotateSpeed));
		}

		if(GestureWorksUnity.Instance.TimeSinceLastEvent >= resetTimer)
		{
			_isSpinning = true;
		}

		currentRotation = transform.rotation.y;

		alteredRotation = currentRotation + dragX;

	

	 }

	




	public void NDrag(GestureEvent gEvent)
	{
		_isSpinning = false;

		if (gEvent.X <= 1200)
		{
			dragX = gEvent.X * 0.003f;
		}

		if (gEvent.X >= 1201)
		{
			dragX = gEvent.X * -0.003f;
		}



		transform.Rotate (0, alteredRotation, 0);
	}

}
