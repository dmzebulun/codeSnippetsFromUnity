﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

[ExecuteInEditMode]
public class GUI_LowerInfoBoxWellMate : TouchObject 
{
	public static bool boxIsSized;
	public static TextAsset currentText;
	public static Texture currentImage;
	public static bool _SpecialGuiFade2;

	private LTRect infoBox;
	private static LTRect infoBoxText;

	private LTRect imageBox;
	//private TextAsset currentText;

	public TextAsset textContent;
	/*public TextAsset textContent02;
	public TextAsset textContent03;
	public TextAsset textContent04;
	public TextAsset textContent05;
	public TextAsset textContent06;
	*/
	
	public Texture imageContent;


	private float scrollLock;
	public TextAsset emptyText;

	public GUISkin customSkin;

	private float w;
	private float h;

	public Vector2 scrollPosition = Vector2.zero;
	public Vector2 scrollNDrag;
	public float dragMultiplier = 0.02f;
	public float imageXPosition = 10f;
	public float imageXPositionSet;
	
	//private bool _isDragging;
	private float dragY;
	private float dragYread;

	void Awake()
	{

		boxIsSized = false;
		_SpecialGuiFade2 = false;
		currentText = emptyText;

		w = Screen.width;
		h = Screen.height;

		scrollLock = 300;



		infoBox = new LTRect(0.460f*w, 0.74f*h, 580, 150);
		infoBoxText = new LTRect (0.46f*w, 0.74f*h, 580, 150);

		imageBox = new LTRect(0.7f*w, .6f*h, 300, 300);

		GuiStartState();

	}

	void Update()
	{

		transform.LookAt(Camera.main.transform);

		if(_SpecialGuiFade2 == true)
		{
			StartCoroutine(GuiFadeOut());
		}

	}


	void OnGUI()
	{

		GUI.skin = customSkin;

		GUI.Box (infoBox.rect, emptyText.text);

		GUI.Box(infoBoxText.rect, currentText.text, "transbox");
		
		//GUI.DrawTexture(imageBox.rect, currentImage, ScaleMode.ScaleToFit);
		


	/*	scrollPosition = GUI.BeginScrollView(infoBox.rect , GUI_LowerInfoBoxCorrosionTapOnly.scrollNDragSpecial + scrollPosition, new Rect(.3f*w, 0.6f*h, 900, 900));



		
		GUI.EndScrollView();*/


	}

	/*void NDrag (GestureEvent gEvent)
	{
		dragY = gEvent.Y;
		
		if (dragY < 1)
		{
			dragYread = 0.0f;
		}
		
		if(dragYread == 0.0f)
		{
			dragYread = gEvent.Y;
		}
		
		if(dragYread >= dragY)
		{
			scrollNDrag = new Vector2(0, (dragY * dragMultiplier));
		}
		else
		{
			scrollNDrag = new Vector2(0, (dragY * dragMultiplier * -1f));
		}
	}*/

	public IEnumerator Tap(GestureEvent gEvent)
	{


		LeanTween.alpha (infoBoxText, 0.0f, .5f);
		//LeanTween.alpha (imageBox, 0.0f, .5f);
		yield return new WaitForSeconds(.5f);
		currentText = emptyText;
		if(boxIsSized == false)
		{
		LeanTween.scale(infoBox, new Vector2(infoBox.rect.width, infoBox.rect.height)* 10000f, 0.5f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.scale(imageBox, new Vector2(imageBox.rect.width, imageBox.rect.height)* 10000f, 0.5f).setEase(LeanTweenType.easeInOutQuad);
		}	

		boxIsSized = true;
		currentText = textContent;
		currentImage = imageContent;

		yield return new WaitForSeconds(.2f);
		LeanTween.alpha (infoBoxText, 1.0f, .5f);

	}

	IEnumerator GuiFadeOut()
	{
		LeanTween.alpha (infoBoxText, 0.0f, .5f);
		LeanTween.alpha (imageBox, 0.0f, .5f);
		LeanTween.alpha (infoBox, 0.0f, .5f);
	
		yield return new WaitForSeconds(.5f);
		_SpecialGuiFade2 = false;

	}

	void GuiStartState()
	{

		LeanTween.alpha (infoBoxText, 0f, 0.01f);
		LeanTween.scale(infoBox, new Vector2(infoBox.rect.width, infoBox.rect.height)* 0.0001f, 0.01f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.scale(imageBox, new Vector2(imageBox.rect.width, imageBox.rect.height)* 0.0001f, 0.01f).setEase(LeanTweenType.easeInOutQuad);
	}



}
