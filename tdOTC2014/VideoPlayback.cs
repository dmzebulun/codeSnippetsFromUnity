﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;


[RequireComponent(typeof (AudioSource))]

public class VideoPlayback: TouchObject
{

	public GestureWorksScript gestureWorks;
	public MovieTexture movTexture;
	public string sceneToSwitchTo;
	//public GameObject imageTransitionPlaneBlack;
	private bool movieIsFinished;

	// Use this for initialization
	void Awake()
	{


	}
	void Start () 
	{
		renderer.material.mainTexture = movTexture;
		audio.clip = movTexture.audioClip;
		//renderer.material.mainTexture = Resources.Load("RPCM-Apple ProRes 422 (LT).mov", typeof(Texture2D));
		VideoDelay();


		//imageTransitionPlaneBlack.GetComponent<ImageTransBlack>().ImageTransStartStateForMenu();
		//GUI_BottomNav.returnedFromVideo = false;
		//movieIsFinished = false;
		//StartCoroutine(VideoEnd());
	}
	
	void Update()
	{
		Debug.Log (movTexture.isReadyToPlay);
		if(movTexture.isPlaying == false)
		{
			//sGUI_BottomNav.returnedFromVideo = true;
			//imageTransitionPlaneBlack.GetComponent<ImageTransBlack>().ImageTransMoveIn();
			gestureWorks.SwitchScenes(PlayVideoButton.returnScene);
		}

		Debug.Log (PlayVideoButton.returnScene);
	}
	void DoubleTap (GestureEvent gEvent)
	{
		//sGUI_BottomNav.returnedFromVideo = true;
		//imageTransitionPlaneBlack.GetComponent<ImageTransBlack>().ImageTransMoveIn();
		gestureWorks.SwitchScenes(PlayVideoButton.returnScene);
		//Debug.Log ("Tapped");
		//movTexture.Stop();
		//imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
		//GUI_BottomNav.returnedFromVideo = true;
		//gestureWorks.SwitchScenes(GUI_BottomNav.returnScene);
	}

	public void VideoDelay()
	{
	if(movTexture.isReadyToPlay == true)
		{
			movTexture.Play();
			audio.Play();
		}
	}


	/*public IEnumerator VideoEnd()
	{
		Debug.Log ("Video End is Running");
		Debug.Log (movTexture.isPlaying);
		//yield return null;
		if(movieIsFinished = true)
		{

			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			
			yield return new WaitForSeconds(.5f);
			
			gestureWorks.SwitchScenes(GUI_BottomNav.returnScene);
			//gestureWorks.SwitchScenes(GUI_BottomNavNoMovement.returnScene02);
		}

	}*/
}
