﻿using UnityEngine;
using System.Collections;

public class VideoPlaybackBasic : MonoBehaviour 
{
	//public GestureWorksScript gestureWorks;
	public MovieTexture movTexture;
	public GameObject videoPlane;
	//public string sceneToSwitchTo;
	//public GameObject imageTransitionPlaneBlack;
	//private bool movieIsFinished;
	
	// Use this for initialization
	
	void Start () 
	{
		
		
		renderer.material.mainTexture = movTexture as MovieTexture;
		//audio.clip = movTexture.audioClip;
		//imageTransitionPlaneBlack.GetComponent<ImageTransBlack>().ImageTransStartStateForMenu();

		VideosStartStateForMenu();

	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public IEnumerator VideoDelay()
	{
		yield return new WaitForSeconds(1f);
		movTexture.Play();
	}

	public void VideosStartStateForMenu()
	{
		Debug.Log("Transtion Set for Menu");
		LeanTween.scaleX(videoPlane, 0f, .01f).setEase(LeanTweenType.easeInOutExpo);
		LeanTween.scaleZ(videoPlane, 0f, .01f).setEase(LeanTweenType.easeInOutExpo);
	}
	
	public void VideoStartStateRegular()
	{
		Debug.Log("Transtion Set for Regular Scene");
		LeanTween.moveLocalZ(videoPlane, 1f, .01f).setEase(LeanTweenType.easeInOutExpo);
		LeanTween.alpha(videoPlane, 1.0f, .01f).setEase(LeanTweenType.easeInOutQuad);
	}
	
	public void VideoPlay()
	{

		Debug.Log("Transtion Moving In");
		/*LeanTween.moveLocalZ(videoPlane, 1f, 1f).setEase(LeanTweenType.easeInOutExpo);
		LeanTween.alpha(videoPlane, 1.0f, 1f).setEase(LeanTweenType.easeInOutQuad);*/
		LeanTween.scaleX(videoPlane, .33f, 1f).setEase(LeanTweenType.easeInOutExpo);
		LeanTween.scaleZ(videoPlane, .19f, 1f).setEase(LeanTweenType.easeInOutExpo);
		StartCoroutine(VideoDelay());
	}
	
	public void VideoEnd()
	{
		movTexture.Stop();
		LeanTween.scaleX(videoPlane, 0f, .5f).setEase(LeanTweenType.easeInOutExpo);
		LeanTween.scaleZ(videoPlane, 0f, .5f).setEase(LeanTweenType.easeInOutExpo);
	}
}
