﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class PlayVideoButtonNoMovement : TouchObject 
{
	public GestureWorksScript gestureWorks;
	//private Vector3 startPosition = new Vector3(-530, -40, -75);
	private Vector3 endPosition;
	public static string returnScene02;
	//public static bool returnedFromVideo;
	public string videoScene;
	public GameObject imageTransitionPlane;
	
	public float smooth = 2F;
	public float speed = .005F;
	
	void Start () 
	{
		returnScene02 = Application.loadedLevelName;
		//endPosition = new Vector3(-530, -60, -75);
	}
	
	void Update()
	{
		//positionChanging();
	}
	
	public void Tap(GestureEvent gEvent)
	{
		//GUI_BottomNav.sceneIsSwitchingGuiFade = true;
		//GUI_BottomNav.gui_BottomNavFade = true;
		//imageTransitionPlane.GetComponent<ImageTransBlack>().ImageTransMoveIn();
		
		StartCoroutine(PlayVideo());
		
		
	}
	
	IEnumerator PlayVideo()
	{
		yield return new WaitForSeconds(1.5f);
		//Debug.Log("Tapped");
		//GUI_BottomNav.returnedFromVideo = true;
		gestureWorks.SwitchScenes(videoScene);
	}
	
	public void positionChanging()
	{
		transform.position = Vector3.Lerp (transform.position, endPosition, Time.deltaTime * smooth);
	}
	
}
