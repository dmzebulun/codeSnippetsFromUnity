﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class AboutTouch : TouchObject 
{
	public void Tap(GestureEvent gEvent)
	{
		//AudioController.Play("About_1-2");

		CamMove.positionCam = new Vector3(-554, -62, -46);
		CamMove.rotationCam = Quaternion.Euler(360,265,0);
	}
}