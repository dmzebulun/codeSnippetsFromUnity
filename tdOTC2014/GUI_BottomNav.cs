﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;


[ExecuteInEditMode]
public class GUI_BottomNav : TouchObject
{
	public bool hasSecondButton = false;
	public static bool sceneIsSwitchingGuiFade;
	public static bool gui_BottomNavFade;
	public GestureWorksScript gestureWorksScript;
	public static bool returnedFromVideo;
	public string category;
	public string category02;
	public string categoryPosition;
	public string categoryDisplayName;
	//public string subCategory;
	public string subCatDisplayName;
	public string subCatDisplayName02;


	public float xOffsetCat;
	public float xOffsetSubCat;
	public float xOffsetSubCat02;

	private LTRect catButton;
	private LTRect subCatButton;
	private LTRect mainMenuButton;
	private LTRect subCatButton02;

	public Texture menuTex;

	private float w;
	private float h;

	public GameObject imageTransitionPlane;
	//public GameObject imageTransitionPlaneBlack;

	public GUISkin customSkin;

	void Awake()
	{
		sceneIsSwitchingGuiFade = false;
		gui_BottomNavFade = false;

		w = Screen.width;
		h = Screen.height;

		mainMenuButton = new LTRect(.005f*w, .89f*h, 540, 200);
		catButton = new LTRect(.2f*w + xOffsetCat, .93f*h, 400, 200);
		subCatButton = new LTRect(.3f*w + xOffsetSubCat, .93f*h, 400, 200);
		subCatButton02 = new LTRect(.3f*w + xOffsetSubCat02, .93f*h, 50, 50);
		GuiStartState();

	}

	void Start()
	{
		GuiFadeIn();

		StartCoroutine(ImageTransitionDelay());
		//Debug.Log(categoryPosition);

	/*	if(Application.loadedLevelName == "Teledyne_Menu")
		{
			StartCoroutine(ImageTransitionDelay());
		}*/

		/*if(returnedFromVideo == true)
		{
			StartCoroutine(ImageTransitionBlackDelay());
		}
		else
		{

			imageTransitionPlaneBlack.GetComponent<ImageTransBlack>().ImageTransStartStateForMenu();
		}	*/
	}

	public IEnumerator ImageTransitionDelay()
	{

		Debug.Log ("Coroutine Called for Image Transition");
		yield return new WaitForSeconds(.3f);
		imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveOut();
		if(ImageTrans.transitionHappened == false)
		{
			yield return new WaitForSeconds(2f);
			imageTransitionPlane.GetComponent<ImageTrans>().EmergencyMove();
		}

	}

	/*public IEnumerator ImageTransitionBlackDelay()
	{
		imageTransitionPlane.GetComponent<ImageTrans>().ImageTransStartStateForMenu();
		Debug.Log ("Coroutine Called for Image Transition Black");
		returnedFromVideo = false;
		yield return new WaitForSeconds(.2f);
		imageTransitionPlaneBlack.GetComponent<ImageTransBlack>().ImageTransMoveOut();
	}*/

	void Update()
	{
		if(gui_BottomNavFade == true)
		{
			GuiFadeout();
		}

	}

	void OnGUI()
	{
		GUI.skin = customSkin;
		if(GUI.Button(mainMenuButton.rect, menuTex))
		{


			if(Application.loadedLevel == 1)
			{ 	
				AudioController.StopAll(1);
				CamMove.positionCam = new Vector3 (-524, -62, -77);
				CamMove.rotationCam = Quaternion.Euler(358,249,0);
			}
			else
			{

				CamMove.positionCam = new Vector3 (-524, -62, -77);
				CamMove.rotationCam = Quaternion.Euler(358,249,0);
				StartCoroutine(SwitchToMainMenu());
			}
		}

		if(GUI.Button(catButton.rect, categoryDisplayName))
		{
			StartCoroutine(SwitchingScenes());
		}

		if(GUI.Button(subCatButton.rect, subCatDisplayName))
		{
			if( hasSecondButton == true)
			{
				StartCoroutine(SwitchingScenes02());
				//Debug.Log ("It's Working.");
			}			
		}

	
		GUI.Button(subCatButton02.rect, subCatDisplayName02);
	}

	void CamState()
	{
		if(categoryPosition == "Teledyne_Menu")
		{
			CamMove.positionCam = new Vector3 (-524, -62, -77);
			CamMove.rotationCam = Quaternion.Euler(358,249,0);
		}
		if(categoryPosition == "Assets" )
		{
			CamMove.positionCam = new Vector3 (-521, -64, -100);
			CamMove.rotationCam = Quaternion.Euler(358,267,0);
		}
		if(categoryPosition == "Power" )
		{
			CamMove.positionCam = new Vector3 (-508, -62, -97);
			CamMove.rotationCam = Quaternion.Euler(359, 180, 0);
		}
		if(categoryPosition == "Subsea" )
		{
			CamMove.positionCam = new Vector3(-544, -62, -57);
			CamMove.rotationCam = Quaternion.Euler(360, 225, 0);
		}
		if(categoryPosition == "About" )
		{
			CamMove.positionCam = new Vector3(-554, -62, -46);
			CamMove.rotationCam = Quaternion.Euler(360,265,0);
		}



	}

	void GuiStartState()
	{

		LeanTween.alpha(mainMenuButton, 0f, 0.001f);
		LeanTween.alpha(catButton, 0f, 0.001f);
		LeanTween.alpha(subCatButton, 0f, 0.001f);
		LeanTween.alpha(subCatButton02, 0f, 0.001f);
	}
	
	
	void GuiFadeIn()
	{

		LeanTween.alpha(mainMenuButton, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(catButton , 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(subCatButton, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(subCatButton02, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
	}
	
	
	void GuiFadeout()
	{
		LeanTween.alpha(mainMenuButton, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(catButton , 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(subCatButton, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(subCatButton02, 0f, .5f).setEase(LeanTweenType.easeOutQuad);


	}	

	public IEnumerator SwitchingScenes()
	{

		CamState();
		//Debug.Log (category);
		//Debug.Log ("Switing Scenes");
		GuiFadeout();
		sceneIsSwitchingGuiFade = true;
		imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();

		yield return new WaitForSeconds(1f);
		gestureWorksScript.SwitchScenes(category);
	}

	public IEnumerator SwitchingScenes02()
	{
		
		//CamState();
		//Debug.Log (category);
		//Debug.Log ("Switing Scenes");
		GuiFadeout();
		sceneIsSwitchingGuiFade = true;
		imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
		
		yield return new WaitForSeconds(1f);
		gestureWorksScript.SwitchScenes(category02);
	}
	public IEnumerator SwitchToMainMenu()
	{
		GuiFadeout();
		sceneIsSwitchingGuiFade = true;
		imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();

		yield return new WaitForSeconds(1f);
		gestureWorksScript.SwitchScenes("Teledyne_Menu");
	}


}
