using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class About_DGO_Touch : TouchObject
{
	public GestureWorksScript gestureWorks;
	public GameObject imageTransitionPlane; 
	
	public void Tap(GestureEvent gEvent)
	{
		ScreenFadeOut.sceneEnding = true;
		imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
		AudioController.StopAll(1);
		GUI_BottomNav.gui_BottomNavFade = true;

		StartCoroutine(SwitchScene());
	}

	private IEnumerator SwitchScene()
	{
		//Debug.Log ("Called");
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("21 AboutDGO");
	}
}
