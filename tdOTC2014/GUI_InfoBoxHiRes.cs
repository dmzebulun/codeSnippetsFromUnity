﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

[ExecuteInEditMode]
public class GUI_InfoBoxHiRes : TouchObject
{
	//public GestureWorksScript gestureWorks;

	//Text and Graph Assets
	public string textBoxTitle01;
	public string textBoxTitle02;
	public string textBoxTitle03;
	public string textBoxTitle04;
	public string textBoxTitle05;
	public string textBoxTitle06;

	public TextAsset textSideContent01;
	public TextAsset textSideContent02;
	public TextAsset textSideContent03;
	public TextAsset textSideContent04;
	public TextAsset textSideContent05;
	public TextAsset textSideContent06;
	public TextAsset emptyText;
	
	public Texture graphContent01;
	public Texture graphContent02;
	public Texture graphContent03;
	public Texture graphContent04;
	public Texture graphContent05;
	public Texture graphContent06;

	private TextAsset currentText;
	private Texture currentGraphContent01;
	private Texture currentGraphContent02;
	private Texture currentGraphContent03;
	private Texture currentGraphContent04;
	private Texture currentGraphContent05;
	private Texture currentGraphContent06;

	private float scrollLock;




	//LeanTween Variables

	private float w;
	private float h;

	private LTRect textTitleBox01;
	private LTRect textTitleBox02;
	private LTRect textTitleBox03;
	private LTRect textTitleBox04;
	private LTRect textTitleBox05;
	private LTRect textTitleBox06;
	private LTRect textContainerBox01;

/*	
 * private LTRect textLowerBox01;
	private LTRect textLowerBox02;
	private LTRect textLowerBox03;
	private LTRect textLowerBox04;
	private LTRect textLowerBox05;
	private LTRect textLowerBox06;
*/

	private LTRect graphBox01;
	private LTRect graphBox02;
	private LTRect graphBox03;
	private LTRect graphBox04;
	private LTRect graphBox05;
	private LTRect graphBox06;


	//Custom GUI Skin. This should be set to the InfoBox Skin.
	public GUISkin customSkin;
	private int guiDepth = 1;

	public Vector2 scrollPosition = Vector2.zero;
	public Vector2 scrollNDrag;
	public float dragMultiplier = 0.02f;

	//private bool _isDragging;
	private float dragY;
	private float dragYread;


	void Awake()
	{

		scrollLock = 850;
		w = Screen.width;
		h = Screen.height;



		//This sets the sizes and positions of the menu tabs for the main info box.
		textTitleBox01 = new LTRect(.02f*w, .04f*h, 150, 40);
		textTitleBox02 = new LTRect(.10f*w, .04f*h, 150, 40);
		textTitleBox03 = new LTRect(.18f*w, .04f*h, 120, 40);
		textTitleBox04 = new LTRect(.244f*w, .04f*h, 110, 40);
		textTitleBox05 = new LTRect(.3025f*w, .04f*h, 100, 40);
		textTitleBox06 = new LTRect(.324f*w, .04f*h, 100, 40);
		//textTitleBox04 = new LTRect(.08f*w, .04f*h, 150, 40);

		textContainerBox01 = new LTRect(.02f*w, .078f*h, 640, 850);

		graphBox01 = new LTRect(.022f*w, .6f*h, 635, 635);
		graphBox02 = new LTRect(.022f*w, .6f*h, 635, 635);
		graphBox03 = new LTRect(.022f*w, .6f*h, 635, 635);
		graphBox04 = new LTRect(.022f*w, .08f*h, 635, 1200);
		graphBox05 = new LTRect(.022f*w, .06f*h, 635, 635);
		graphBox06 = new LTRect(.022f*w, .45f*h, 635, 635);



	}

	void Start() //Assign LeanTween variables sizes and position
	{
		currentText = textSideContent01;
		currentGraphContent01 = null;
		currentGraphContent02 = null;
		currentGraphContent03 = null;
		currentGraphContent04 = null;
		currentGraphContent05 = null;
		currentGraphContent06 = null;

		GuiStartState();

		GuiFadeIn();
	
	}

	void Update()
	{
		GuiFadeCheck();
		//Debug.Log ("dragY is " + dragY);
		//Debug.Log ("dragYread is " + dragYread);

	}

	void OnGUI() 
	{
		GUI.skin = customSkin;
		GUI.depth = guiDepth;

		//Main Content Box on Left


		//Menu Buttons 
		if(GUI.Toggle (textTitleBox01.rect, false, textBoxTitle01))
		{
			currentText = textSideContent01;
			currentGraphContent01 = null;
			currentGraphContent02 = null;
			currentGraphContent03 = null;
			currentGraphContent03 = null;
			currentGraphContent05 = null;
			currentGraphContent06 = null;

			scrollPosition = new Vector2(0,0);

			SubSeaHybridModelMove.positionHybridModel = new Vector3(-538.4f, -62.1f, -82f);

			scrollLock = 850;

			
			ScrollAnimationScript.scrollableIcon = false;

			GestureAnimationScriptConditional.hasGesture = false;
		}

		if(GUI.Toggle (textTitleBox02.rect, false, textBoxTitle02))
		{
			currentText = textSideContent02;
			currentGraphContent01 = null;
			currentGraphContent02 = graphContent02;
			currentGraphContent03 = null;
			currentGraphContent04 = null;
			currentGraphContent05 = null;
			currentGraphContent06 = null;

			scrollPosition = new Vector2(0,0);

			SubSeaHybridModelMove.positionHybridModel = new Vector3(-538.4f, -62.1f, -82f);

			scrollLock = 1200;

			
			ScrollAnimationScript.scrollableIcon = false;

			GestureAnimationScriptConditional.hasGesture = false;

		}

		if(GUI.Toggle (textTitleBox03.rect, false, textBoxTitle03))
		{
			currentText = textSideContent03;
			currentGraphContent01 = null;
			currentGraphContent02 = null;
			currentGraphContent03 = graphContent03;
			currentGraphContent04 = null;
			currentGraphContent05 = null;
			currentGraphContent06 = null;

			scrollPosition = new Vector2(0,0);

			SubSeaHybridModelMove.positionHybridModel = new Vector3(-538.4f, -62.1f, -99.7f);

			scrollLock = 1200;
			
			ScrollAnimationScript.scrollableIcon = false;

			GestureAnimationScriptConditional.hasGesture = false;

		}

		if(GUI.Toggle (textTitleBox04.rect, false, textBoxTitle04))
		{
			currentText = textSideContent04;
			currentGraphContent01 = null;
			currentGraphContent02 = null;
			currentGraphContent03 = null;
			currentGraphContent04 = graphContent04;
			currentGraphContent05 = null;
			currentGraphContent06 = null;

			scrollPosition = new Vector2(0,0);

			SubSeaHybridModelMove.positionHybridModel = new Vector3(-538.4f, -62.1f, -111.2f);

			scrollLock = 1250;
			
			ScrollAnimationScript.scrollableIcon = false;

			GestureAnimationScriptConditional.hasGesture = false;
			
		}

		if(GUI.Toggle (textTitleBox05.rect, false, textBoxTitle05))
		{
			currentText = textSideContent05;
			currentGraphContent01 = null;
			currentGraphContent02 = null;
			currentGraphContent03 = null;
			currentGraphContent04 = null;
			currentGraphContent05 = null;
			currentGraphContent06 = null;
			
			scrollPosition = new Vector2(0,0);

			SubSeaHybridModelMove.positionHybridModel = new Vector3(-538.4f, -62.1f, -123.5f);

			scrollLock = 850;
			
			ScrollAnimationScript.scrollableIcon = false;

			GestureAnimationScriptConditional.hasGesture = false;
		}


		GUI.Box (textContainerBox01.rect, " ");


		scrollPosition = GUI.BeginScrollView( textContainerBox01.rect , scrollNDrag + scrollPosition, new Rect(.02f*w, .078f*h, 475, scrollLock));
		GUI.Box (textContainerBox01.rect, currentText.text, "transBox");

		GUI.DrawTexture(graphBox01.rect, currentGraphContent01, ScaleMode.ScaleToFit);
		GUI.DrawTexture(graphBox02.rect, currentGraphContent02, ScaleMode.ScaleToFit);
		GUI.DrawTexture(graphBox03.rect, currentGraphContent03, ScaleMode.ScaleToFit);
		GUI.DrawTexture(graphBox04.rect, currentGraphContent04, ScaleMode.ScaleToFit);
		GUI.DrawTexture(graphBox05.rect, currentGraphContent05, ScaleMode.ScaleToFit);
		GUI.DrawTexture(graphBox06.rect, currentGraphContent06, ScaleMode.ScaleToFit);

		GUI.EndScrollView();
	}

	void NDrag (GestureEvent gEvent)
	{
		dragY = gEvent.Y;

		if (dragY < 1)
		{
			dragYread = 0.0f;
		}
	
		if(dragYread == 0.0f)
		{
			dragYread = gEvent.Y;
		}

		if(dragYread >= dragY)
		{
			scrollNDrag = new Vector2(0, (dragY * dragMultiplier));
		}
		else
		{
			scrollNDrag = new Vector2(0, (dragY * dragMultiplier * -1f));
		}


	
	}

	void GuiFadeCheck()
	{
		if(GUI_BottomNav.sceneIsSwitchingGuiFade == true)
		{
			GuiFadeout();
		}
	}




	void GuiStartState()
	{
		LeanTween.alpha(textTitleBox01, 0f, 0.001f);
		LeanTween.alpha(textTitleBox02, 0f, 0.001f);
		LeanTween.alpha(textTitleBox03, 0f, 0.001f);
		LeanTween.alpha(textTitleBox04, 0f, 0.001f);		
		LeanTween.alpha(textTitleBox05, 0f, 0.001f);
		LeanTween.alpha(textTitleBox06, 0f, 0.001f);
		LeanTween.alpha(textContainerBox01, 0f, 0.001f);
		LeanTween.alpha(graphBox01, 0f, 0.001f);
		LeanTween.alpha(graphBox02, 0f, 0.001f);
		LeanTween.alpha(graphBox03, 0f, 0.001f);
		LeanTween.alpha(graphBox04, 0f, 0.001f);
		LeanTween.alpha(graphBox05, 0f, 0.001f);
		LeanTween.alpha(graphBox06, 0f, 0.001f);
	
	
	}


	void GuiFadeIn()
	{
		LeanTween.alpha(textTitleBox01 , 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textTitleBox02, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textTitleBox03, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textTitleBox04, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textTitleBox05, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textTitleBox06, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textContainerBox01, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox01, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox02, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox03, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox04, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox05, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox06, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);


	}


	public void GuiFadeout()
	{
		LeanTween.alpha(textTitleBox01 , 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textTitleBox02 , 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textTitleBox03 , 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textTitleBox04 , 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textTitleBox05 , 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textTitleBox06 , 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textContainerBox01, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox01, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox02, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox03, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox04, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox05, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox06, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
	}	
}