﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;


[ExecuteInEditMode]
public class ScrollTextExample : TouchObject
{

	public Vector2 scrollPosition;
	public Vector2 scrollPositionAdd;
	public GUIStyle style;

	void OnGUI()
	{
		scrollPosition = GUI.BeginScrollView(new Rect(10,200,100,100), scrollPosition, new Rect(0, 0, 220, 200));
		GUI.Button(new Rect(0,0,100,20), "Top Left");
		GUI.Button(new Rect(0 ,180, 100, 20), "Bottom Left");
		GUI.EndScrollView();
	}
	
	void NDrag(GestureEvent gEvent)
	{
		Debug.Log (gEvent.Values);

		scrollPosition = new Vector2(0, gEvent.Y);


	}
}