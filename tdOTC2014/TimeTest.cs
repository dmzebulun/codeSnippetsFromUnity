﻿using System;

using System.Collections.Generic;

using UnityEngine;



public class TimeTest : MonoBehaviour
	
{
	
	public float timeLeft = 2f;
	
	
	
	public void Update()
		
	{
		
		timeLeft -= Time.deltaTime;
		Debug.Log (Time.deltaTime);
		//Debug.Log (Time.fixedDeltaTime);
		
		
		if (timeLeft <= 0.0f)
			
		{
			
			// End the level here.
			
			guiText.text = "You ran out of time";
			
		}
		
		else
			
		{
			
			guiText.text = "Time left = " + (int)timeLeft + " seconds";
			
		}
		
		
		
	}
	
	
	
}
