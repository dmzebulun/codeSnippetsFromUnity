﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class TimeOut : TouchObject
{
	//public float resetTimer = 10.0f;
	public GestureWorksScript gestureWorks;
	public GameObject imageTransitionPlane;

	public static bool _Timedout;
	public static float globalResetTimer = 180.0f;


	// Use this for initialization
	void Start () 
	{
		_Timedout = false;
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Debug.Log ("Default Scene is " + GUI_DefaultMenuSelection.defaultScene);
		//Debug.Log ("C is " + GUI_DefaultMenuSelection.currentScene);
		//Debug.Log ("Time Since Last Event is " + GestureWorksUnity.Instance.TimeSinceLastEvent);
		//Debug.Log ("positionCam is " + CamMove.positionCam);
		//Debug.Log ("rotationCam is " + CamMove.rotationCam);
		//Debug.Log ("defautCamPosition is " + GUI_DefaultMenuSelection.defaultCamPosition);

		SceneTimer();
		CamTimer();

	}

	void SceneTimer()
	{
		//Debug.Log(GUI_DefaultMenuSelection.currentScene);
		if(GestureWorksUnity.Instance.TimeSinceLastEvent >= globalResetTimer)
		{
			//Debug.Log ("Switching Scenes");
			if(Application.loadedLevelName != GUI_DefaultMenuSelection.defaultScene)
			{
				//_Timedout = true;

				imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();

				StartCoroutine(SwitchToDefaultScene());
			}
		}
	}

	void CamTimer()
	{
		//Debug.Log ("Active");
		if(GestureWorksUnity.Instance.TimeSinceLastEvent >= globalResetTimer)
		{
			//Debug.Log ("TimeOut");
			if(CamMove.positionCam != GUI_DefaultMenuSelection.defaultCamPosition)			
			{
				CamMove.positionCam = GUI_DefaultMenuSelection.defaultCamPosition;
				CamMove.rotationCam = GUI_DefaultMenuSelection.defaultCamRotation;
			}
		}
	}

	public IEnumerator SwitchToDefaultScene()
	{
		//GUI_BottomNav.gui_BottomNavFade = true;
		yield return new WaitForSeconds(1);
		gestureWorks.SwitchScenes(GUI_DefaultMenuSelection.defaultScene);
	}

}
