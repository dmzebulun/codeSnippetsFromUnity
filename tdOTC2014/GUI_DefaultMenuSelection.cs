using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

[ExecuteInEditMode]
public class GUI_DefaultMenuSelection : TouchObject
{
	public GestureWorksScript gestureWorks;

	//Default Menu Selection
	public static Vector3 defaultCamPosition;
	public static Quaternion defaultCamRotation;
	public static string defaultScene;
	public static string currentScene;
	//public static float globalResetTimer = 180.0f; //Three Minute Timer for Timeout Script

	//GUI Lean Tween Variables
	public GUISkin customSkin;
	private float w;
	private float h;
	private LTRect buttonMM;

	private LTRect buttonAssets;
	private LTRect buttonAssets01;
	private LTRect buttonAssets02;
	private LTRect buttonAssets03;
	private LTRect buttonAssets04;

	private LTRect buttonSubSea;
	private LTRect buttonSubSea01;
	private LTRect buttonSubSea02;
	private LTRect buttonSubSea03;
	private LTRect buttonSubSea04;
	private LTRect buttonSubSea05;
	private LTRect buttonSubSea06;
	private LTRect buttonSubSea07;
	private LTRect buttonSubSea08;
	private LTRect buttonSubSea09;

	private LTRect buttonPower;
	private LTRect buttonPower01;
	private LTRect buttonPower02;
	private LTRect buttonPower03;
	private LTRect buttonPower04;
	private LTRect buttonPower05;
	//private LTRect buttonPower06;
	//private LTRect buttonPower07;

	private LTRect buttonAbout;
	private LTRect buttonAbout01;
	private LTRect buttonAbout02;
	private LTRect buttonAbout03;
	private LTRect buttonAbout04;
	private LTRect buttonAbout05;
	private LTRect buttonAbout06;

	private LTRect guiBox;
	private LTRect fadeBox;

	//Screen Fade Variables
	public GameObject imageTransitionPlane;
	public float fadeSpeed = 1.5f;
	public bool sceneStarting = true;
	public bool sceneEnding = false;


	//Timeout Variables
	public float defaultMenuTimer = 25.0f;




	//Set everything up
	void Start()
	{
		sceneEnding = false;
		CamMove.defaultPandR = false;
		GUI_BottomNav.returnedFromVideo = false;

		imageTransitionPlane.GetComponent<ImageTrans>().ImageTransStartStateForMenu();

		//guiTexture.pixelInset = new Rect(0f,0f, Screen.width, Screen.height);
		//guiTexture.color = Color.black;


		w = Screen.width;
		h = Screen.height;
		guiBox = new LTRect(0.4f*w, 0.06f*h, 400, 50 );
		buttonMM = new LTRect(0.4f*w, 0.11f*h, 400, 75 );

		buttonAssets = new LTRect(0.13f*w, 0.19f*h, 300, 80 );
		buttonAssets01 = new LTRect(0.145f*w, 0.27f*h, 250, 80 );
		buttonAssets02 = new LTRect(0.145f*w, 0.35f*h, 250, 80 );
		buttonAssets03 = new LTRect(0.145f*w, 0.43f*h, 250, 80 );
		buttonAssets04 = new LTRect(0.145f*w, 0.51f*h, 250, 80 );

		buttonSubSea = new LTRect(0.33f*w, 0.19f*h, 300, 80);
		buttonSubSea01 = new LTRect(0.34f*w, 0.27f*h, 250, 80);
		buttonSubSea02 = new LTRect(0.34f*w, 0.35f*h, 250, 80);
		buttonSubSea03 = new LTRect(0.34f*w, 0.43f*h, 250, 80);
		buttonSubSea04 = new LTRect(0.34f*w, 0.51f*h, 250, 80);
		buttonSubSea05 = new LTRect(0.34f*w, 0.59f*h, 250, 80);
		buttonSubSea06 = new LTRect(0.34f*w, 0.67f*h, 250, 80);
		buttonSubSea07 = new LTRect(0.34f*w, 0.75f*h, 250, 80);
		buttonSubSea08 = new LTRect(0.34f*w, 0.83f*h, 250, 80);
		buttonSubSea09 = new LTRect(0.34f*w, 0.91f*h, 250, 80);

		buttonPower = new LTRect(0.53f*w, 0.19f*h, 300, 80 );
		buttonPower01 = new LTRect(0.54f*w, 0.27f*h, 250, 80 );
		buttonPower02 = new LTRect(0.54f*w, 0.35f*h, 250, 80 );
		buttonPower03 = new LTRect(0.54f*w, 0.43f*h, 250, 80 );
		buttonPower04 = new LTRect(0.54f*w, 0.51f*h, 250, 80 );
		buttonPower05 = new LTRect(0.54f*w, 0.59f*h, 250, 80 );
		//buttonPower06 = new LTRect(0.54f*w, 0.8f*h, 250, 80 );
		//buttonPower07 = new LTRect(0.54f*w, 0.9f*h, 250, 80 );

		buttonAbout = new LTRect(0.73f*w, 0.19f*h, 300, 80);
		buttonAbout01 = new LTRect(0.74f*w, 0.27f*h, 250, 80);
		buttonAbout02 = new LTRect(0.74f*w, 0.35f*h, 250, 80);
		buttonAbout03 = new LTRect(0.74f*w, 0.43f*h, 250, 80);
		buttonAbout04 = new LTRect(0.74f*w, 0.51f*h, 250, 80);
		buttonAbout05 = new LTRect(0.74f*w, 0.59f*h, 250, 80);
		buttonAbout06 = new LTRect(0.74f*w, 0.67f*h, 250, 80);

		GuiStartState();

		GuiFadeIn();

		defaultCamPosition = new Vector3(-524, -62, -77);
		defaultCamRotation = Quaternion.Euler(358,249,0);
		defaultScene = "Teledyne_Menu";

	}

	public void Update()
	{
		if(sceneStarting == true)
		{
			//StartScene();
		}

		if(sceneEnding == true)
		{
			//StartCoroutine(FadeToBlack());
		}

		DefaultSelectionTimeout();

		//Debug.Log ("Alpha :" + guiTexture.color.a);
	}
	//Screen Fade scripting
	
	void StartScene()
	{
		StartCoroutine(FadeToClear());
		if(guiTexture.color.a <= 0.05f)
		{
			guiTexture.color = Color.clear;
			guiTexture.enabled = false;
			sceneStarting = false;
		}
	}


	public IEnumerator FadeToClear()
	{
		guiTexture.color = Color.Lerp(guiTexture.color, Color.clear, fadeSpeed * Time.deltaTime);
		
		yield return null;
	}
	
	public IEnumerator FadeToBlack()
	{

		yield return new WaitForSeconds(1f);
		//Debug.Log ("FadeToBlack");
		guiTexture.enabled = true;
		guiTexture.color = Color.Lerp(guiTexture.color, new Color(0,0,0,1), fadeSpeed * Time.deltaTime);

		if(guiTexture.color.a >=.9f)
		{
			guiTexture.color = Color.black;
		}
		

	}
	
	void OnGUI()
	{
		GUI.skin = customSkin;

		GUI.Box (guiBox.rect, "Select Default Menu");

		if (GUI.Button(buttonMM.rect, "Main Menu"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "Teledyne_Menu";
			defaultCamPosition = new Vector3(-524, -62, -77);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToDefaultMenu());
		}

		if (GUI.Button(buttonAssets.rect, "Asset Integrity\n and Corrosion Monitoring"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "Teledyne_Menu";
			defaultCamPosition = new Vector3(-521, -64, -100);
			defaultCamRotation = Quaternion.Euler(358,267,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToDefaultMenu());
		}

		if(GUI.Button (buttonAssets01.rect, "Ring Pair\n Corrosion Monitor"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "1 RPCM";
			defaultCamPosition = new Vector3(-521, -64, -100);
			defaultCamRotation = Quaternion.Euler(358,267,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory01());
		}
		if(GUI.Button (buttonAssets02.rect, "Corrosion Monitoring:\n Topside Systems"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "2 CorrosionMonitoring";
			defaultCamPosition = new Vector3(-524, -62, -100);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory02());
		}
		if(GUI.Button (buttonAssets03.rect, "PT Sensor"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "3 PT Sensor";
			defaultCamPosition = new Vector3(-524, -62, -100);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory03());
		}
		if(GUI.Button (buttonAssets04.rect, "Corrosion and Erosion Sensors"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "4 Cormon";
			defaultCamPosition = new Vector3(-524, -62, -100);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory04());
		}

		if (GUI.Button(buttonSubSea.rect, "SubSea Data Transmission:\n Optical, Hybrid, and Broadband Solutions"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "Teledyne_Menu";
			defaultCamPosition = new Vector3(-544, -62, -57);
			defaultCamRotation = Quaternion.Euler(360,225,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToDefaultMenu());
		}

		if(GUI.Button (buttonSubSea01.rect, "Subsea Hybrid \n Connectors"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "5 SubseaHybrid";
			defaultCamPosition = new Vector3(-524, -62, -57);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory05());
		}
		if(GUI.Button (buttonSubSea02.rect, "Rolling Seal Connector"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "6 RollingSeal";
			defaultCamPosition = new Vector3(-524, -62, -57);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory06());
		}
		if(GUI.Button (buttonSubSea03.rect, "Subsea Ethernet"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "7 Subsea Ethernet";
			defaultCamPosition = new Vector3(-524, -62, -57);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory07());
		}
		if(GUI.Button (buttonSubSea04.rect, "API16D Connectors"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "8 API";
			defaultCamPosition = new Vector3(-524, -62, -57);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory08());
		}
		if(GUI.Button (buttonSubSea05.rect, "Flange Harness Assembly"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "9 Flange";
			defaultCamPosition = new Vector3(-524, -62, -57);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory09());
		}
		if(GUI.Button (buttonSubSea06.rect, "DGO Penetratros"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "10 DGO";
			defaultCamPosition = new Vector3(-524, -62, -57);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory10());
		}
		if(GUI.Button (buttonSubSea07.rect, "Cable Terminations"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "11 Fact";
			defaultCamPosition = new Vector3(-524, -62, -57);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory11());
		}

		if(GUI.Button (buttonSubSea08.rect, "MCDU:\n Distribution Systems"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "12 MCDU";
			defaultCamPosition = new Vector3(-524, -62, -57);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory12());
		}
		if(GUI.Button (buttonSubSea09.rect, "Wellhead Feedthrough System"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "13 Wellhead";
			defaultCamPosition = new Vector3(-524, -62, -57);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory13());
		}

		if (GUI.Button(buttonPower.rect, "Power Transmission:\n Electrical Solutions for Harsh Environments"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "Teledyne_Menu";
			defaultCamPosition = new Vector3(-508, -62, -97);
			defaultCamRotation = Quaternion.Euler(359,180,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToDefaultMenu());
		}

		if(GUI.Button (buttonPower01.rect, "Wet Mate Connectors"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "14 WetMate";
			defaultCamPosition = new Vector3(-524, -62, -97);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory14());
		}
		if(GUI.Button (buttonPower02.rect, "Subsea Power\n Wet Mate Connectors"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "15 SubseaPower";
			defaultCamPosition = new Vector3(-524, -62, -97);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory15());
		}
		if(GUI.Button (buttonPower03.rect, "Cable Solutions"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "16 Cable";
			defaultCamPosition = new Vector3(-524, -62, -97);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory16());
		}
		if(GUI.Button (buttonPower04.rect, "Ceramic Penetrators"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "17 Ceramic";
			defaultCamPosition = new Vector3(-524, -62, -97);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory17());
		}
		if(GUI.Button (buttonPower05.rect, "Subsea Vehicle\n Connections"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "19 SubseaVehicle";
			defaultCamPosition = new Vector3(-524, -62, -97);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory19());
		}
		/*if(GUI.Button (buttonPower06.rect, "Subsea Vehicle Interconnect"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultCamPosition = new Vector3(-524, -62, -77);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToDefaultMenu());
		}
		if(GUI.Button (buttonPower07.rect, "ROV Connections"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultCamPosition = new Vector3(-524, -62, -77);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToDefaultMenu());
		}*/


		if (GUI.Button(buttonAbout.rect, "More Information"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "Teledyne_Menu";
			defaultCamPosition = new Vector3(-554, -62, -46);
			defaultCamRotation = Quaternion.Euler(360,265,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToDefaultMenu());
		}
		if(GUI.Button (buttonAbout01.rect, "About Teledyne"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "21 AboutTeledyne";
			defaultCamPosition = new Vector3(-524, -62, -47);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory20());
		}
		if(GUI.Button (buttonAbout02.rect, "Teledyne Marine Systems"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "23 MarineSystems";
			defaultCamPosition = new Vector3(-524, -62, -47);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory21());
		}
		if(GUI.Button (buttonAbout03.rect, "Marine Acoustic Imageing"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "23 AboutMarine";
			defaultCamPosition = new Vector3(-524, -62, -47);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory22());
		}

		if(GUI.Button (buttonAbout04.rect, "Reliability"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "24 ReliabilityProgram";
			defaultCamPosition = new Vector3(-524, -62, -47);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory22c());
		}

		if(GUI.Button (buttonAbout05.rect, "Subsea Video Animation"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "21_About_SubseaVideo";
			defaultCamPosition = new Vector3(-524, -62, -47);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory22a());
		}

		if(GUI.Button (buttonAbout06.rect, "Locations"))
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultScene = "21 Map";
			defaultCamPosition = new Vector3(-524, -62, -47);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToCategory22b());
		}
	}

	//GUI fading scripting
	private void GuiStartState()
	{
		LeanTween.alpha(buttonMM , 0f, 0.01f);
		LeanTween.alpha(buttonAssets , 0f, 0.01f);
		LeanTween.alpha(buttonAssets01 , 0f, 0.01f);
		LeanTween.alpha(buttonAssets02 , 0f, 0.01f);
		LeanTween.alpha(buttonAssets03 , 0f, 0.01f);
		LeanTween.alpha(buttonAssets04 , 0f, 0.01f);
		LeanTween.alpha(buttonSubSea , 0f, 0.01f);
		LeanTween.alpha(buttonSubSea01 , 0f, 0.01f);
		LeanTween.alpha(buttonSubSea02 , 0f, 0.01f);
		LeanTween.alpha(buttonSubSea03 , 0f, 0.01f);
		LeanTween.alpha(buttonSubSea04 , 0f, 0.01f);
		LeanTween.alpha(buttonSubSea05 , 0f, 0.01f);
		LeanTween.alpha(buttonSubSea06 , 0f, 0.01f);
		LeanTween.alpha(buttonSubSea07 , 0f, 0.01f);
		LeanTween.alpha(buttonSubSea08 , 0f, 0.01f);
		LeanTween.alpha(buttonSubSea09 , 0f, 0.01f);
		LeanTween.alpha(buttonPower , 0f, 0.01f);
		LeanTween.alpha(buttonPower01 , 0f, 0.01f);
		LeanTween.alpha(buttonPower02 , 0f, 0.01f);
		LeanTween.alpha(buttonPower03 , 0f, 0.01f);
		LeanTween.alpha(buttonPower04 , 0f, 0.01f);
		LeanTween.alpha(buttonPower05 , 0f, 0.01f);
		//LeanTween.alpha(buttonPower06 , 0f, 0.01f);
		//LeanTween.alpha(buttonPower07 , 0f, 0.01f);
		LeanTween.alpha(buttonAbout , 0f, 0.01f);
		LeanTween.alpha(buttonAbout01 , 0f, 0.01f);
		LeanTween.alpha(buttonAbout02 , 0f, 0.01f);
		LeanTween.alpha(buttonAbout03 , 0f, 0.01f);
		LeanTween.alpha(buttonAbout04 , 0f, 0.01f);
		LeanTween.alpha(buttonAbout05 , 0f, 0.01f);
		LeanTween.alpha(buttonAbout06 , 0f, 0.01f);
		LeanTween.alpha(guiBox , 0f, 0.01f);
	}

	private void GuiFadeIn()
	{	
		LeanTween.alpha(buttonMM , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAssets , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAssets01 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAssets02 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAssets03 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAssets04 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea01 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea02 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea03 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea04 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea05 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea06 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea07 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea08 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea09 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonPower , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonPower01 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonPower02 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonPower03 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonPower04 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonPower05 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		//LeanTween.alpha(buttonPower06 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		//LeanTween.alpha(buttonPower07 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAbout , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAbout01 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAbout02 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAbout03 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAbout04 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAbout05 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAbout06 , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(guiBox , 1f, 1f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
	}

	private void GuiFadeOut()
	{
		LeanTween.alpha(buttonMM , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAssets , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAssets01 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAssets02 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAssets03 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAssets04 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea01 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea02 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea03 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea04 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea05 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea06 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea07 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea08, 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonSubSea09 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonPower , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonPower01 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonPower02 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonPower03 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonPower04 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonPower05 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		//LeanTween.alpha(buttonPower06 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		//LeanTween.alpha(buttonPower07 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAbout , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAbout01 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAbout02, 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAbout03 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAbout04 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAbout05 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(buttonAbout06 , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(guiBox , 0f, 1f).setEase(LeanTweenType.easeOutQuad);
	}
	// This will auto select the default menu parameter to the main menu after the defaultMenuTimer runs out. 
	void DefaultSelectionTimeout()
	{
		if(GestureWorksUnity.Instance.TimeSinceLastEvent >= defaultMenuTimer)
		{
			GuiFadeOut();
			imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
			defaultCamPosition = new Vector3(-524, -62, -77);
			defaultCamRotation = Quaternion.Euler(358,249,0);
			Debug.Log ("P: " + defaultCamPosition);
			Debug.Log ("R: " + defaultCamPosition);
			sceneEnding = true;
			StartCoroutine(SwitchToDefaultMenu());
		}
	}

	//Menu Coroutines
	private IEnumerator SwitchToDefaultMenu()
	{
		Debug.Log ("Called");
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes(defaultScene);
	}

	private IEnumerator SwitchToMainMenu()
	{
		Debug.Log ("Called");
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("Teledyne_Menu");
	}
	
	private IEnumerator SwitchToCategory01()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("1 RPCM");
	}

	private IEnumerator SwitchToCategory02()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("2 CorrosionMonitoring");
	}
	private IEnumerator SwitchToCategory03()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("3 PT Sensor");
	}

	private IEnumerator SwitchToCategory04()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("4 Cormon");
	}
	private IEnumerator SwitchToCategory05()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("5 SubseaHybrid");
	}
	private IEnumerator SwitchToCategory06()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("6 RollingSeal");
	}
	private IEnumerator SwitchToCategory07()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("7 Subsea Ethernet");
	}
	private IEnumerator SwitchToCategory08()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("8 API");
	}
	private IEnumerator SwitchToCategory09()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("9 Flange");
	}
	private IEnumerator SwitchToCategory10()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("10 DGO");
	}
	private IEnumerator SwitchToCategory11()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("11 Fact");
	}
	private IEnumerator SwitchToCategory12()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("12 MCDU");
	}
	private IEnumerator SwitchToCategory13()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("13 Wellhead");
	}
	private IEnumerator SwitchToCategory14()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("14 WetMate");
	}
	private IEnumerator SwitchToCategory15()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("15 SubseaPower");
	}
	private IEnumerator SwitchToCategory16()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("16 Cable");
	}
	private IEnumerator SwitchToCategory17()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("17 Ceramic");
	}
	private IEnumerator SwitchToCategory19()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("19 SubseaVehicle");
	}
	private IEnumerator SwitchToCategory20()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("21 AboutTeledyne");
	}
	private IEnumerator SwitchToCategory21()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("23 MarineSystems");
	}
	private IEnumerator SwitchToCategory22()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("23 AboutMarine");
	}
	private IEnumerator SwitchToCategory22a()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("21_About_SubseaVideo");
	}
	private IEnumerator SwitchToCategory22b()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("21 Map");
	}
	private IEnumerator SwitchToCategory22c()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("24 ReliabilityProgram");
	}/*
	private IEnumerator SwitchToCategory04()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("SubCat_Template");
	}
	private IEnumerator SwitchToCategory04()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("SubCat_Template");
	}
	private IEnumerator SwitchToCategory04()
	{
		yield return new WaitForSeconds(2);
		gestureWorks.SwitchScenes("SubCat_Template");
	}*/

}


