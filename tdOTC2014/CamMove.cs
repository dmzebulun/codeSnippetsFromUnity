﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class CamMove : TouchObject
{
	//This is a list of all the Position and Rotation Parameters for the various buttons
	//Main Menu Position = new Vector3 (-524, -62, -77);
	//Main Menu Rotation = Quaternion.Euler(358,249,0);


	public static bool defaultPandR;
	public static Vector3 positionCam;
	public static Quaternion rotationCam;

	public float smooth = 2F;
	public float speed = .01F;

	private Vector3 zeroPosition = new Vector3(0,0,0);
	private Quaternion zeroRotation = Quaternion.Euler(0,0,0);

	void Start ()
	{

		SingleRunSetDefault();
	
		if(TimeOut._Timedout == true)
		{
		positionCam = GUI_DefaultMenuSelection.defaultCamPosition;
		rotationCam = GUI_DefaultMenuSelection.defaultCamRotation;

		TimeOut._Timedout = false;
		}
	}

	void Update ()
	{


		PositionChanging();
		RotationChange ();
	}

	public void PositionChanging ()
	{
		transform.position = Vector3.Lerp (transform.position, positionCam, Time.deltaTime * smooth);
		//Debug.Log (positionCam);

	}
	public void RotationChange ()
	{
		transform.rotation= Quaternion.Lerp(transform.rotation, rotationCam, Time.time * speed); 
		//Debug.Log (rotationCam);
	}

	public void SingleRunSetDefault()
	{
		if (defaultPandR == false)
		{
		positionCam = GUI_DefaultMenuSelection.defaultCamPosition;
		rotationCam = GUI_DefaultMenuSelection.defaultCamRotation;
		}
		defaultPandR = true;
	}

}
