﻿using UnityEngine;
using System.Collections;

public class ImageTransBlack : MonoBehaviour 
{
	public GameObject imageTransPlane;



	//Button for testing LeenTween function. Comment out before build.
/* void OnGUI()
	{
		if(GUI.Button(new Rect(200,200,200,200), "MoveIn"))
		{
			ImageTransMoveIn();
		}
		if(GUI.Button(new Rect(400, 400, 200, 200), "MoveOut"))
		{
			ImageTransMoveOut();
		}
	}
*/

	public void ImageTransStartStateForMenu()
	{
		Debug.Log("Transtion Set for Menu");
		LeanTween.moveLocalZ(imageTransPlane, .2f, .01f).setEase(LeanTweenType.easeInOutExpo);
		LeanTween.alpha(imageTransPlane, 0.01f, .01f).setEase(LeanTweenType.easeInOutQuad);
	}

	public void ImageTransStartStateRegular()
	{
		Debug.Log("Transtion Set for Regular Scene");
		LeanTween.moveLocalZ(imageTransPlane, 1f, .01f).setEase(LeanTweenType.easeInOutExpo);
		LeanTween.alpha(imageTransPlane, 1.0f, .01f).setEase(LeanTweenType.easeInOutQuad);
	}

	public void ImageTransMoveIn()
	{
		Debug.Log("Transtion Moving In");
		LeanTween.moveLocalZ(imageTransPlane, 1f, 1f).setEase(LeanTweenType.easeInOutExpo);
		LeanTween.alpha(imageTransPlane, 1.0f, 1.5f).setEase(LeanTweenType.easeInOutQuad);
	}

	public void ImageTransMoveOut()
	{
		Debug.Log("Transtion Moving Out");
		LeanTween.moveLocalZ(imageTransPlane, .2f, 1f).setEase(LeanTweenType.easeInOutExpo);
		LeanTween.alpha (imageTransPlane, 0.0f, 1f).setEase(LeanTweenType.easeInOutQuad);
	}
}
