﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class ScrollAnimationScript : TouchObject
{
	public static bool scrollableIcon;
	public float smooth = 2F;


	public Vector3 startPosition;
	public Vector3 endPosition;
	public Vector3 destination;
	
	// Use this for initialization
	void Start () 
	{
		scrollableIcon = false;


		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (scrollableIcon == true)
		{
			destination = endPosition;
		}
		else
		{
			destination = startPosition;	
		}

		positionChangingGesture();
	}
	
	void positionChangingGesture()
	{
		transform.position = Vector3.Lerp(transform.position, destination, Time.deltaTime * smooth);
	}
}