﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class ScreenFadeOut : TouchObject 
{

	//private bool sceneStarting = true;
	public static bool sceneEnding = false;
	
	public float fadeSpeed = 1.5f;

	public GameObject imageTransitionPlane;


	
	
	public void Start()
	{
		guiTexture.pixelInset = new Rect(0f,0f, Screen.width, Screen.height);
		guiTexture.color = Color.black;
		
		//sceneStarting = true;
	}
	public void Update()
	{
		/*if(sceneStarting == true)
		{
			StartCoroutine(FadeToClear());
		}*/
		
		if(sceneEnding == true)
		{
			StartCoroutine(FadeToBlack());

		}

		//Debug.Log ("Alpha :" + guiTexture.color.a);
	}


	//Screen Fade scripting

/*	public IEnumerator FadeToClear()
	{
		guiTexture.color = Color.Lerp(guiTexture.color, Color.clear, fadeSpeed * Time.deltaTime);
		
		if(guiTexture.color.a <= 0.05f)
		{
			guiTexture.color = Color.clear;
			guiTexture.enabled = false;
			sceneStarting = false;
		}
		
		yield return null;
	}*/
	
	public IEnumerator FadeToBlack()
	{
		imageTransitionPlane.GetComponent<ImageTrans>().ImageTransMoveIn();
		GUI_BottomNav.gui_BottomNavFade = true;
		yield return new WaitForSeconds(1f);

		//Debug.Log ("FadeToBlack");
		guiTexture.enabled = true;
		guiTexture.color = Color.Lerp(guiTexture.color, new Color(0,0,0,1), fadeSpeed * Time.deltaTime);
		
		if(guiTexture.color.a >=.9f)
		{
			guiTexture.color = Color.black;
		}
		sceneEnding = false;
			
	}
}







