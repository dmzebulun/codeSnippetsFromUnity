﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class ModelMoverMarineSonar : TouchObject
{
	
	public static Vector3 positionSonarModel;
	//public static Quaternion rotationCam;

	public float smooth = 2F;
	public float speed = .01F;
	
	void Start ()
	{
		//positionCam = new Vector3 (-524, -62, -77);
		//rotationCam = Quaternion.Euler(358,249,0);

		positionSonarModel = new Vector3(-531.7664f, -63.11732f, -80.15838f);
		//rotationCam = GUI_DefaultMenuSelection.defaultCamRotation;
	}

	void Update (){
		PositionChanging();
		//RotationChange ();
	}

	public void PositionChanging (){
		transform.position = Vector3.Lerp (transform.position, positionSonarModel, Time.deltaTime * smooth);
		//Debug.Log (positionCam);

		}
	/*public void RotationChange (){
		Camera.main.transform.rotation= Quaternion.Lerp(Camera.main.transform.rotation, rotationCam, Time.time * speed); 
		//Debug.Log (rotationCam);
		}*/

}
