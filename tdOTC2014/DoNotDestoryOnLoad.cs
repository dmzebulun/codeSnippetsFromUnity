﻿using UnityEngine;
using System.Collections;

public class DoNotDestoryOnLoad : MonoBehaviour 
{

	void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}
}
