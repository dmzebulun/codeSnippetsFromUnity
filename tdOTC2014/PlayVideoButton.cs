﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class PlayVideoButton : TouchObject 
{
	public GestureWorksScript gestureWorks;
	//private Vector3 startPosition = new Vector3(-530, -40, -75);
	private Vector3 endPosition;
	public static string returnScene;
	//public static bool returnedFromVideo;
	public string videoScene;
	public GameObject imageTransitionPlane;
	public GameObject videoPlane;

	public float smooth = 2F;
	public float speed = .005F;

	void Start () 
	{
		returnScene = Application.loadedLevelName;
		endPosition = new Vector3(-530, -60, -75);
	}

	void Update()
	{
		positionChanging();
	}

	public void Tap(GestureEvent gEvent)
	{
		gestureWorks.SwitchScenes(videoScene);

		//GUI_BottomNav.sceneIsSwitchingGuiFade = true;
		//GUI_BottomNav.gui_BottomNavFade = true;
		//imageTransitionPlane.GetComponent<ImageTransBlack>().ImageTransMoveIn();
		//videoPlane.GetComponent<VideoPlaybackBasic>().VideoPlay();
		//StartCoroutine(PlayVideo());


	}

	IEnumerator PlayVideo()
	{
		yield return new WaitForSeconds(1.5f);
		//Debug.Log("Tapped");
		GUI_BottomNav.returnedFromVideo = true;
		gestureWorks.SwitchScenes(videoScene);
	}

	public void positionChanging()
	{
		transform.position = Vector3.Lerp (transform.position, endPosition, Time.deltaTime * smooth);
	}

}
