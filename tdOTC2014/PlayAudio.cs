﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

public class PlayAudio : TouchObject 
{	
	public string menuAudioClip;
	private bool tapped;

	void Awake()
	{
		tapped = false;
	}

	public void Tap(GestureEvent gEvent)
	{

		if(tapped == true)
		{
			AudioController.StopAll(1);
			tapped = false;
		}
		else
		{
			AudioController.Play(menuAudioClip);
			tapped = true;
		}	             
	}
}
