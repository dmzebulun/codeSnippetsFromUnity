﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;


[RequireComponent(typeof (AudioSource))]

public class VideoPlaybackNoMovement : TouchObject
{

	public GestureWorksScript gestureWorks;
	public MovieTexture movTexture;
	public string sceneToSwitchTo;

	// Use this for initialization
	void Start () 
	{

		renderer.material.mainTexture = movTexture as MovieTexture;
		audio.clip = movTexture.audioClip;
		movTexture.Play ();
		audio.Play();
	}

	void Update()
	{
		//Debug.Log (movTexture.isPlaying);
		if(movTexture.isPlaying == false)
		{
			StartCoroutine(VideoEnd());
		}
	}
	

	void DoubleTap (GestureEvent gEvent)
	{
		//Debug.Log ("Tapped");

		movTexture.Stop();
		StartCoroutine(VideoEnd());
	}


	IEnumerator VideoEnd()
	{

		ScreenFadeOut.sceneEnding = true;
		
		yield return new WaitForSeconds(.5f);
		
		//gestureWorks.SwitchScenes(PlayVideoButton.returnScene);
		gestureWorks.SwitchScenes(PlayVideoButtonNoMovement.returnScene02);

	}
}
