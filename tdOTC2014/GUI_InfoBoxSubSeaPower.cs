﻿using UnityEngine;
using System.Collections;
using GestureWorksCoreNET;
using GestureWorksCoreNET.Unity;

[ExecuteInEditMode]
public class GUI_InfoBoxSubSeaPower : TouchObject
{
	//public GestureWorksScript gestureWorks;

	//Text and Graph Assets
	public string textBoxTitle01;
	public string textBoxTitle02;
	public string textBoxTitle03;
	public string textBoxTitle04;

	public TextAsset textSideContent01;
	public TextAsset textSideContent02;
	public TextAsset textSideContent03;
	public TextAsset textSideContent04;
	public TextAsset textSideContent05;
	public TextAsset textSideContent06;
	public TextAsset emptyText;
	
	public Texture graphContent01;
	public Texture graphContent02;
	public Texture graphContent03;
	public Texture graphContent04;
	public Texture graphContent05;
	public Texture graphContent06;

	private TextAsset currentText;
	private Texture currentGraphContent01;
	private Texture currentGraphContent02;
	private Texture currentGraphContent03;
	private Texture currentGraphContent04;
	private Texture currentGraphContent05;
	private Texture currentGraphContent06;

	private float scrollLock;


	//LeanTween Variables

	private float w;
	private float h;

	private LTRect textTitleBox01;
	private LTRect textTitleBox02;
	private LTRect textTitleBox03;
	private LTRect textContainerBox01;

/*	
 * private LTRect textLowerBox01;
	private LTRect textLowerBox02;
	private LTRect textLowerBox03;
	private LTRect textLowerBox04;
	private LTRect textLowerBox05;
	private LTRect textLowerBox06;
*/

	private LTRect graphBox01;
	private LTRect graphBox02;
	private LTRect graphBox03;
	private LTRect graphBox04;
	private LTRect graphBox05;
	private LTRect graphBox06;


	//Custom GUI Skin. This should be set to the InfoBox Skin.
	public GUISkin customSkin;
	private int guiDepth = 1;

	public Vector2 scrollPosition = Vector2.zero;
	public Vector2 scrollNDrag;
	public float dragMultiplier = 0.02f;

	//private bool _isDragging;
	private float dragY;
	private float dragYread;


	void Awake()
	{
		w = Screen.width;
		h = Screen.height;



		//This sets the sizes and positions of the menu tabs for the main info box.
		textTitleBox01 = new LTRect(.02f*w, .04f*h, 150, 40);
		textTitleBox02 = new LTRect(.105f*w, .04f*h, 150, 40);
		textTitleBox03 = new LTRect(.19f*w, .04f*h, 150, 40);
		//textTitleBox04 = new LTRect(.08f*w, .04f*h, 150, 40);

		textContainerBox01 = new LTRect(.02f*w, .078f*h, 475, 850);

		graphBox01 = new LTRect(.022f*w, .6f*h, 470, 470);
		graphBox02 = new LTRect(.022f*w, .9f*h, 470, 470);
		graphBox03 = new LTRect(.022f*w, 1.2f*h, 470, 470);
		graphBox04 = new LTRect(.022f*w, .04f*h, 470, 470);
		graphBox05 = new LTRect(.022f*w, .4f*h, 470, 470);
		graphBox06 = new LTRect(.022f*w, .3f*h, 470, 470);



	}

	void Start() //Assign LeanTween variables sizes and position
	{
		currentText = textSideContent01;
		currentGraphContent01 = null;
		currentGraphContent02 = null;
		currentGraphContent03 = null;
		currentGraphContent04 = null;
		currentGraphContent05 = null;
		currentGraphContent06 = null;

		GuiStartState();

		GuiFadeIn();

		scrollLock = 850;
	
	}

	void Update()
	{
		GuiFadeCheck();
		//Debug.Log ("dragY is " + dragY);
		//Debug.Log ("dragYread is " + dragYread);

	}

	void OnGUI() 
	{
		GUI.skin = customSkin;
		GUI.depth = guiDepth;

		//Main Content Box on Left


		//Menu Buttons 
		if(GUI.Toggle (textTitleBox01.rect, false, textBoxTitle01))
		{
			currentText = textSideContent01;
			currentGraphContent01 = null;
			currentGraphContent02 = null;
			currentGraphContent03 = null;
			currentGraphContent03 = null;
			currentGraphContent05 = null;
			currentGraphContent06 = null;

			scrollPosition = new Vector2(0,0);

			scrollLock = 850;

			ScrollAnimationScript.scrollableIcon = false;
		}

		if(GUI.Toggle (textTitleBox02.rect, false, textBoxTitle02))
		{
			currentText = textSideContent02;
			currentGraphContent01 = graphContent01;
			currentGraphContent02 = graphContent02;
			currentGraphContent03 = graphContent03;
			currentGraphContent04 = null;
			currentGraphContent05 = null;
			currentGraphContent06 = null;

			scrollPosition = new Vector2(0,0);

			scrollLock = 1600;

			ScrollAnimationScript.scrollableIcon = true;

		}

		if(GUI.Toggle (textTitleBox03.rect, false, textBoxTitle03))
		{
			currentText = emptyText;
			currentGraphContent01 = null;
			currentGraphContent02 = null;
			currentGraphContent03 = null;
			currentGraphContent04 = graphContent04;
			currentGraphContent05 = graphContent05;
			currentGraphContent06 = null;

			scrollPosition = new Vector2(0,0);

			scrollLock = 850;

			ScrollAnimationScript.scrollableIcon = false;
		}
		GUI.Box (textContainerBox01.rect, " ");


		scrollPosition = GUI.BeginScrollView( textContainerBox01.rect , scrollNDrag + scrollPosition, new Rect(.02f*w, .078f*h, 475, scrollLock));
		GUI.Box (textContainerBox01.rect, currentText.text, "transBox");

		GUI.DrawTexture (graphBox01.rect, currentGraphContent01, ScaleMode.ScaleToFit);
		GUI.DrawTexture (graphBox02.rect, currentGraphContent02, ScaleMode.ScaleToFit);
		GUI.DrawTexture (graphBox03.rect, currentGraphContent03, ScaleMode.ScaleToFit);
		GUI.DrawTexture(graphBox04.rect, currentGraphContent04, ScaleMode.ScaleToFit);
		GUI.DrawTexture(graphBox05.rect, currentGraphContent05, ScaleMode.ScaleToFit);
		GUI.DrawTexture(graphBox06.rect, currentGraphContent06, ScaleMode.ScaleToFit);

		GUI.EndScrollView();
	}

	void NDrag (GestureEvent gEvent)
	{
		dragY = gEvent.Y;

		if (dragY < 1)
		{
			dragYread = 0.0f;
		}
	
		if(dragYread == 0.0f)
		{
			dragYread = gEvent.Y;
		}

		if(dragYread >= dragY)
		{
			scrollNDrag = new Vector2(0, (dragY * dragMultiplier));
		}
		else
		{
			scrollNDrag = new Vector2(0, (dragY * dragMultiplier * -1f));
		}



	/*	if (gEvent.Y <= 500)
		{
			scrollNDrag = new Vector2(0, (gEvent.Y * dragMultiplier));
		}
		if (gEvent.Y >= 550)
		{
			scrollNDrag = new Vector2(0, (gEvent.Y * dragMultiplier * -1f));
		}*/

	
	}

	void GuiFadeCheck()
	{
		if(GUI_BottomNav.sceneIsSwitchingGuiFade == true)
		{
			GuiFadeout();
		}
	}




	void GuiStartState()
	{
		LeanTween.alpha(textTitleBox01, 0f, 0.001f);
		LeanTween.alpha(textTitleBox02, 0f, 0.001f);
		LeanTween.alpha(textTitleBox03, 0f, 0.001f);
		LeanTween.alpha(textContainerBox01, 0f, 0.001f);
		LeanTween.alpha(graphBox01, 0f, 0.001f);
		LeanTween.alpha(graphBox02, 0f, 0.001f);
		LeanTween.alpha(graphBox03, 0f, 0.001f);
		LeanTween.alpha(graphBox04, 0f, 0.001f);
		LeanTween.alpha(graphBox05, 0f, 0.001f);
		LeanTween.alpha(graphBox06, 0f, 0.001f);
	
	
	}


	void GuiFadeIn()
	{
		LeanTween.alpha(textTitleBox01 , 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textTitleBox02, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textTitleBox03, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textContainerBox01, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox01, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox02, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox03, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox04, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox05, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox06, 1f, .5f).setDelay(1.0f).setEase(LeanTweenType.easeOutQuad);


	}


	public void GuiFadeout()
	{
		LeanTween.alpha(textTitleBox01 , 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textTitleBox02, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textTitleBox03 , 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(textContainerBox01, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox01, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox02, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox03, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox04, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox05, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
		LeanTween.alpha(graphBox06, 0f, .5f).setEase(LeanTweenType.easeOutQuad);
	}	
}