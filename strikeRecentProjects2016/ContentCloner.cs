﻿using UnityEngine;
using System.Collections;

public class ContentCloner : MonoBehaviour
{
    public static GameObject currentClonedContent;



    public GameObject content01;
    public GameObject content02;
    public GameObject content03;
    public GameObject content04;
    public GameObject content05;
    public GameObject content06;
    public GameObject content07;
    public GameObject content08;

    public int cloneNumber;



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

   

    public void CloneContent(int clone)
    {
        if (Timer._screenSaverActive == false)
        {


            if (clone == 1)
            {

                currentClonedContent = Instantiate(content01, content01.transform.position, content01.transform.rotation) as GameObject;


                //LeanTween.scale(currentClonedContent, new Vector3(.04f, .04f, .04f), .1f).setDelay(1f);

                GameObject content01Image01 = GameObject.Find("Content01Image1");
                GameObject content01Image02 = GameObject.Find("Content01Image2");
                GameObject content01Image03 = GameObject.Find("Content01Image3");
                GameObject content01Image04 = GameObject.Find("Content01Image4");
                GameObject content01Image05 = GameObject.Find("Content01Image5");
                GameObject content01Image06 = GameObject.Find("Content01Image6");
                GameObject content01Image07 = GameObject.Find("Content01Image7");
                GameObject content01Image08 = GameObject.Find("Content01Image8");
                GameObject content01Image09 = GameObject.Find("Content01Image9");
                GameObject content01Image10 = GameObject.Find("Content01Image10");

                LeanTween.moveLocalZ(content01Image01, 0f, 1f).setDelay(.5f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image02, 0f, 1f).setDelay(.6f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image03, 0f, 1f).setDelay(.7f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image04, 0f, 1f).setDelay(.8f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image05, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image06, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image07, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image08, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image09, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image10, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
            }

            else if (clone == 2)
            {
                currentClonedContent = Instantiate(content02, content02.transform.position, content02.transform.rotation) as GameObject;
                LeanTween.moveLocalZ(currentClonedContent, -31.66f, 1.5f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

                //LeanTween.scale(currentClonedContent, new Vector3(.04f, .04f, .04f), .1f).setDelay(1f);

                GameObject content01Image01 = GameObject.Find("Content01Image1");
                GameObject content01Image02 = GameObject.Find("Content01Image2");
                GameObject content01Image03 = GameObject.Find("Content01Image3");
                GameObject content01Image04 = GameObject.Find("Content01Image4");
                GameObject content01Image05 = GameObject.Find("Content01Image5");

                LeanTween.moveLocalZ(content01Image01, 0f, 1f).setDelay(.5f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image02, 0f, 1f).setDelay(.6f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image03, 0f, 1f).setDelay(.7f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image04, 0f, 1f).setDelay(.8f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image05, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
            }
            else if (clone == 3)
            {
                currentClonedContent = Instantiate(content03, content03.transform.position, content03.transform.rotation) as GameObject;


                //LeanTween.scale(currentClonedContent, new Vector3(.04f, .04f, .04f), .1f).setDelay(1f);

                GameObject content01Image01 = GameObject.Find("Content01Image1");
                GameObject content01Image02 = GameObject.Find("Content01Image2");
                GameObject content01Image03 = GameObject.Find("Content01Image3");
                GameObject content01Image04 = GameObject.Find("Content01Image4");
                GameObject content01Image05 = GameObject.Find("Content01Image5");

                LeanTween.moveLocalZ(content01Image01, 0f, 1f).setDelay(.5f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image02, 0f, 1f).setDelay(.6f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image03, 0f, 1f).setDelay(.7f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image04, 0f, 1f).setDelay(.8f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image05, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
            }
            else if (clone == 4)
            {
                currentClonedContent = Instantiate(content04, content04.transform.position, content04.transform.rotation) as GameObject;

                //LeanTween.scale(currentClonedContent, new Vector3(.04f, .04f, .04f), .1f).setDelay(1f);

                GameObject content01Image01 = GameObject.Find("Content01Image1");
                GameObject content01Image02 = GameObject.Find("Content01Image2");
                GameObject content01Image03 = GameObject.Find("Content01Image3");
                GameObject content01Image04 = GameObject.Find("Content01Image4");
                GameObject content01Image05 = GameObject.Find("Content01Image5");

                LeanTween.moveLocalZ(content01Image01, 0f, 1f).setDelay(.5f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image02, 0f, 1f).setDelay(.6f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image03, 0f, 1f).setDelay(.7f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image04, 0f, 1f).setDelay(.8f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image05, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
            }
            else if (clone == 5)
            {
                currentClonedContent = Instantiate(content05, content05.transform.position, content05.transform.rotation) as GameObject;

                //LeanTween.scale(currentClonedContent, new Vector3(.04f, .04f, .04f), .1f).setDelay(1f);

                GameObject content01Image01 = GameObject.Find("Content01Image1");
                GameObject content01Image02 = GameObject.Find("Content01Image2");
                GameObject content01Image03 = GameObject.Find("Content01Image3");
                GameObject content01Image04 = GameObject.Find("Content01Image4");
                GameObject content01Image05 = GameObject.Find("Content01Image5");

                LeanTween.moveLocalZ(content01Image01, 0f, 1f).setDelay(.5f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image02, 0f, 1f).setDelay(.6f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image03, 0f, 1f).setDelay(.7f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image04, 0f, 1f).setDelay(.8f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image05, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
            }
            else if (clone == 6)
            {
                currentClonedContent = Instantiate(content06, content06.transform.position, content06.transform.rotation) as GameObject;

                //LeanTween.scale(currentClonedContent, new Vector3(.04f, .04f, .04f), .1f).setDelay(1f);

                GameObject content01Image01 = GameObject.Find("Content01Image1");
                GameObject content01Image02 = GameObject.Find("Content01Image2");
                GameObject content01Image03 = GameObject.Find("Content01Image3");
                GameObject content01Image04 = GameObject.Find("Content01Image4");
                GameObject content01Image05 = GameObject.Find("Content01Image5");
                GameObject content01Image06 = GameObject.Find("Content01Image6");
                GameObject content01Image07 = GameObject.Find("Content01Image7");
                GameObject content01Image08 = GameObject.Find("Content01Image8");
                GameObject content01Image09 = GameObject.Find("Content01Image9");



                LeanTween.moveLocalZ(content01Image01, 0f, 1f).setDelay(.5f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image02, 0f, 1f).setDelay(.6f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image03, 0f, 1f).setDelay(.7f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image04, 0f, 1f).setDelay(.8f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image05, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image06, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image07, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image08, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image09, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
            }
            else if (clone == 7)
            {
                currentClonedContent = Instantiate(content07, content07.transform.position, content07.transform.rotation) as GameObject;
                LeanTween.moveLocalZ(currentClonedContent, -24.2f, 1.5f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

                //LeanTween.scale(currentClonedContent, new Vector3(.04f, .04f, .04f), .1f).setDelay(1f);

                GameObject content01Image01 = GameObject.Find("Content01Image1");
                GameObject content01Image02 = GameObject.Find("Content01Image2");
                GameObject content01Image03 = GameObject.Find("Content01Image3");
                GameObject content01Image04 = GameObject.Find("Content01Image4");
                GameObject content01Image05 = GameObject.Find("Content01Image5");

                LeanTween.moveLocalZ(content01Image01, 0f, 1f).setDelay(.5f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image02, 0f, 1f).setDelay(.6f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image03, 0f, 1f).setDelay(.7f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image04, 0f, 1f).setDelay(.8f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image05, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
            }
            else if (clone == 8)
            {
                currentClonedContent = Instantiate(content08, content08.transform.position, content08.transform.rotation) as GameObject;

                //LeanTween.scale(currentClonedContent, new Vector3(.04f, .04f, .04f), .1f).setDelay(1f);

                GameObject content01Image01 = GameObject.Find("Content01Image1");
                GameObject content01Image02 = GameObject.Find("Content01Image2");
                GameObject content01Image03 = GameObject.Find("Content01Image3");
                GameObject content01Image04 = GameObject.Find("Content01Image4");
                GameObject content01Image05 = GameObject.Find("Content01Image5");
                GameObject content01Image06 = GameObject.Find("Content01Image6");

                LeanTween.moveLocalZ(content01Image01, 0f, 1f).setDelay(.5f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image02, 0f, 1f).setDelay(.6f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image03, 0f, 1f).setDelay(.7f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image04, 0f, 1f).setDelay(.8f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image05, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
                LeanTween.moveLocalZ(content01Image06, 0f, 1f).setDelay(.9f).setEase(LeanTweenType.easeInOutQuint);
            }
        }
        

    }

}
