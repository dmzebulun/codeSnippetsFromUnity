﻿using UnityEngine;
using System.Collections;
using System;
using TouchScript.Gestures;
using UnityEngine.SceneManagement;

public class Destory : MonoBehaviour
{

    private void OnEnable()
    {
        // subscribe to gesture's tapped event
        GetComponent<TapGesture>().Tapped += tappedHandler;
    }

    private void OnDisable()
    {
        // subscribe to gesture's tapped event
        GetComponent<TapGesture>().Tapped += tappedHandler;
    }

    private void tappedHandler(object sender, EventArgs e)
    {
        DestroyObject(Timer._activeScreenSaver);
        Timer._screenSaverActive = false;
    }
}

