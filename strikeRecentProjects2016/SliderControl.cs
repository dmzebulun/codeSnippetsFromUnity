﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SliderControl : MonoBehaviour
{   
    public Slider mainSlider;
    public GameObject mainPanel;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
       // Debug.Log(mainSlider.value);
	}

    public void SliderController()
    {
        mainPanel.transform.localPosition = new Vector3(mainSlider.value * -1, 0f, mainPanel.transform.localPosition.z);
        //_content.transform.position = new Vector3(_globalSlider.value, _content.transform.position.y, _content.transform.position.z);

    }
}
