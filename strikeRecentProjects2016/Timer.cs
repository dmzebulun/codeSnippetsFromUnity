﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Timer : MonoBehaviour
{
	public static float _timer;
	public float _timeOut = 600f;
	public GameObject _prefabScreenSaver;
	public static GameObject _activeScreenSaver;
	public bool _timerDone;
	public static bool _screenSaverActive;
	public GameObject _menuAnimationScript;


	void Start()
	{
		_timerDone = false;
		_screenSaverActive = false;
		StartCoroutine(EventTimer());
	}

	void Update()
	{
		if(_timer == (_timeOut-1) )
		{
			_timer =0;

			if(_screenSaverActive == false)
			{
				_activeScreenSaver = Instantiate(_prefabScreenSaver, new Vector3(0,0, -94.95f), Quaternion.Euler(-90f,0,0)) as GameObject;
                _menuAnimationScript.GetComponent<AnimationController>().Home();
                //_menuAnimationScript.GetComponent<AnimationController>().MenuTextScaler(9);
                _screenSaverActive = true;
			}


		}
	}


	public IEnumerator EventTimer()
	{
		for(_timer=0; _timer<=_timeOut; _timer++)
		{
			yield return new WaitForSeconds(1f);
			Debug.Log (_timer);


		}
		yield return null;
	}

    public void ResetTimer()
    {
        _timer = 0;
    }


	
}
