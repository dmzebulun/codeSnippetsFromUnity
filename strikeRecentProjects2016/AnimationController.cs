﻿using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour
{
    //Global Variables
    public static GameObject currentClone;
    public static GameObject currentProject;


    //Group 01 Variables
    #region
    
    public GameObject group1;
    public GameObject g1triangle1;
    public GameObject g1triangle2;
    public GameObject g1triangle3;
    public GameObject g1triangle4;
    public GameObject g1triangle5;
    public GameObject g1triangle6;
    public GameObject g1triangle7;
    public GameObject g1triangle8;
    public GameObject g1triangle9;
    public GameObject g1triangle10;

    private Vector3 g1triangle1position;
    private Vector3 g1triangle2position;
    private Vector3 g1triangle3position;
    private Vector3 g1triangle4position;
    private Vector3 g1triangle5position;
    private Vector3 g1triangle6position;
    private Vector3 g1triangle7position;
    private Vector3 g1triangle8position;
    private Vector3 g1triangle9position;
    private Vector3 g1triangle10position;
    private Vector3 g1testPosition;

    public Vector3 g1textHomePosition;
    public Vector3 g1textHomeRotation;
    public Vector3 g1textHomeScale;
    public GameObject g1text;
    #endregion

    //Group 02 Variables
    #region
    public GameObject group2;
    public GameObject g2triangle1;
    public GameObject g2triangle2;
    public GameObject g2triangle3;
    public GameObject g2triangle4;
    public GameObject g2triangle5;
    public GameObject g2triangle6;
    //public GameObject g2triangle7;
    //public GameObject g2triangle8;
    //public GameObject g2triangle9;
    //public GameObject g2triangle10;

    private Vector3 g2triangle1position;
    private Vector3 g2triangle2position;
    private Vector3 g2triangle3position;
    private Vector3 g2triangle4position;
    private Vector3 g2triangle5position;
    private Vector3 g2triangle6position;
    //private Vector3 g2triangle7position;
    //private Vector3 g2triangle8position;


    public Vector3 g2textHome;
    public GameObject g2text;
    #endregion

    //Group 03 Variables
    #region
    public GameObject group3;
    public GameObject g3triangle1;
    public GameObject g3triangle2;
    public GameObject g3triangle3;
    public GameObject g3triangle4;
    public GameObject g3triangle5;
    public GameObject g3triangle6;
    //public GameObject g3triangle7;
    // public GameObject g3triangle8;
    // public GameObject g3triangle9;
    // public GameObject g3triangle10;

    private Vector3 g3triangle1position;
    private Vector3 g3triangle2position;
    private Vector3 g3triangle3position;
    private Vector3 g3triangle4position;
    private Vector3 g3triangle5position;
    private Vector3 g3triangle6position;
    //private Vector3 g3triangle7position;
    //private Vector3 g3triangle8position;
    
    public Vector3 g3textHome;
    public GameObject g3text;
    #endregion

    //Group 04 Variables
    #region
    public GameObject group4;
    public GameObject g4triangle1;
    public GameObject g4triangle2;
    public GameObject g4triangle3;
    public GameObject g4triangle4;
    public GameObject g4triangle5;
    public GameObject g4triangle6;
    public GameObject g4triangle7;
    // public GameObject g4triangle8;
    // public GameObject g4triangle9;
    // public GameObject g4triangle10;
    
    private Vector3 g4triangle1position;
    private Vector3 g4triangle2position;
    private Vector3 g4triangle3position;
    private Vector3 g4triangle4position;
    private Vector3 g4triangle5position;
    private Vector3 g4triangle6position;
    private Vector3 g4triangle7position;
    //private Vector3 g4triangle8position;
    
    public Vector3 g4textHome;
    public GameObject g4text;
    #endregion

    //Group 05 Variables
    #region
    public GameObject group5;
    public GameObject g5triangle1;
    public GameObject g5triangle2;
    public GameObject g5triangle3;
    public GameObject g5triangle4;
    public GameObject g5triangle5;
    public GameObject g5triangle6;
    public GameObject g5triangle7;
    public GameObject g5triangle8;
    public GameObject g5triangle9;
    public GameObject g5triangle10;
    
    private Vector3 g5triangle1position;
    private Vector3 g5triangle2position;
    private Vector3 g5triangle3position;
    private Vector3 g5triangle4position;
    private Vector3 g5triangle5position;
    private Vector3 g5triangle6position;
    private Vector3 g5triangle7position;
    private Vector3 g5triangle8position;
    private Vector3 g5triangle9position;
    private Vector3 g5triangle10position;


    public Vector3 g5textHome;
    public GameObject g5text;
    #endregion

    //Group 06 Variables
    #region
    public GameObject group6;
    public GameObject g6triangle1;
    public GameObject g6triangle2;
    public GameObject g6triangle3;
    public GameObject g6triangle4;
    public GameObject g6triangle5;
    public GameObject g6triangle6;
    public GameObject g6triangle7;
    public GameObject g6triangle8;
    public GameObject g6triangle9;
    public GameObject g6triangle10;
    public GameObject g6triangle11;
    public GameObject g6triangle12;

    private Vector3 g6triangle1position;
    private Vector3 g6triangle2position;
    private Vector3 g6triangle3position;
    private Vector3 g6triangle4position;
    private Vector3 g6triangle5position;
    private Vector3 g6triangle6position;
    private Vector3 g6triangle7position;
    private Vector3 g6triangle8position;
    private Vector3 g6triangle9position;
    private Vector3 g6triangle10position;
    private Vector3 g6triangle11position;
    private Vector3 g6triangle12position;

    public Vector3 g6textHome;
    public GameObject g6text;
    #endregion

    //Group 07 Variables
    #region
    public GameObject group7;
    public GameObject g7triangle1;
    public GameObject g7triangle2;
    public GameObject g7triangle3;
    public GameObject g7triangle4;
    public GameObject g7triangle5;
    //public GameObject g7triangle6;
    //public GameObject g7triangle7;
    //public GameObject g7triangle8;
    //public GameObject g7triangle9;
    //public GameObject g7triangle10;

    private Vector3 g7triangle1position;
    private Vector3 g7triangle2position;
    private Vector3 g7triangle3position;
    private Vector3 g7triangle4position;
    private Vector3 g7triangle5position;
    //private Vector3 g7triangle6position;
    //private Vector3 g7triangle7position;
    //private Vector3 g7triangle8position;
    
    public Vector3 g7textHome;
    public GameObject g7text;
    #endregion

    //Group 08 Variables
    #region
    public GameObject group8;
    public GameObject g8triangle1;
    public GameObject g8triangle2;
    public GameObject g8triangle3;
    public GameObject g8triangle4;
    public GameObject g8triangle5;
    public GameObject g8triangle6;
    public GameObject g8triangle7;
    //public GameObject g8triangle8;
    //public GameObject g8triangle9;
    //public GameObject g8triangle10;
    
    private Vector3 g8triangle1position;
    private Vector3 g8triangle2position;
    private Vector3 g8triangle3position;
    private Vector3 g8triangle4position;
    private Vector3 g8triangle5position;
    private Vector3 g8triangle6position;
    private Vector3 g8triangle7position;
    //private Vector3 g8triangle8position;

    public Vector3 g8textHome;
    public GameObject g8text;
    #endregion

    //Clone Variables
    #region
    public GameObject ClonedContent01;
    public GameObject ClonedContent02;
    public GameObject ClonedContent03;
    public GameObject ClonedContent04;
    public GameObject ClonedContent05;
    public GameObject ClonedContent06;
    public GameObject ClonedContent07;
    public GameObject ClonedContent08;
    #endregion

    //Camera Variables
    #region
    public GameObject mainCamera;
    private Vector3 camHome;
    private Vector3 camHomeRotation;

    #endregion

    //Button Variables
    #region
    public GameObject group01Button;
    public GameObject group02Button;
    public GameObject group03Button;
    public GameObject group04Button;
    public GameObject group05Button;
    public GameObject group06Button;
    public GameObject group07Button;
    public GameObject group08Button;
    #endregion

    public Material disableMaterial;
    public Material activeMaterial;




    //Set Up Variables at Start
    void Start ()
    {
        GetTrianglePosition();
        GetTextPostion();
        camHome = new Vector3(0f, 0f, -103f);
        camHomeRotation = new Vector3(0, 0, 0);
        
    }

    void Update()
    {
        Debug.Log(g1triangle1position);
        Debug.Log(g1triangle1.transform.position);
    }





    //Button Calls
    #region
    public void Home()
    {
        LeanTween.move(mainCamera, camHome, 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(mainCamera, new Vector3(0f, 0f, 0f), 1f).setEase(LeanTweenType.easeInOutQuint);

        ResetTextPSR();
        ResetTriangles();
        ResetMaterial();

        StartCoroutine(WaitAndDestroyObject());
    }

    public IEnumerator WaitAndDestroyObject()
    {
        yield return new WaitForSeconds(.5f); 
        DestroyObject(ContentCloner.currentClonedContent);
    }

    public void AnimateButton01()
    {
        if (Timer._screenSaverActive == false)
        {
            HideOtherText();

            LeanTween.move(mainCamera, new Vector3(-66f, 41f, -35f), 1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(mainCamera, new Vector3(0f, 0f, -26.7f), 1f).setEase(LeanTweenType.easeInOutQuint);


            LeanTween.moveLocalX(g1text, -31f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalY(g1text, 4.5f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g1text, -51f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(g1text, new Vector3(0, 180, 26.5f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.scale(g1text, new Vector3(1f, 1f, 1f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.moveLocalZ(g1triangle1, -50f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g1triangle2, -33.75f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g1triangle3, -50f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g1triangle4, -33.75f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g1triangle5, -50f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g1triangle6, -33.75f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g1triangle7, -50f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g1triangle8, -33.75f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g1triangle9, -50f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g1triangle10, -33.75f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);

            g1triangle1.GetComponentInChildren<Renderer>().material = activeMaterial;
            g1triangle2.GetComponentInChildren<Renderer>().material = activeMaterial;
            g1triangle3.GetComponentInChildren<Renderer>().material = activeMaterial;
            g1triangle4.GetComponentInChildren<Renderer>().material = activeMaterial;
            g1triangle5.GetComponentInChildren<Renderer>().material = activeMaterial;
            g1triangle6.GetComponentInChildren<Renderer>().material = activeMaterial;
            g1triangle7.GetComponentInChildren<Renderer>().material = activeMaterial;
            g1triangle8.GetComponentInChildren<Renderer>().material = activeMaterial;
            g1triangle9.GetComponentInChildren<Renderer>().material = activeMaterial;
            g1triangle10.GetComponentInChildren<Renderer>().material = activeMaterial;
        }
       

        

    }

    public void AnimateButton02()
    {
        if (Timer._screenSaverActive == false)
        {
            HideOtherText();

            LeanTween.move(mainCamera, new Vector3(-1f, 13.5f, -35f), 1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(mainCamera, new Vector3(0f, 0f, 26.7f), 1f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.moveLocalX(g2text, -0.02f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalY(g2text, 6.26f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g2text, -44.58f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(g2text, new Vector3(0, 180, -26.5f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.scale(g2text, new Vector3(.01f, .01f, .01f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.moveLocalZ(g2triangle1, -28.8f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g2triangle2, -44f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g2triangle3, -28.8f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g2triangle4, -44f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g2triangle5, -28f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g2triangle6, -44f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            //LeanTween.moveLocalZ(g2triangle7, -50f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            //LeanTween.moveLocalZ(g2triangle8, -27f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            //LeanTween.moveLocalZ(g2triangle9, -50f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            //LeanTween.moveLocalZ(g2triangle10, -27f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);

            g2triangle1.GetComponentInChildren<Renderer>().material = activeMaterial;
            g2triangle2.GetComponentInChildren<Renderer>().material = activeMaterial;
            g2triangle3.GetComponentInChildren<Renderer>().material = activeMaterial;
            g2triangle4.GetComponentInChildren<Renderer>().material = activeMaterial;
            g2triangle5.GetComponentInChildren<Renderer>().material = activeMaterial;
            g2triangle6.GetComponentInChildren<Renderer>().material = activeMaterial;
            //g2triangle7.GetComponentInChildren<Renderer>().material = activeMaterial;
            //g2triangle8.GetComponentInChildren<Renderer>().material = activeMaterial;
            //g2triangle9.GetComponentInChildren<Renderer>().material = activeMaterial;
            //g2triangle10.GetComponentInChildren<Renderer>().material = activeMaterial;
        }

    }

    public void AnimateButton03()
    {
        if (Timer._screenSaverActive == false)
        {

            HideOtherText();

            LeanTween.move(mainCamera, new Vector3(48f, 41f, -35f), 1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(mainCamera, new Vector3(0f, 0f, -26.7f), 1f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.moveLocalX(g3text, 1.9f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalY(g3text, 27.8f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g3text, -43.7f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(g3text, new Vector3(0, 180, 26.5f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.scale(g3text, new Vector3(.01f, .01f, .01f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.moveLocalZ(g3triangle1, -43f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g3triangle2, -27f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g3triangle3, -43f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g3triangle4, -43f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g3triangle5, -27f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g3triangle6, -27f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            // LeanTween.moveLocalZ(g3triangle7, -50f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            // LeanTween.moveLocalZ(g3triangle8, -27f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            // LeanTween.moveLocalZ(g3triangle9, -50f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            // LeanTween.moveLocalZ(g3triangle10, -27f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);

            g3triangle1.GetComponentInChildren<Renderer>().material = activeMaterial;
            g3triangle2.GetComponentInChildren<Renderer>().material = activeMaterial;
            g3triangle3.GetComponentInChildren<Renderer>().material = activeMaterial;
            g3triangle4.GetComponentInChildren<Renderer>().material = activeMaterial;
            g3triangle5.GetComponentInChildren<Renderer>().material = activeMaterial;
            g3triangle6.GetComponentInChildren<Renderer>().material = activeMaterial;
            //g3triangle7.GetComponentInChildren<Renderer>().material = activeMaterial;
            //g3triangle8.GetComponentInChildren<Renderer>().material = activeMaterial;
            //g3triangle9.GetComponentInChildren<Renderer>().material = activeMaterial;
            //g3triangle10.GetComponentInChildren<Renderer>().material = activeMaterial;
        }

    }

    public void AnimateButton04()
    {
        if (Timer._screenSaverActive == false)
        {
            HideOtherText();

            LeanTween.move(mainCamera, new Vector3(72f, 13.5f, -35f), 1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(mainCamera, new Vector3(0f, 0f, 26.7f), 1f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.moveLocalX(g4text, 21.6f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalY(g4text, -10.1f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g4text, -45.4f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(g4text, new Vector3(0, 180, -26.5f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.scale(g4text, new Vector3(1f, 1f, 1f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.moveLocalZ(g4triangle1, -45f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g4triangle2, -28.8f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g4triangle3, -28.8f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g4triangle4, -45f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g4triangle5, -28.8f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g4triangle6, -28.8f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g4triangle7, -45f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            //LeanTween.moveLocalZ(g4triangle8, -27f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            //LeanTween.moveLocalZ(g4triangle9, -45f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            //LeanTween.moveLocalZ(g4triangle10, -27f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);

            g4triangle1.GetComponentInChildren<Renderer>().material = activeMaterial;
            g4triangle2.GetComponentInChildren<Renderer>().material = activeMaterial;
            g4triangle3.GetComponentInChildren<Renderer>().material = activeMaterial;
            g4triangle4.GetComponentInChildren<Renderer>().material = activeMaterial;
            g4triangle5.GetComponentInChildren<Renderer>().material = activeMaterial;
            g4triangle6.GetComponentInChildren<Renderer>().material = activeMaterial;
            g4triangle7.GetComponentInChildren<Renderer>().material = activeMaterial;
            //g4triangle8.GetComponentInChildren<Renderer>().material = activeMaterial;
            //g4triangle9.GetComponentInChildren<Renderer>().material = activeMaterial;
            //g4triangle10.GetComponentInChildren<Renderer>().material = activeMaterial;
        }
        
    }

    public void AnimateButton05()
    {
        if (Timer._screenSaverActive == false)
        {
            HideOtherText();

            LeanTween.move(mainCamera, new Vector3(-70f, -13f, -35f), 1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(mainCamera, new Vector3(0f, 0f, -26.7f), 1f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.moveLocalX(g5text, -34.7f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalY(g5text, -4.2f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g5text, -45.7f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(g5text, new Vector3(0, 180, 26.5f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.scale(g5text, new Vector3(.01f, .01f, .01f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.moveLocalZ(g5triangle1, -45f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g5triangle2, -28.8f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g5triangle3, -45f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g5triangle4, -28.8f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g5triangle5, -28.8f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g5triangle6, -45f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g5triangle7, -28.8f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g5triangle8, -45f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g5triangle9, -45f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g5triangle10, -28.8f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);

            g5triangle1.GetComponentInChildren<Renderer>().material = activeMaterial;
            g5triangle2.GetComponentInChildren<Renderer>().material = activeMaterial;
            g5triangle3.GetComponentInChildren<Renderer>().material = activeMaterial;
            g5triangle4.GetComponentInChildren<Renderer>().material = activeMaterial;
            g5triangle5.GetComponentInChildren<Renderer>().material = activeMaterial;
            g5triangle6.GetComponentInChildren<Renderer>().material = activeMaterial;
            g5triangle7.GetComponentInChildren<Renderer>().material = activeMaterial;
            g5triangle8.GetComponentInChildren<Renderer>().material = activeMaterial;
            g5triangle9.GetComponentInChildren<Renderer>().material = activeMaterial;
            g5triangle10.GetComponentInChildren<Renderer>().material = activeMaterial;
        }
        
    }

    public void AnimateButton06()
    {
        if (Timer._screenSaverActive == false)
        {
            HideOtherText();

            LeanTween.move(mainCamera, new Vector3(-14.3f, -37.6f, -35f), 1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(mainCamera, new Vector3(0f, 0f, 26.7f), 1f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.moveLocalX(g6text, -15.9f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalY(g6text, 9.7f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g6text, -45.4f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(g6text, new Vector3(0, 180, -26.5f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.scale(g6text, new Vector3(.01f, .01f, .01f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.moveLocalZ(g6triangle1, -28.8f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g6triangle2, -45f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g6triangle3, -28.8f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g6triangle4, -28.8f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g6triangle5, -45f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g6triangle6, -28.8f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g6triangle7, -45f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g6triangle8, -45f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g6triangle9, -28.8f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g6triangle10, -45f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g6triangle11, -28.8f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g6triangle12, -28.8f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);

            g6triangle1.GetComponentInChildren<Renderer>().material = activeMaterial;
            g6triangle2.GetComponentInChildren<Renderer>().material = activeMaterial;
            g6triangle3.GetComponentInChildren<Renderer>().material = activeMaterial;
            g6triangle4.GetComponentInChildren<Renderer>().material = activeMaterial;
            g6triangle5.GetComponentInChildren<Renderer>().material = activeMaterial;
            g6triangle6.GetComponentInChildren<Renderer>().material = activeMaterial;
            g6triangle7.GetComponentInChildren<Renderer>().material = activeMaterial;
            g6triangle8.GetComponentInChildren<Renderer>().material = activeMaterial;
            g6triangle9.GetComponentInChildren<Renderer>().material = activeMaterial;
            g6triangle10.GetComponentInChildren<Renderer>().material = activeMaterial;
            g6triangle11.GetComponentInChildren<Renderer>().material = activeMaterial;
            g6triangle12.GetComponentInChildren<Renderer>().material = activeMaterial;
        }

    }

    public void AnimateButton07()
    {
        if (Timer._screenSaverActive == false)
        {
            HideOtherText();

            LeanTween.move(mainCamera, new Vector3(33.5f, -21.1f, -27.3f), 1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(mainCamera, new Vector3(0f, 0f, -26.7f), 1f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.moveLocalX(g7text, 37.6f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalY(g7text, 27.08f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g7text, -45.4f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(g7text, new Vector3(0, 180, 26.5f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.scale(g7text, new Vector3(.008f, .008f, .008f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.moveLocalZ(g7triangle1, -28.8f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g7triangle2, -45f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g7triangle3, -28.8f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g7triangle4, -45f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g7triangle5, -28.8f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            //LeanTween.moveLocalZ(g7triangle6, -28.8f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            //LeanTween.moveLocalZ(g7triangle7, -45f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            //LeanTween.moveLocalZ(g7triangle8, -28.8f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            //LeanTween.moveLocalZ(g7triangle9, -45f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            //LeanTween.moveLocalZ(g7triangle10, -28.8f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);

            g7triangle1.GetComponentInChildren<Renderer>().material = activeMaterial;
            g7triangle2.GetComponentInChildren<Renderer>().material = activeMaterial;
            g7triangle3.GetComponentInChildren<Renderer>().material = activeMaterial;
            g7triangle4.GetComponentInChildren<Renderer>().material = activeMaterial;
            g7triangle5.GetComponentInChildren<Renderer>().material = activeMaterial;
            // g7triangle6.GetComponentInChildren<Renderer>().material = activeMaterial;
            // g7triangle7.GetComponentInChildren<Renderer>().material = activeMaterial;
            // g7triangle8.GetComponentInChildren<Renderer>().material = activeMaterial;
            // g7triangle9.GetComponentInChildren<Renderer>().material = activeMaterial;
            // g7triangle10.GetComponentInChildren<Renderer>().material = activeMaterial;
        }

    }

    public void AnimateButton08()
    {
        if (Timer._screenSaverActive == false)
        {
            HideOtherText();

            LeanTween.move(mainCamera, new Vector3(74f, -44.9f, -30.2f), 1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(mainCamera, new Vector3(0f, 0f, 26.7f), 1f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.moveLocalX(g8text, -14.4f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalY(g8text, -2f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g8text, -45f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(g8text, new Vector3(0, 180, -26.5f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.scale(g8text, new Vector3(1f, 1f, 1f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.moveLocalZ(g8triangle1, -28.8f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g8triangle2, -28.8f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g8triangle3, -45f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g8triangle4, -28.8f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g8triangle5, -45f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g8triangle6, -28.8f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.moveLocalZ(g8triangle7, -45f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            //LeanTween.moveLocalZ(g8triangle8, -28.8f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            //LeanTween.moveLocalZ(g8triangle9, -45f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            //LeanTween.moveLocalZ(g8triangle10, -28.8f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);

            g8triangle1.GetComponentInChildren<Renderer>().material = activeMaterial;
            g8triangle2.GetComponentInChildren<Renderer>().material = activeMaterial;
            g8triangle3.GetComponentInChildren<Renderer>().material = activeMaterial;
            g8triangle4.GetComponentInChildren<Renderer>().material = activeMaterial;
            g8triangle5.GetComponentInChildren<Renderer>().material = activeMaterial;
            g8triangle6.GetComponentInChildren<Renderer>().material = activeMaterial;
            g8triangle7.GetComponentInChildren<Renderer>().material = activeMaterial;
            //g8triangle8.GetComponentInChildren<Renderer>().material = activeMaterial;
            //g8triangle9.GetComponentInChildren<Renderer>().material = activeMaterial;
            //g8triangle10.GetComponentInChildren<Renderer>().material = activeMaterial;
        }
        
    }
    #endregion

 
    //Reset Scripts
    #region
    public void GetTrianglePosition()
    {
        g1triangle1position = g1triangle1.transform.position;
        g1triangle2position = g1triangle2.transform.position;
        g1triangle3position = g1triangle3.transform.position;
        g1triangle4position = g1triangle4.transform.position;
        g1triangle5position = g1triangle5.transform.position;
        g1triangle6position = g1triangle6.transform.position;
        g1triangle7position = g1triangle7.transform.position;
        g1triangle8position = g1triangle8.transform.position;
        g1triangle9position = g1triangle9.transform.position;
        g1triangle10position = g1triangle10.transform.position;

        g2triangle1position = g2triangle1.transform.position;
        g2triangle2position = g2triangle2.transform.position;
        g2triangle3position = g2triangle3.transform.position;
        g2triangle4position = g2triangle4.transform.position;
        g2triangle5position = g2triangle5.transform.position;
        g2triangle6position = g2triangle6.transform.position;
        //g2triangle7position = g2triangle7.transform.position;
        //g2triangle8position = g2triangle8.transform.position;
        
        g3triangle1position = g3triangle1.transform.position;
        g3triangle2position = g3triangle2.transform.position;
        g3triangle3position = g3triangle3.transform.position;
        g3triangle4position = g3triangle4.transform.position;
        g3triangle5position = g3triangle5.transform.position;
        g3triangle6position = g3triangle6.transform.position;
        //g3triangle7position = g3triangle7.transform.position;
        //g3triangle8position = g3triangle8.transform.position;

        g4triangle1position = g4triangle1.transform.position;
        g4triangle2position = g4triangle2.transform.position;
        g4triangle3position = g4triangle3.transform.position;
        g4triangle4position = g4triangle4.transform.position;
        g4triangle5position = g4triangle5.transform.position;
        g4triangle6position = g4triangle6.transform.position;
        g4triangle7position = g4triangle7.transform.position;
        //g4triangle8position = g4triangle8.transform.position;

        g5triangle1position = g5triangle1.transform.position;
        g5triangle2position = g5triangle2.transform.position;
        g5triangle3position = g5triangle3.transform.position;
        g5triangle4position = g5triangle4.transform.position;
        g5triangle5position = g5triangle5.transform.position;
        g5triangle6position = g5triangle6.transform.position;
        g5triangle7position = g5triangle7.transform.position;
        g5triangle8position = g5triangle8.transform.position;
        g5triangle9position = g5triangle9.transform.position;
        g5triangle10position = g5triangle10.transform.position;

        g6triangle1position = g6triangle1.transform.position;
        g6triangle2position = g6triangle2.transform.position;
        g6triangle3position = g6triangle3.transform.position;
        g6triangle4position = g6triangle4.transform.position;
        g6triangle5position = g6triangle5.transform.position;
        g6triangle6position = g6triangle6.transform.position;
        g6triangle7position = g6triangle7.transform.position;
        g6triangle8position = g6triangle8.transform.position;
        g6triangle9position = g6triangle9.transform.position;
        g6triangle10position = g6triangle10.transform.position;
        g6triangle11position = g6triangle11.transform.position;
        g6triangle12position = g6triangle12.transform.position;

        g7triangle1position = g7triangle1.transform.position;
        g7triangle2position = g7triangle2.transform.position;
        g7triangle3position = g7triangle3.transform.position;
        g7triangle4position = g7triangle4.transform.position;
        g7triangle5position = g7triangle5.transform.position;
        //g7triangle6position = g7triangle6.transform.position;
        //g7triangle7position = g7triangle7.transform.position;
        //g7triangle8position = g7triangle8.transform.position;

        g8triangle1position = g8triangle1.transform.position;
        g8triangle2position = g8triangle2.transform.position;
        g8triangle3position = g8triangle3.transform.position;
        g8triangle4position = g8triangle4.transform.position;
        g8triangle5position = g8triangle5.transform.position;
        g8triangle6position = g8triangle6.transform.position;
        g8triangle7position = g8triangle7.transform.position;
        //g8triangle8position = g8triangle8.transform.position;




    }

    public void ResetTriangles()
    {

        LeanTween.move(g1triangle1, g1triangle1position, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g1triangle2, g1triangle2position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g1triangle3, g1triangle3position, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g1triangle4, g1triangle4position, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g1triangle5, g1triangle5position, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g1triangle6, g1triangle6position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g1triangle7, g1triangle7position, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g1triangle8, g1triangle8position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g1triangle9, g1triangle9position, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g1triangle10, g1triangle10position, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.move(g2triangle1, g2triangle1position, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g2triangle3, g2triangle3position, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g2triangle4, g2triangle4position, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g2triangle2, g2triangle2position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g2triangle5, g2triangle5position, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g2triangle6, g2triangle6position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        

        LeanTween.move(g3triangle1, g3triangle1position, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g3triangle2, g3triangle2position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g3triangle3, g3triangle3position, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g3triangle4, g3triangle4position, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g3triangle5, g3triangle5position, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g3triangle6, g3triangle6position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        //LeanTween.moveLocalZ(g3triangle7, g3triangle7position.z, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        //LeanTween.moveLocalZ(g3triangle8, g3triangle8position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        //LeanTween.moveLocalZ(g3triangle9, g3triangle9position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        //LeanTween.moveLocalZ(g3triangle10, g3triangle10position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.move(g4triangle1, g4triangle1position, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g4triangle2, g4triangle2position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g4triangle3, g4triangle3position, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g4triangle4, g4triangle4position, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g4triangle5, g4triangle5position, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g4triangle6, g4triangle6position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g4triangle7, g4triangle7position, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        //LeanTween.moveLocalZ(g4triangle8, g4triangle8position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        //LeanTween.moveLocalZ(g4triangle9, g4triangle9position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        //LeanTween.moveLocalZ(g4triangle10, g4triangle10position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.move(g5triangle1, g5triangle1position, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g5triangle2, g5triangle2position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g5triangle3, g5triangle3position, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g5triangle4, g5triangle4position, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g5triangle5, g5triangle5position, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g5triangle6, g5triangle6position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g5triangle7, g5triangle7position, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g5triangle8, g5triangle8position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g5triangle9, g5triangle9position, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g5triangle10, g5triangle10position, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.move(g6triangle1, g6triangle1position, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g6triangle2, g6triangle2position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g6triangle3, g6triangle3position, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g6triangle4, g6triangle4position, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g6triangle5, g6triangle5position, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g6triangle6, g6triangle6position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g6triangle7, g6triangle7position, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g6triangle8, g6triangle8position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g6triangle9, g6triangle9position, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g6triangle10, g6triangle10position, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g6triangle11, g6triangle11position, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g6triangle12, g6triangle12position, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.move(g7triangle1, g7triangle1position, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g7triangle2, g7triangle2position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g7triangle3, g7triangle3position, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g7triangle4, g7triangle4position, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g7triangle5, g7triangle5position, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        //LeanTween.moveLocalZ(g7triangle6, g7triangle6position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        //LeanTween.moveLocalZ(g7triangle7, g7triangle7position.z, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        //LeanTween.moveLocalZ(g7triangle8, g7triangle8position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        //LeanTween.moveLocalZ(g7triangle9, g7triangle9position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        //LeanTween.moveLocalZ(g7triangle10, g7triangle10position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.move(g8triangle1, g8triangle1position, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g8triangle2, g8triangle2position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g8triangle3, g8triangle3position, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g8triangle4, g8triangle4position, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g8triangle5, g8triangle5position, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g8triangle6, g8triangle6position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(g8triangle7, g8triangle7position, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        //LeanTween.moveLocalZ(g8triangle8, g8triangle8position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        //LeanTween.moveLocalZ(g8triangle9, g8triangle9position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        //LeanTween.moveLocalZ(g8triangle10, g8triangle10position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);

    }

    //public void ResetTriangles()
    //{
    //    //LeanTween.moveLocalZ(g1text, -51f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g1triangle1, -36.9f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g1triangle2, -20f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g1triangle3, -36.6f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g1triangle4, -19.2f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g1triangle5, -36f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g1triangle6, -20f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g1triangle7, -34.1f, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g1triangle8, -20f, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g1triangle9, -36f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g1triangle10, -19.4f, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //
    //    LeanTween.move(g1triangle1, g1triangle1position, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.move(g1triangle2, g1triangle2position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.move(g1triangle3, g1triangle3position, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.move(g1triangle4, g1triangle4position, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.move(g1triangle5, g1triangle5position, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.move(g1triangle6, g1triangle6position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.move(g1triangle7, g1triangle7position, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.move(g1triangle8, g1triangle8position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.move(g1triangle9, g1triangle9position, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.move(g1triangle10, g1triangle10position, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //
    //    LeanTween.move(g2triangle1, g2triangle1position, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.move(g2triangle3, g2triangle3position, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.move(g2triangle4, g2triangle4position, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.move(g2triangle2, g2triangle2position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.move(g2triangle5, g2triangle5position, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.move(g2triangle6, g2triangle6position, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g2triangle7, g2triangle7position.z, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g2triangle8, g2triangle8position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g2triangle9, g2triangle9position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g2triangle10, g2triangle10position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //
    //    //LeanTween.moveLocalZ(g2triangle1, g2triangle1position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g2triangle2, g2triangle2position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g2triangle3, g2triangle3position.z, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g2triangle4, g2triangle4position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g2triangle5, g2triangle5position.z, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g2triangle6, g2triangle6position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //
    //    LeanTween.moveLocalZ(g3triangle1, g3triangle1position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g3triangle2, g3triangle2position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g3triangle3, g3triangle3position.z, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g3triangle4, g3triangle4position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g3triangle5, g3triangle5position.z, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g3triangle6, g3triangle6position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g3triangle7, g3triangle7position.z, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g3triangle8, g3triangle8position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g3triangle9, g3triangle9position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g3triangle10, g3triangle10position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //
    //    LeanTween.moveLocalZ(g4triangle1, g4triangle1position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g4triangle2, g4triangle2position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g4triangle3, g4triangle3position.z, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g4triangle4, g4triangle4position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g4triangle5, g4triangle5position.z, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g4triangle6, g4triangle6position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g4triangle7, g4triangle7position.z, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g4triangle8, g4triangle8position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g4triangle9, g4triangle9position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g4triangle10, g4triangle10position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //
    //    LeanTween.moveLocalZ(g5triangle1, g5triangle1position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g5triangle2, g5triangle2position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g5triangle3, g5triangle3position.z, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g5triangle4, g5triangle4position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g5triangle5, g5triangle5position.z, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g5triangle6, g5triangle6position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g5triangle7, g5triangle7position.z, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g5triangle8, g5triangle8position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g5triangle9, g5triangle9position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g5triangle10, g5triangle10position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //
    //    LeanTween.moveLocalZ(g6triangle1, g6triangle1position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g6triangle2, g6triangle2position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g6triangle3, g6triangle3position.z, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g6triangle4, g6triangle4position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g6triangle5, g6triangle5position.z, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g6triangle6, g6triangle6position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g6triangle7, g6triangle7position.z, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g6triangle8, g6triangle8position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g6triangle9, g6triangle9position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g6triangle10, g6triangle10position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g6triangle11, g6triangle11position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g6triangle12, g6triangle12position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //
    //    LeanTween.moveLocalZ(g7triangle1, g7triangle1position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g7triangle2, g7triangle2position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g7triangle3, g7triangle3position.z, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g7triangle4, g7triangle4position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g7triangle5, g7triangle5position.z, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g7triangle6, g7triangle6position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g7triangle7, g7triangle7position.z, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g7triangle8, g7triangle8position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g7triangle9, g7triangle9position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g7triangle10, g7triangle10position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //
    //    LeanTween.moveLocalZ(g8triangle1, g8triangle1position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g8triangle2, g8triangle2position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g8triangle3, g8triangle3position.z, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g8triangle4, g8triangle4position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g8triangle5, g8triangle5position.z, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g8triangle6, g8triangle6position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    LeanTween.moveLocalZ(g8triangle7, g8triangle7position.z, 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g8triangle8, g8triangle8position.z, 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g8triangle9, g8triangle9position.z, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
    //    //LeanTween.moveLocalZ(g8triangle10, g8triangle10position.z, 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuint);
    //
    //}

    public void HideOtherText()
    {
        LeanTween.moveLocalZ(g1text, -150f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g2text, -150f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g3text, -150f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g4text, -150f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g5text, -150f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g6text, -150f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g7text, -150f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g8text, -150f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
    }

    public void GetTextPostion()
    {
        g1textHomePosition = g1text.transform.position;
        g1textHomeScale = g1text.transform.localScale;
        //g2textHome = g1text.transform.position;
        //g3textHome = g1text.transform.position;
        //g4textHome = g1text.transform.position;
        //g5textHome = g1text.transform.position;
        //g6textHome = g1text.transform.position;
        //g7textHome = g1text.transform.position;
        //g8textHome = g1text.transform.position;

       
    }

    public void ResetTextPSR()
    {
        LeanTween.moveLocalX(g1text, -20.9f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalY(g1text, -5.7f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g1text, -37.9f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(g1text, new Vector3(0, 180, 0f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(g1text, new Vector3(3f, 3f, 3f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.moveLocalX(g2text, 7.8f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalY(g2text, 7.1f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g2text, -32f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(g2text, new Vector3(0, 180, 0f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(g2text, new Vector3(.03f, .03f, .03f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.moveLocalX(g3text, 11.2f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalY(g3text, 16.2f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g3text, -32f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(g3text, new Vector3(0, 180, 0f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(g3text, new Vector3(.03f, .03f, .03f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.moveLocalX(g4text, 34.2f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalY(g4text, -11f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g4text, -31.4f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(g4text, new Vector3(0, 180, 0f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(g4text, new Vector3(3f, 3f, 3f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.moveLocalX(g5text, -30.4f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalY(g5text, -9.7f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g5text, -32f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(g5text, new Vector3(0, 180, 0f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(g5text, new Vector3(.03f, .03f, .03f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.moveLocalX(g6text, -22.1f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalY(g6text, 5.2f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g6text, -32f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(g6text, new Vector3(0, 180, 0f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(g6text, new Vector3(.03f, .03f, .03f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.moveLocalX(g7text, 39f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalY(g7text, 17.6f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g7text, -32f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(g7text, new Vector3(0, 180, 0f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(g7text, new Vector3(.03f, .03f, .03f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.moveLocalX(g8text, -8.2f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalY(g8text, -5.2f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g8text, -31f, 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(g8text, new Vector3(0, 180, 0f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(g8text, new Vector3(3f, 3f, 3f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
    }

    public void ReturnTextZ()
    {
        LeanTween.moveLocalZ(g1text, -30f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g2text, -30f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g3text, -30f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g4text, -30f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g5text, -30f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g6text, -30f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g7text, -30f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocalZ(g8text, -30f, 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuint);
    }

    public void ResetMaterial()
    {
       g1triangle1.GetComponentInChildren<Renderer>().material = disableMaterial;
       g1triangle2.GetComponentInChildren<Renderer>().material = disableMaterial;
       g1triangle3.GetComponentInChildren<Renderer>().material = disableMaterial;
       g1triangle4.GetComponentInChildren<Renderer>().material = disableMaterial;
       g1triangle5.GetComponentInChildren<Renderer>().material = disableMaterial;
       g1triangle6.GetComponentInChildren<Renderer>().material = disableMaterial;
       g1triangle7.GetComponentInChildren<Renderer>().material = disableMaterial;
       g1triangle8.GetComponentInChildren<Renderer>().material = disableMaterial;
       g1triangle9.GetComponentInChildren<Renderer>().material = disableMaterial;
       g1triangle10.GetComponentInChildren<Renderer>().material = disableMaterial;

       g2triangle1.GetComponentInChildren<Renderer>().material = disableMaterial;
       g2triangle2.GetComponentInChildren<Renderer>().material = disableMaterial;
       g2triangle3.GetComponentInChildren<Renderer>().material = disableMaterial;
       g2triangle4.GetComponentInChildren<Renderer>().material = disableMaterial;
       g2triangle5.GetComponentInChildren<Renderer>().material = disableMaterial;
       g2triangle6.GetComponentInChildren<Renderer>().material = disableMaterial;
       //g2triangle7.GetComponentInChildren<Renderer>().material = disableMaterial;
       //g2triangle8.GetComponentInChildren<Renderer>().material = disableMaterial;
       //g2triangle9.GetComponentInChildren<Renderer>().material = disableMaterial;
       //g2triangle10.GetComponentInChildren<Renderer>().material = disableMaterial;
       
       g3triangle1.GetComponentInChildren<Renderer>().material = disableMaterial;
       g3triangle2.GetComponentInChildren<Renderer>().material = disableMaterial;
       g3triangle3.GetComponentInChildren<Renderer>().material = disableMaterial;
       g3triangle4.GetComponentInChildren<Renderer>().material = disableMaterial;
       g3triangle5.GetComponentInChildren<Renderer>().material = disableMaterial;
       g3triangle6.GetComponentInChildren<Renderer>().material = disableMaterial;
       // g3triangle7.GetComponentInChildren<Renderer>().material = disableMaterial;
       // g3triangle8.GetComponentInChildren<Renderer>().material = disableMaterial;
       // g3triangle9.GetComponentInChildren<Renderer>().material = disableMaterial;
       // g3triangle10.GetComponentInChildren<Renderer>().material = disableMaterial;
       //
       g4triangle1.GetComponentInChildren<Renderer>().material = disableMaterial;
       g4triangle2.GetComponentInChildren<Renderer>().material = disableMaterial;
       g4triangle3.GetComponentInChildren<Renderer>().material = disableMaterial;
       g4triangle4.GetComponentInChildren<Renderer>().material = disableMaterial;
       g4triangle5.GetComponentInChildren<Renderer>().material = disableMaterial;
       g4triangle6.GetComponentInChildren<Renderer>().material = disableMaterial;
       g4triangle7.GetComponentInChildren<Renderer>().material = disableMaterial;
       //g4triangle8.GetComponentInChildren<Renderer>().material = disableMaterial;
       //g4triangle9.GetComponentInChildren<Renderer>().material = disableMaterial;
       //g4triangle10.GetComponentInChildren<Renderer>().material = disableMaterial;
       
       g5triangle1.GetComponentInChildren<Renderer>().material = disableMaterial;
       g5triangle2.GetComponentInChildren<Renderer>().material = disableMaterial;
       g5triangle3.GetComponentInChildren<Renderer>().material = disableMaterial;
       g5triangle4.GetComponentInChildren<Renderer>().material = disableMaterial;
       g5triangle5.GetComponentInChildren<Renderer>().material = disableMaterial;
       g5triangle6.GetComponentInChildren<Renderer>().material = disableMaterial;
       g5triangle7.GetComponentInChildren<Renderer>().material = disableMaterial;
       g5triangle8.GetComponentInChildren<Renderer>().material = disableMaterial;
       g5triangle9.GetComponentInChildren<Renderer>().material = disableMaterial;
       g5triangle10.GetComponentInChildren<Renderer>().material = disableMaterial;

       g6triangle1.GetComponentInChildren<Renderer>().material = disableMaterial;
       g6triangle2.GetComponentInChildren<Renderer>().material = disableMaterial;
       g6triangle3.GetComponentInChildren<Renderer>().material = disableMaterial;
       g6triangle4.GetComponentInChildren<Renderer>().material = disableMaterial;
       g6triangle5.GetComponentInChildren<Renderer>().material = disableMaterial;
       g6triangle6.GetComponentInChildren<Renderer>().material = disableMaterial;
       g6triangle7.GetComponentInChildren<Renderer>().material = disableMaterial;
       g6triangle8.GetComponentInChildren<Renderer>().material = disableMaterial;
       g6triangle9.GetComponentInChildren<Renderer>().material = disableMaterial;
       g6triangle10.GetComponentInChildren<Renderer>().material = disableMaterial;
       g6triangle11.GetComponentInChildren<Renderer>().material = disableMaterial;
       g6triangle12.GetComponentInChildren<Renderer>().material = disableMaterial;

       g7triangle1.GetComponentInChildren<Renderer>().material = disableMaterial;
       g7triangle2.GetComponentInChildren<Renderer>().material = disableMaterial;
       g7triangle3.GetComponentInChildren<Renderer>().material = disableMaterial;
       g7triangle4.GetComponentInChildren<Renderer>().material = disableMaterial;
       g7triangle5.GetComponentInChildren<Renderer>().material = disableMaterial;
       //g7triangle6.GetComponentInChildren<Renderer>().material = disableMaterial;
       //g7triangle7.GetComponentInChildren<Renderer>().material = disableMaterial;
       //g7triangle8.GetComponentInChildren<Renderer>().material = disableMaterial;
       //g7triangle9.GetComponentInChildren<Renderer>().material = disableMaterial;
       //g7triangle10.GetComponentInChildren<Renderer>().material = disableMaterial;
       
       g8triangle1.GetComponentInChildren<Renderer>().material = disableMaterial;
       g8triangle2.GetComponentInChildren<Renderer>().material = disableMaterial;
       g8triangle3.GetComponentInChildren<Renderer>().material = disableMaterial;
       g8triangle4.GetComponentInChildren<Renderer>().material = disableMaterial;
       g8triangle5.GetComponentInChildren<Renderer>().material = disableMaterial;
       g8triangle6.GetComponentInChildren<Renderer>().material = disableMaterial;
       g8triangle7.GetComponentInChildren<Renderer>().material = disableMaterial;
       //g8triangle8.GetComponentInChildren<Renderer>().material = disableMaterial;
       //g8triangle9.GetComponentInChildren<Renderer>().material = disableMaterial;
       //g8triangle10.GetComponentInChildren<Renderer>().material = disableMaterial;


    }
    #endregion
}
