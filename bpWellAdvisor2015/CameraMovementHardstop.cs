﻿using UnityEngine;
using System.Collections;

public class CameraMovementHardstop : MonoBehaviour 
{
	public GameObject _movementDriver;

	void Update () 
	{
			this.transform.position = new Vector3(0f, (-1 * _movementDriver.transform.position.y), -30f);
	}
}
