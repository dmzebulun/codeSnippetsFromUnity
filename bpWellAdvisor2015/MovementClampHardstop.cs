﻿using UnityEngine;
using System.Collections;

public class MovementClampHardstop : MonoBehaviour 
{


	// Update is called once per frame
	void Update () 
	{
		this.transform.position = new Vector3(0f, (Mathf.Clamp(this.transform.position.y, -20f, 20f)), -25f);
	}
}
