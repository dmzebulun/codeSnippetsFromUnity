﻿using UnityEngine;
using System.Collections;

public class MovementClamp : MonoBehaviour 
{
	public bool _outOfBounds;
	public float _upperLimit;
	public float _lowerLimit;
	public Vector3 UpperLimitVector;
	public Vector3 LowerLimitVector;

	// Use this for initialization
	void Start () 
	{
		_outOfBounds = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(_outOfBounds == false)
		{
			this.transform.position = new Vector3(0f, (Mathf.Clamp(this.transform.position.y, _lowerLimit, _upperLimit)), -30f);
		}
		
		if(this.transform.position.y >= _upperLimit)
		{
			UpperLimit();
		}
		
		if(this.transform.position.y <= _lowerLimit)
		{
			LowerLimit();
		}
		
	}
	
	void UpperLimit()
	{
		_outOfBounds = true;
		LeanTween.moveLocal(this.gameObject, UpperLimitVector, .2f).setEase(LeanTweenType.easeInOutSine);
		StartCoroutine(ResetOutOfBounds());
	}
	
	void LowerLimit()
	{
		_outOfBounds = true;
		LeanTween.moveLocal(this.gameObject, LowerLimitVector, .2f).setEase(LeanTweenType.easeInOutSine);
		StartCoroutine(ResetOutOfBounds());
	}
	
	public IEnumerator ResetOutOfBounds()
	{
		yield return new WaitForSeconds(.3f);
		_outOfBounds = false;
	}
}
