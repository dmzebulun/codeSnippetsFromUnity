﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using AssetBundles;
using UnityEngine.UI;

public class AssetLoader : MonoBehaviour
{

    public string assetBundlesPath;
    public GameObject gameButtonPreFab;

    public List<string> gamesList;

    public GameObject gameButtonHolder;
    public List<Transform> gameButtonPositions;


    

    public AssetBundleLoadAssetOperation gameLogo;


    // Use this for initialization
    void Awake()
    {
        // Initializes Asset Bundle Manager Object
        StartCoroutine(Initialize());

        // Starts our setup methods for the lobby.
        GameButtonPositions();
        FindAssetBundles();
        

    }

 


    //Find all asset bundles in folder
    public void FindAssetBundles()
    {
        //Define the path to the "AssetBundles" folder. Should be under the root of the project folder.
        DirectoryInfo rootFolder = new DirectoryInfo(Directory.GetCurrentDirectory());
        assetBundlesPath = (rootFolder + "\\AssetBundles\\Windows");

        int gameButtonPostionIndex = 1;


        //Find each asset bundle in the assetBundles folder. 
        //We want to exclude the .manifest files and the windows and windows.manifest files from this count.
        foreach (string file in Directory.GetFiles(assetBundlesPath))
        {
            
            if (!file.Contains(".manifest") && (file.ToString() != (assetBundlesPath + "\\Windows")))
            {
                
                Debug.Log(file);
                //Add each game to the list of games so we can get a count. 
                gamesList.Add(file);

                //Create a button for each game
                //Grab the position and rotation of each premade button as we go down the list.
                Vector3 buttonPosition = gameButtonPositions[gameButtonPostionIndex].position;
                Quaternion buttonRotation = gameButtonPositions[gameButtonPostionIndex].rotation;

                //Clone a new button for the assetbundle
                GameObject button = Instantiate(gameButtonPreFab, buttonPosition, buttonRotation) as GameObject;

                //Move to the next button in the list.
                gameButtonPostionIndex += 1;

                //Get the asset bundle name from the path. There is probably a better way to do this. 
                //Documentation on the AssetBundle Manager is a little sparse since it's somewhat new
                string bundleName = file.Replace(assetBundlesPath + "\\", "");
                string thisBundle = bundleName;

                string logoName = "logo";
                GameObject gameButton = button;

                Debug.Log(bundleName);

                StartCoroutine(LoadLogo(thisBundle, logoName, gameButton));


            }
            else
            {
                //Debug.Log(file);

            }

            
            
        }
        Debug.Log(gamesList.Count);
        CreateButtonsForAsset();
    }

   


    //Create buttons for each asset bundle
    public void CreateButtonsForAsset()
    {
        //Using the Transform data from the gameButtonPositions this will determine how many and where each button goes.
        //We need to skip the first object in the array since it is the parent object and we only want the child.
        
       
        //How mnay buttons do we need to make?
        foreach (string game in gamesList)
        {
           


            // Set the texture for button to the game's logo.

            
           
            //Increase our index to move to the next button position.
            
        }


    }




    //Load asset bundle from button
    public void LoadAssetBundle()
    {

    }



    //Get GameButtonPostions into List so that we have adjustable points in game to map the game icons/buttons.
    public void GameButtonPositions()
    {
        Transform[] ts = gameButtonHolder.GetComponentsInChildren<Transform>();

        foreach(Transform child in ts)
        {
            gameButtonPositions.Add(child);
        }

    }

    // Initialize the downloading url and AssetBundleManifest object.
    protected IEnumerator Initialize()
    {
        
        // Don't destroy this gameObject as we depend on it to run the loading script.
        DontDestroyOnLoad(gameObject);

        // With this code, when in-editor or using a development builds: Always use the AssetBundle Server
        // (This is very dependent on the production workflow of the project. 
        // 	Another approach would be to make this configurable in the standalone player.)
        #if DEVELOPMENT_BUILD || UNITY_EDITOR
        AssetBundleManager.SetDevelopmentAssetBundleServer();

        #else
        //Original Location in script
        AssetBundleManager.SetDevelopmentAssetBundleServer ();
		// Use the following code if AssetBundles are embedded in the project for example via StreamingAssets folder etc:
		AssetBundleManager.SetSourceAssetBundleURL(Application.dataPath + "/");
		// Or customize the URL based on your deployment or configuration
		//AssetBundleManager.SetSourceAssetBundleURL("http://www.MyWebsite/MyAssetBundles");
        #endif

        // Initialize AssetBundleManifest which loads the AssetBundleManifest object.
        var request = AssetBundleManager.Initialize();
        if (request != null)
            yield return StartCoroutine(request);
    }

    //protected IEnumerator LoadLogo(string assetBundleName, string assetName, GameObject button)
    protected IEnumerator LoadLogo(string assetBundleName, string assetName, GameObject button)
    {
        Debug.Log("this is the coroutine " + assetBundleName);
        // This is simply to get the elapsed time for this phase of AssetLoading.
        float startTime = Time.realtimeSinceStartup;
        
        // Load asset from assetBundle.
        AssetBundleLoadAssetOperation request = AssetBundleManager.LoadAssetAsync(assetBundleName, assetName, typeof(Sprite));
        if (request == null)
            yield break;
        yield return StartCoroutine(request);

        // Get the asset.
        Sprite logo = request.GetAsset<Sprite>();
        Debug.Log(logo);

        if (logo != null)
        {
            
            button.GetComponent<Image>().sprite = logo;

        }




        // Calculate and display the elapsed time.
        float elapsedTime = Time.realtimeSinceStartup - startTime;
        Debug.Log(assetName + (logo == null ? " was not" : " was") + " loaded successfully in " + elapsedTime + " seconds");
    }
}
