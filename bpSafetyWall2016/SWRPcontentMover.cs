﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SWRPcontentMover : MonoBehaviour
{

    public int currentSlide;

    public GameObject contentMover;

    public float xCor01;
    public float xCor02;
    public float xCor03;
    public float xCor04;
    public float xCor05;
    public float xCor06;
    public float xCor07;

    
   

    public void MoveLeft()
    {

        if(currentSlide == 0)
        {
            LeanTween.moveLocalX(contentMover, xCor02, 1f).setEase(LeanTweenType.easeInOutQuad);

        }
        if (currentSlide == 1)
        {
            LeanTween.moveLocalX(contentMover, xCor03, 1f).setEase(LeanTweenType.easeInOutQuad);

        }
        if (currentSlide == 2)
        {
            LeanTween.moveLocalX(contentMover, xCor04, 1f).setEase(LeanTweenType.easeInOutQuad);

        }
        if (currentSlide == 3)
        {
            LeanTween.moveLocalX(contentMover, xCor05, 1f).setEase(LeanTweenType.easeInOutQuad);

        }
        //if (currentSlide == 4)
        //{
        //    LeanTween.moveLocalX(contentMover, xCor06, 1f).setEase(LeanTweenType.easeInOutQuad);
        //
        //}
        //if (currentSlide == 5)
        //{
        //    LeanTween.moveLocalX(contentMover, xCor07, 1f).setEase(LeanTweenType.easeInOutQuad);
        //
        //}
        

        if(currentSlide < 4)
        {
            currentSlide = currentSlide + 1;
        }
        

    }

    public void MoveRight()
    {

        //if (currentSlide == 6)
        //{
        //    LeanTween.moveLocalX(contentMover, xCor06, 1f).setEase(LeanTweenType.easeInOutQuad);
        //
        //}
        //if (currentSlide == 5)
        //{
        //    LeanTween.moveLocalX(contentMover, xCor05, 1f).setEase(LeanTweenType.easeInOutQuad);
        //
        //}
        if (currentSlide == 4)
        {
            LeanTween.moveLocalX(contentMover, xCor04, 1f).setEase(LeanTweenType.easeInOutQuad);

        }
        if (currentSlide == 3)
        {
            LeanTween.moveLocalX(contentMover, xCor03, 1f).setEase(LeanTweenType.easeInOutQuad);

        }
        if (currentSlide == 2)
        {
            LeanTween.moveLocalX(contentMover, xCor02, 1f).setEase(LeanTweenType.easeInOutQuad);

        }
        if (currentSlide == 1)
        {
            LeanTween.moveLocalX(contentMover, xCor01, 1f).setEase(LeanTweenType.easeInOutQuad);

        }
        

        if(currentSlide > 0)
        {
            currentSlide = currentSlide - 1;
        }
        
    }

    public void CloseContent()
    {
        DestroyObject(this.gameObject);
    }
	
}
