﻿using UnityEngine;
using System.Collections;

public class CloneVideo : MonoBehaviour 
{
	public GameObject _videoToClone;

	private GameObject _currentVideo;

	public static bool _videoPlaying;

    

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void VideoCloner()
	{
		if(_videoPlaying == false)
		{
			_currentVideo = Instantiate(_videoToClone) as GameObject;
			_videoPlaying = true;
			
		}
	}


}
