﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ContentCloner : MonoBehaviour 
{
	public static GameObject _currentClonedContent;
	public static float _currentAlpha;
	public Object _prefabDWH;
	public Object _prefabSD;
	public Object _prefabCTR;
	public Object _prefabPS;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(_currentClonedContent != null)
		{
			_currentClonedContent.GetComponentInChildren<CanvasGroup>().alpha = _currentAlpha;

		}
		Debug.Log (_currentClonedContent);
	}


	public void CloneDWH()
	{
		_currentClonedContent = Instantiate(_prefabDWH, new Vector3(0,8,0), Quaternion.Euler(0,0,0)) as GameObject;
		LeanTween.value(_currentClonedContent.gameObject, 0f, 1f, 1f).setOnUpdate((float alpha)=>{_currentAlpha = alpha;});

	}

	public void CloneSD()
	{
		if(_currentClonedContent == null)
		{
			_currentClonedContent = Instantiate(_prefabSD, new Vector3(0,8,0), Quaternion.Euler(0,0,0)) as GameObject;
			LeanTween.value(_currentClonedContent.gameObject, 0f, 1f, 1f).setOnUpdate((float alpha)=>{_currentAlpha = alpha;});
		}


	}

	public void CloneCTR(int Option)
	{
		if(_currentClonedContent == null)
		{

			_currentClonedContent = Instantiate(_prefabCTR, new Vector3(0,8,0), Quaternion.Euler(0,0,0)) as GameObject;
			if(Option == 1)
			{
				_currentClonedContent.GetComponentInChildren<Slider>().value = 837;
			}
			else 
			{
				_currentClonedContent.GetComponentInChildren<Slider>().value = 5721;
			}
			LeanTween.value(_currentClonedContent.gameObject, 0f, 1f, 1f).setOnUpdate((float alpha)=>{_currentAlpha = alpha;});
		}
	}

	public void ClonePS()
	{
		if(_currentClonedContent == null)
		{

		_currentClonedContent = Instantiate(_prefabPS, new Vector3(0,8,0), Quaternion.Euler(0,0,0)) as GameObject;
		LeanTween.value(_currentClonedContent.gameObject, 0f, 1f, 1f).setOnUpdate((float alpha)=>{_currentAlpha = alpha;});
		}
	}

    
}
