﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeFollow : MonoBehaviour 
{

	public CanvasGroup _groupToFollow;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		this.gameObject.GetComponent<CanvasGroup>().alpha = _groupToFollow.GetComponent<CanvasGroup>().alpha;
	}
}
