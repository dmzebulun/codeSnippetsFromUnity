﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuAnimation : MonoBehaviour 
{
	public GameObject _whiteLeft;
	public GameObject _deepWaterHorizon;
	public GameObject _saferDrilling;
	public GameObject _capabilityToRespond;
	public GameObject _capabilityToRespondSub01;
	public GameObject _capabilityToRespondSub02;
	public GameObject _processSafety;
	public GameObject _whiteRight;
	public GameObject _bottomNav;

	public GameObject _deepWaterHorizonCatButton;
	public GameObject _saferDrillingCatButton;
	public GameObject _capabilityToRespondCatButton;
	public GameObject _processSafetyCatButton;

	public GameObject _homeIcon;
	public GameObject _homeButtonText;

	private bool _atHome;


	void Start()
	{
		_atHome = true;
	}


	public void ResetMainMenu()
	{	
		Debug.Log ("Reseting Main Menu");
		Timer._timer =0;

		LeanTween.move(_whiteLeft,           new Vector3(-280f, 16f, -235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
		//LeanTween.move(_deepWaterHorizon,    new Vector3(-149f, 16f, -235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.move(_saferDrilling,       new Vector3(-149f, 16f, -235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.move(_capabilityToRespond, new Vector3(0f, 16f, -235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.move(_processSafety,       new Vector3(149f, 16f, -235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.move(_whiteRight,          new Vector3(280f, 16f, -235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);

		LeanTween.move(_capabilityToRespondSub01,           new Vector3(-99f, 257.5f, -259.6f), .5f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.move(_capabilityToRespondSub02,           new Vector3(98.5f,  -231f, -259.6f), .5f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);


		LeanTween.move(_bottomNav,           new Vector3(0,0,0), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);

		LeanTween.move(_deepWaterHorizonCatButton,    new Vector3(-267f, -110f, -290f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.move(_saferDrillingCatButton,       new Vector3(-267f, -110f, -290f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.move(_capabilityToRespondCatButton, new Vector3(-267f, -110f, -290f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.move(_processSafetyCatButton,       new Vector3(-267f, -110f, -290f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);


		_atHome = true;

	}

	public void MenuDeepWaterHorizon()
	{
		Timer._timer =0;
		Debug.Log ("DWH");

		if(_atHome == true)
		{
			LeanTween.move(_whiteLeft,           new Vector3(-497f,16f,-235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
			//LeanTween.move(_deepWaterHorizon,    new Vector3(-342f,16f,-235f), 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_saferDrilling,       new Vector3(-366f,16f,-235f), 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_capabilityToRespond, new Vector3(377f,16f,-235f), 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_processSafety,       new Vector3(526f,16f,-235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_whiteRight,          new Vector3(657f,16f,-235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);

			LeanTween.move(_deepWaterHorizonCatButton, new Vector3(-161f, -110f, -290f), 1f).setEase(LeanTweenType.easeInOutQuad);

				
			_atHome = false;

			//MenuBottomNav();
		}



	}

	public void MenuSaferDrilling()
	{
		Debug.Log ("SD");
		Timer._timer =0;

		if(_atHome == true)
		{
			LeanTween.move(_whiteLeft,           new Vector3(-497f,16f,-235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
			//LeanTween.move(_deepWaterHorizon,    new Vector3(-342f,16f,-235f), 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_saferDrilling,       new Vector3(-366f,16f,-235f), 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_capabilityToRespond, new Vector3(377f,16f,-235f), 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_processSafety,       new Vector3(526f,16f,-235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_whiteRight,          new Vector3(657f,16f,-235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);

			LeanTween.move(_capabilityToRespondSub01,           new Vector3(-99f, 257.5f, -259.6f), .5f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_capabilityToRespondSub02,           new Vector3(98.5f,  -231f, -259.6f), .5f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);


			LeanTween.move(_saferDrillingCatButton, new Vector3(-161f, -110f, -290f), 1f).setEase(LeanTweenType.easeInOutQuad);
			
			_atHome = false;

			//MenuBottomNav();
		}


	}

	public void MenuCapabilityToRespond()
	{
		Debug.Log ("CTR");
		Timer._timer =0;
		if(_atHome == true)
		{
			LeanTween.move(_capabilityToRespondSub01,           new Vector3(-43f, 120.2f, -259.6f), .5f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_capabilityToRespondSub02,           new Vector3(34.9f, -72.2f, -259.6f), .5f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);

		}


	}

	public void MenuCapabilityToRespondSub01()
	{
		Debug.Log ("CTR");
		
		if(_atHome == true)
		{

			LeanTween.move(_capabilityToRespondSub01,           new Vector3(-99f, 257.5f, -259.6f), .5f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_capabilityToRespondSub02,           new Vector3(98.5f,  -231f, -259.6f), .5f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);


			LeanTween.move(_whiteLeft,           new Vector3(-646f,16f,-235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
			//LeanTween.move(_deepWaterHorizon,    new Vector3(-569f,16f,-235f), 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_saferDrilling,       new Vector3(-515f,16f,-235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_capabilityToRespond, new Vector3(-366f,16f,-235f), 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_processSafety,       new Vector3(377f,16f,-235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_whiteRight,          new Vector3(509f,16f,-235f), 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuad);
			
			LeanTween.move(_capabilityToRespondCatButton, new Vector3(-161f, -110f, -290f), 1f).setEase(LeanTweenType.easeInOutQuad);
			
			
			_atHome = false;
			
			//MenuBottomNav();
		}
		
		
	}

	public void MenuCapabilityToRespondSub02()
	{
		Debug.Log ("CTR");
		
		if(_atHome == true)
		{

			LeanTween.move(_capabilityToRespondSub01,           new Vector3(-99f, 257.5f, -259.6f), .5f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_capabilityToRespondSub02,           new Vector3(98.5f,  -231f, -259.6f), .5f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);

			LeanTween.move(_whiteLeft,           new Vector3(-646f,16f,-235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
			//LeanTween.move(_deepWaterHorizon,    new Vector3(-569f,16f,-235f), 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_saferDrilling,       new Vector3(-515f,16f,-235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_capabilityToRespond, new Vector3(-366f,16f,-235f), 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_processSafety,       new Vector3(377f,16f,-235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_whiteRight,          new Vector3(509f,16f,-235f), 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuad);
			
			LeanTween.move(_capabilityToRespondCatButton, new Vector3(-161f, -110f, -290f), 1f).setEase(LeanTweenType.easeInOutQuad);
			
			
			_atHome = false;
			
			//MenuBottomNav();
		}
		
		
	}



	public void MenuProcessSafety()
	{
		Debug.Log ("PS");
		Timer._timer =0;
		if(_atHome == true)
		{
			LeanTween.move(_whiteLeft,           new Vector3(-796f,16f,-235f), 1f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
			//LeanTween.move(_deepWaterHorizon,    new Vector3(-685f,16f,-235f), 1f).setDelay(.1f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_saferDrilling,       new Vector3(-665f,16f,-235f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_capabilityToRespond, new Vector3(-516f,16f,-235f), 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_processSafety,       new Vector3(-367f,16f,-235f), 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_whiteRight,          new Vector3(356f,16f,-235f), 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuad);

			LeanTween.move(_capabilityToRespondSub01,           new Vector3(-99f, 257.5f, -259.6f), .5f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);
			LeanTween.move(_capabilityToRespondSub02,           new Vector3(98.5f,  -231f, -259.6f), .5f).setDelay(0f).setEase(LeanTweenType.easeInOutQuad);

			LeanTween.move(_processSafetyCatButton, new Vector3(-161f, -110f, -290f), 1f).setEase(LeanTweenType.easeInOutQuad);

			_atHome = false;

			//MenuBottomNav();
		}

	}

	public void MenuBottomNav()
	{
		LeanTween.move (_bottomNav, new Vector3(370f, 0, 0), 1f).setEase(LeanTweenType.easeInOutSine);
		//LeanTween.scale(_homeButtonText, new Vector3(.5f, .5f, .5f), 1f).setEase(LeanTweenType.easeInOutExpo);
		//LeanTween.scale(_homeIcon, new Vector3(2,2,2), 1f).setEase(LeanTweenType.easeInOutExpo);
	}
}
