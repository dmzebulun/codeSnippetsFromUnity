﻿ using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ContentMover : MonoBehaviour 
{
	public Slider _bottomSlider;
	public GameObject _content;
	public Vector3 _contentPosition;

	//private bool _page01 = true;
	//private bool _page02 = false;
	//private bool _page03 = false;
	//private bool _page04 = false;
	//private bool _page05 = false;
	//private bool _page06 = false;

	public float _page01Position;
	public float _page02Position;
	public float _page03Position;
	public float _page04Position;
	public float _page05Position;
	public float _page06Position;
	public float _page07Position;
	public float _page08Position;
	public float _page09Position;
	public float _page10Position;
	public float _page11Position;
    public float _page12Position;

    // Use this for initialization
    void Start () 
	{
		_content.transform.position = _contentPosition;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//_bottomSlider.value = _contentPosition.x;
		_content.transform.position = new Vector3((-1 * _bottomSlider.value), 0, 0);
	}

	public void NextPage()
	{
		if(Timer._timer != 0)
		{
			Timer._timer = 0;
		}
		Debug.Log ("Moving Forward");

        if (_bottomSlider.value >= _page11Position && _bottomSlider.value < (_page12Position))
        {

            LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page12Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue) => { _bottomSlider.value = sliderValue; });
        }

        if (_bottomSlider.value >= _page10Position && _bottomSlider.value < (_page11Position))
		{
			
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page11Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}

		if(_bottomSlider.value >= _page09Position && _bottomSlider.value < (_page10Position))
		{
			
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page10Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}

		if(_bottomSlider.value >= _page08Position && _bottomSlider.value < (_page09Position))
		{
			
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page09Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}
		if(_bottomSlider.value >= _page07Position && _bottomSlider.value < (_page08Position))
		{
			
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page08Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}
		if(_bottomSlider.value >= _page06Position && _bottomSlider.value < (_page07Position))
		{
			
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page07Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}
		if(_bottomSlider.value >= _page05Position && _bottomSlider.value < (_page06Position))
		{

			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page06Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}

		if(_bottomSlider.value >= _page04Position && _bottomSlider.value < (_page05Position))
		{
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page05Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}

		if(_bottomSlider.value >= _page03Position && _bottomSlider.value < (_page04Position))
		{
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page04Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}

		if(_bottomSlider.value >= _page02Position && _bottomSlider.value < (_page03Position))
		{
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page03Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}

		if(_bottomSlider.value >= _page01Position && _bottomSlider.value < (_page02Position))
		{
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page02Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}


	}

	public void PreviousPage()
	{
		if(Timer._timer != 0)
		{
			Timer._timer = 0;
		}
		Debug.Log("Moving Back");
		if(_bottomSlider.value > _page01Position && _bottomSlider.value <= _page02Position)
		{	
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page01Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}
		
		if(_bottomSlider.value > _page02Position && _bottomSlider.value <= _page03Position)
		{
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page02Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}
		
		if(_bottomSlider.value > _page03Position && _bottomSlider.value <= _page04Position)
		{
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page03Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}
		
		if(_bottomSlider.value > _page04Position && _bottomSlider.value <= _page05Position)
		{
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page04Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}
		
		if(_bottomSlider.value > _page05Position && _bottomSlider.value <= _page06Position)
		{
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page05Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}

		if(_bottomSlider.value > _page06Position && _bottomSlider.value <= _page07Position)
		{
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page06Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}

		if(_bottomSlider.value > _page07Position && _bottomSlider.value <= _page08Position)
		{
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page07Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}

		if(_bottomSlider.value > _page08Position && _bottomSlider.value <= _page09Position)
		{
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page08Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}

		if(_bottomSlider.value > _page09Position && _bottomSlider.value <= _page10Position)
		{
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page09Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}

		if(_bottomSlider.value > _page10Position && _bottomSlider.value <= _page11Position)
		{
			LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page10Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		}

        if (_bottomSlider.value > _page11Position && _bottomSlider.value <= _page12Position)
        {
            LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page11Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue) => { _bottomSlider.value = sliderValue; });
        }
    }

	public void SubMenuHome()
	{
		if(Timer._timer != 0)
		{
			Timer._timer = 0;
		}
		LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page01Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		
	}

	public void SubMenuButton01()
	{
		if(Timer._timer != 0)
		{
			Timer._timer = 0;
		}
		LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page02Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 

	}

	public void SubMenuButton02()
	{
		if(Timer._timer != 0)
		{
			Timer._timer = 0;
		}
		LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page03Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		
	}

	public void SubMenuButton03()
	{
		if(Timer._timer != 0)
		{
			Timer._timer = 0;
		}
		LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page04Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		
	}

	public void SubMenuButton04()
	{
		if(Timer._timer != 0)
		{
			Timer._timer = 0;
		}
		LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page05Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		
	}

	public void SubMenuButton05()
	{
		if(Timer._timer != 0)
		{
			Timer._timer = 0;
		}
		LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page06Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		
	}

	public void SubMenuButton06()
	{
		LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page07Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		
	}

	public void SubMenuButton07()
	{
		if(Timer._timer != 0)
		{
			Timer._timer = 0;
		}
		LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page08Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		
	}

	public void SubMenuButton08()
	{
		if(Timer._timer != 0)
		{
			Timer._timer = 0;
		}
		LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page09Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		
	}

	public void SubMenuButton09()
	{
		if(Timer._timer != 0)
		{
			Timer._timer = 0;
		}
		LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page10Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		
	}

	public void SubMenuButton10()
	{
		if(Timer._timer != 0)
		{
			Timer._timer = 0;
		}
		LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page11Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue)=>{_bottomSlider.value = sliderValue;}); 
		
	}

    public void SubMenuButton11()
    {
        if (Timer._timer != 0)
        {
            Timer._timer = 0;
        }
        LeanTween.value(_bottomSlider.gameObject, _bottomSlider.value, _page12Position, .5f).setEase(LeanTweenType.easeInOutSine).setOnUpdate((float sliderValue) => { _bottomSlider.value = sliderValue; });

    }

   




}
