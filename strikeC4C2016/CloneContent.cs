﻿using UnityEngine;
using System.Collections;

public class CloneContent : MonoBehaviour
{
    public GameObject _content01;
    public GameObject _content02;
    public GameObject _content03;
    public GameObject _content04;
    public GameObject _content05;
    public GameObject _content06;
    public GameObject _content07;
    public GameObject _content08;


    public void CloneThatContent(int content)
    {
        if(content == 1)
        {
            GlobalVariables.currentClonedContent = Instantiate(_content01, new Vector3(0, 17.17f, 0f), Quaternion.Euler(340f, 0, 0)) as GameObject;

        }
        if (content == 2)
        {
            GlobalVariables.currentClonedContent = Instantiate(_content02, new Vector3(0, 17.17f, 0f), Quaternion.Euler(340f, 0, 0)) as GameObject;

        }
        if (content == 3)
        {
            GlobalVariables.currentClonedContent = Instantiate(_content03, new Vector3(0, 17.17f, 0f), Quaternion.Euler(340f, 0, 0)) as GameObject;

        }
        if (content == 4)
        {
            GlobalVariables.currentClonedContent = Instantiate(_content04, new Vector3(0, 17.17f, 0f), Quaternion.Euler(340f, 0, 0)) as GameObject;

        }
        if (content == 5)
        {
            GlobalVariables.currentClonedContent = Instantiate(_content05, new Vector3(0, 17.17f, 0f), Quaternion.Euler(340f, 0, 0)) as GameObject;

        }
        if (content == 6)
        {
            GlobalVariables.currentClonedContent = Instantiate(_content06, new Vector3(0, 17.17f, 0f), Quaternion.Euler(340f, 0, 0)) as GameObject;

        }
        if (content == 7)
        {
            GlobalVariables.currentClonedContent = Instantiate(_content07, new Vector3(0, 17.17f, 0f), Quaternion.Euler(340f, 0, 0)) as GameObject;

        }
        if (content == 8)
        {
            GlobalVariables.currentClonedContent = Instantiate(_content08, new Vector3(0, 17.17f, 0f), Quaternion.Euler(340f, 0, 0)) as GameObject;

        }


    }
}