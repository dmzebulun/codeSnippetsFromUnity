﻿using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour
{

    public GameObject mainCam;
    public GameObject mainText;

    public GameObject bgPlane;
    public GameObject bigCircle;

    public GameObject base01;
    public GameObject base02;
    public GameObject base03;
    public GameObject base04;
    public GameObject base05;
    public GameObject base06;
    public GameObject base07;
    public GameObject base08;

    public GameObject baseText01;
    public GameObject baseText02;
    public GameObject baseText03;
    public GameObject baseText04;
    public GameObject baseText05;
    public GameObject baseText06;
    public GameObject baseText07;
    public GameObject baseText08;

    public GameObject homeButtonsHolder;

    public void Start()
    {
        CamStart();
    }
    //Set Global variables for Start state
    public void gvAtStart()
    {
        GlobalVariables.atHome = false;
        GlobalVariables.atStart = true;
        GlobalVariables.atTopic = false;
        Debug.Log("State is at Start");
    }

    //Set Global variables for Home state
    public void gvAtHome()
    {
        GlobalVariables.atHome = true;
        GlobalVariables.atStart = false;
        GlobalVariables.atTopic = false;
        Debug.Log("State is at Home");
    }

    //Set Global variables for Topic state
    public void gvAtTopic()
    {
        GlobalVariables.atHome = false;
        GlobalVariables.atStart = false;
        GlobalVariables.atTopic = true;
        Debug.Log("State is at Topic");
    }

    //Cam Move Close on C4C Everything Goes Away
    public void CamStart()
    {
        gvAtStart();

        LeanTween.move(mainCam, new Vector3(0, 0, -15f), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(mainCam, new Vector3(0, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.scale(bgPlane, new Vector3(2f, 2f, 2f), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(bgPlane, new Vector3(0f, 0f, 215f), 1f).setEase(LeanTweenType.easeInOutQuint);


        LeanTween.scale(base01, new Vector3(0, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(base02, new Vector3(0, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(base03, new Vector3(0, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(base04, new Vector3(0, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(base05, new Vector3(0, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(base06, new Vector3(0, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(base07, new Vector3(0, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(base08, new Vector3(0, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);

        TextMoveOut();


        LeanTween.scale(bigCircle, new Vector3(0f, 0f, 0f), 1f).setEase(LeanTweenType.easeInOutQuint);

    }

    //Camera Movement to Home. Big Circle Full Scale, Words Fly In, Base Scale Up As well
    public void CamHome()
    {
       
        gvAtHome();

        LeanTween.move(mainCam, new Vector3(0, 0, -46.53f), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(mainCam, new Vector3(0, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.scale(bgPlane, new Vector3(1f, 1f, 1f), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(bgPlane, new Vector3(0f, 0f, 190f), 1f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.move(mainText, new Vector3(-14f, 10.6f, -0.5f), 1f).setEase(LeanTweenType.easeInOutQuint);


        LeanTween.scale(base01, new Vector3(0.03f, 0.03f, 0.03f), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(base02, new Vector3(0.03f, 0.03f, 0.03f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(base03, new Vector3(0.03f, 0.03f, 0.03f), 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(base04, new Vector3(0.03f, 0.03f, 0.03f), 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(base05, new Vector3(0.03f, 0.03f, 0.03f), 1f).setDelay(.5f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(base06, new Vector3(0.03f, 0.03f, 0.03f), 1f).setDelay(.6f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(base07, new Vector3(0.03f, 0.03f, 0.03f), 1f).setDelay(.7f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.scale(base08, new Vector3(0.03f, 0.03f, 0.03f), 1f).setDelay(.8f).setEase(LeanTweenType.easeInOutQuint);

        LeanTween.move(baseText01, new Vector3(0, 21.9f, -1.7f), 1.5f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(baseText02, new Vector3(16f, 15.5f, -1.7f), 1.5f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(baseText03, new Vector3(22f, 1f, -1.7f), 1.5f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(baseText04, new Vector3(16f, -15f, -1.7f), 1.5f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(baseText05, new Vector3(0f, -21.2f, -1.7f), 1.5f).setDelay(.5f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(baseText06, new Vector3(-16f, -15f, -1.7f), 1.5f).setDelay(.6f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(baseText07, new Vector3(-22f, 1f, -1.7f), 1.5f).setDelay(.7f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(baseText08, new Vector3(-16f, 15.5f, -1.7f), 1.5f).setDelay(.8f).setEase(LeanTweenType.easeInOutQuint);


        LeanTween.scale(bigCircle, new Vector3(.25f, .25f, .25f), 1f).setEase(LeanTweenType.easeInOutQuint);


        Destroy(GlobalVariables.currentClonedContent);
              
    }

    public void CamHomeLogoButton()
    {
        if (GlobalVariables.atHome != true && GlobalVariables.atTopic == false)
        {
            gvAtHome();

            LeanTween.move(mainCam, new Vector3(0, 0, -46.53f), 1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.rotate(mainCam, new Vector3(0, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.scale(bgPlane, new Vector3(1f, 1f, 1f), 1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.move(bgPlane, new Vector3(0f, 0f, 190f), 1f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.move(mainText, new Vector3(-14f, 10.6f, -0.5f), 1f).setEase(LeanTweenType.easeInOutQuint);


            LeanTween.scale(base01, new Vector3(0.03f, 0.03f, 0.03f), 1f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.scale(base02, new Vector3(0.03f, 0.03f, 0.03f), 1f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.scale(base03, new Vector3(0.03f, 0.03f, 0.03f), 1f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.scale(base04, new Vector3(0.03f, 0.03f, 0.03f), 1f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.scale(base05, new Vector3(0.03f, 0.03f, 0.03f), 1f).setDelay(.5f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.scale(base06, new Vector3(0.03f, 0.03f, 0.03f), 1f).setDelay(.6f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.scale(base07, new Vector3(0.03f, 0.03f, 0.03f), 1f).setDelay(.7f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.scale(base08, new Vector3(0.03f, 0.03f, 0.03f), 1f).setDelay(.8f).setEase(LeanTweenType.easeInOutQuint);

            LeanTween.move(baseText01, new Vector3(0, 21.9f, -1.7f), 1.5f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.move(baseText02, new Vector3(16f, 15.5f, -1.7f), 1.5f).setDelay(.2f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.move(baseText03, new Vector3(22f, 1f, -1.7f), 1.5f).setDelay(.3f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.move(baseText04, new Vector3(16f, -15f, -1.7f), 1.5f).setDelay(.4f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.move(baseText05, new Vector3(0f, -21.2f, -1.7f), 1.5f).setDelay(.5f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.move(baseText06, new Vector3(-16f, -15f, -1.7f), 1.5f).setDelay(.6f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.move(baseText07, new Vector3(-22f, 1f, -1.7f), 1.5f).setDelay(.7f).setEase(LeanTweenType.easeInOutQuint);
            LeanTween.move(baseText08, new Vector3(-16f, 15.5f, -1.7f), 1.5f).setDelay(.8f).setEase(LeanTweenType.easeInOutQuint);


            LeanTween.scale(bigCircle, new Vector3(.25f, .25f, .25f), 1f).setEase(LeanTweenType.easeInOutQuint);
        }
        else
        {
            Debug.Log("You are already at Home");
        }





    }

    public void TextMoveOut()
    {
        LeanTween.move(baseText01, new Vector3(0, 21.9f, -40f), 1.5f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(baseText02, new Vector3(16f, 15.5f, -40f), 1.5f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(baseText03, new Vector3(22f, 1f, -40f), 1.5f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(baseText04, new Vector3(16f, -15f, -40f), 1.5f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(baseText05, new Vector3(0f, -21.2f, -40f), 1.5f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(baseText06, new Vector3(-16f, -15f, -40f), 1.5f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(baseText07, new Vector3(-22f, 1f, -40f), 1.5f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.move(baseText08, new Vector3(-16f, 15.5f, -40f), 1.5f).setEase(LeanTweenType.easeInOutQuint);

        //LeanTween.move(mainText, new Vector3(-14f, 10.6f, -40f), 1f).setEase(LeanTweenType.easeInOutQuint);
     }

    public void MainTextMoveOut()
    {
        LeanTween.move(mainText, new Vector3(-14f, 10.6f, -40f), 1f).setEase(LeanTweenType.easeInOutQuint);
    }
    
    



    //Cam Moves close to first Base and Content
    public void CamMoveC1()
    {
        gvAtTopic();
        TextMoveOut();
        MainTextMoveOut();
        
        LeanTween.move(mainCam, new Vector3(0, 16.26f, -2.57f), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(mainCam, new Vector3(-22f, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);

        //LeanTween.move(Content01, new Vector3(0, 17f, 0), 1f).setDelay(.5f).setEase(LeanTweenType.easeInOutQuint);
    }

    //Cam Moves close to first Base and Content
    public void CamMoveC2()
    {
        gvAtTopic();
        TextMoveOut();
        LeanTween.move(mainCam, new Vector3(14f, 10f, -2.57f), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(mainCam, new Vector3(350f, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);
    }

    //Cam Moves close to first Base and Content
    public void CamMoveC3()
    {
        gvAtTopic();
        TextMoveOut();
        LeanTween.move(mainCam, new Vector3(17f, -1.5f, -2.57f), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(mainCam, new Vector3(360f, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);
    }

    //Cam Moves close to first Base and Content
    public void CamMoveC4()
    {
        gvAtTopic();
        TextMoveOut();
        LeanTween.move(mainCam, new Vector3(10f, -15f, -2.57f), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(mainCam, new Vector3(10f, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);
    }

    //Cam Moves close to first Base and Content
    public void CamMoveC5()
    {
        gvAtTopic();
        TextMoveOut();
        LeanTween.move(mainCam, new Vector3(0, -16.26f, -2.57f), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(mainCam, new Vector3(12f, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);
    }

    //Cam Moves close to first Base and Content
    public void CamMoveC6()
    {
        gvAtTopic();
        TextMoveOut();
        LeanTween.move(mainCam, new Vector3(-10, -15f, -2.57f), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(mainCam, new Vector3(10f, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);
    }

    //Cam Moves close to first Base and Content
    public void CamMoveC7()
    {
        gvAtTopic();
        TextMoveOut();
        LeanTween.move(mainCam, new Vector3(-10f, -1.5f, -2.57f), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(mainCam, new Vector3(360f, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);
    }

    //Cam Moves close to first Base and Content
    public void CamMoveC8()
    {
        gvAtTopic();
        TextMoveOut();
        LeanTween.move(mainCam, new Vector3(-14, 10f, -2.57f), 1f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.rotate(mainCam, new Vector3(350f, 0, 0), 1f).setEase(LeanTweenType.easeInOutQuint);
    }
}
